<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends MY_Controller {
     
	  function __construct() {
			parent::__construct();
			$this->_checkLogin();
			$this->load->library('grocery_CRUD');
		}
    
    
	public function index()
	{
			$usnm = $this->session->userdata('username');
            $data = array();
            $data['class'] = 'loginbackground';
            $data['function'] = 'index'; 				
			$data['menu'] = 'User Profile';
			$data['jk'] = array('WANITA','PRIA'); //print_r($data['jk']);die;
			$data['usr'] = $this->db->query("select a.*,b.role from m_user a left join m_role b on b.id_role=a.id_role where a.username='$usnm'")->row_array();// print_r($data['usr']);die;
			$data['kdunit'] = $this->db->query("select * from unit_kerja")->result_array();// print_r($data['usr']);die;
				           
            $this->load->view('profile_view',$data);
	}
	
	public function editProfile() {
		//print_r($_POST);die;
		$usnm = $this->session->userdata('username');
		$this->load->model('file_model');
		
		$data = array();
		$cek = $this->input->post('password');
                if(!empty($cek)) {
                $data['password'] = $this->_encode_password($this->input->post('password')); }
		$data['nama_lengkap'] = $this->input->post('nama');
		$data['nik'] = $this->input->post('nik');
		$data['jenis_kelamin'] = $this->input->post('jk');
		$data['email'] = $this->input->post('email');
		$data['about'] = $this->input->post('about');
		$data['kode_unit_kerja'] = $this->input->post('kodeunit');
		$data['update_at'] = date('Y-m-d H:i:s');
		//print_r($data);die;
		//$config['upload_path'] = './'.uploads;
		$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp|tif|doc|pdf|xls|ppt|docx|xlsx|pptx|zip|rar';
        $config['encrypt_name'] = TRUE;
        $config['remove_spaces'] = TRUE;
		
		$this->load->library('upload', $config);
        
        $upload_path = 'assets/images'; 
        
        $error = 0;
        $no = 0;
        
		$tmp_name = $_FILES['thumbnail']['tmp_name'];
		$name = $_FILES['thumbnail']['name'];
		
		$tipe_file   = $_FILES['thumbnail']['type'];
        
        if( $this->file_model->get_file_type($tipe_file)=='photo' OR
            $this->file_model->get_file_type($tipe_file)=='pdf' OR
            $this->file_model->get_file_type($tipe_file)=='doc' OR 
            $this->file_model->get_file_type($tipe_file)=='video' 
          ){
            
            $rand = rand();
            
            $file_upload = $upload_path.'/'."{$_FILES['thumbnail']['name']}";   ///penamaan file yang diupload         
            //$file_location = "upload/$album_id-".$rand."-{$_FILES['thumbnail']['name'][$key]}";       ///lokasi file yang akan diupload     
             move_uploaded_file($tmp_name, $file_upload );
                                      
             $data['path_image'] = $file_upload;
             
        }
        
        else {
            $error++;
        }  
		
		$this->db->where('username', $usnm);
		$this->db->update('m_user', $data);
		
		$data['class'] = 'profile';
        $data['function'] = 'index'; 				
		$data['menu'] = 'User Profile';
		$data['form'] = 'profile';
		$data['rnt'] = $data['nama_lengkap'];
		
		//redirect('applikasi/runningtext');
		
		$this->load->view('confirm_sukses',$data);
		
		}
	
	
	private function _encode_password($string) {
		$string .= $this->config->item('encryption_key');
		// Return the SHA-1 encryption
		return sha1($string);
	}
                
                
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */