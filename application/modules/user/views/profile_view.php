<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- start: HEAD -->


    <?php echo Modules::run('templates/' . TEMPLATE . '/meta_css'); ?>
    <!-- end: HEAD -->
    <!-- start: BODY -->
    <body>
        <!-- start: HEADER -->
        <?php echo Modules::run('templates/' . TEMPLATE . '/header'); ?>
        <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugins/select2/select2.css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/DataTables/media/css/DT_bootstrap.css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/gritter/css/jquery.gritter.css">
        <link href="<?php echo base_url() ?>assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/select2/select2.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/datepicker/css/datepicker.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/jQuery-Tags-Input/jquery.tagsinput.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/summernote/build/summernote.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/ckeditor/contents.css">
        <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
        <!-- end: HEADER -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>
        <link type="text/css" href="<?php echo base_url() ?>assets/library/gb/greybox.css" rel="stylesheet" />
        <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
        <!-- end: HEADER -->
        <!-- start: MAIN CONTAINER -->
        <div class="main-container" style="margin-top:150px;">
            <?php echo Modules::run('templates/' . TEMPLATE . '/menu'); ?>
            <!-- start: PAGE -->
            <div class="main-content">

                <div class="container">
                    <!-- start: PAGE HEADER -->
                    <div class="row">
                        <div class="col-sm-12">							
                            <!-- start: PAGE TITLE & BREADCRUMB -->
                            <ol class="breadcrumb">
                                <li>
                                    <i class="clip-stack"></i>
                                    <a href="#">
                                        User Profile
                                    </a>
                                </li>
                            </ol>

                            <div class="page-header">
                                <h1>User Profile</h1>
                            </div>
                            <!-- end: PAGE TITLE & BREADCRUMB -->

                            <div class="row">                                
                                <div class="col-sm-12">                                
                                    <div class="tabbable">
                                        <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
                                            <li class="active">
                                                <a data-toggle="tab" href="#panel_overview">
                                                    Overview
                                                </a>
                                            </li>
                                            <li>
                                                <a data-toggle="tab" href="#panel_edit_account">
                                                    Edit Account
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div id="panel_overview" class="tab-pane in active">
                                                <div class="row">
                                                    <div class="col-sm-5 col-md-4">
                                                        <div class="user-left">
                                                            <div class="center">
                                                                <h4><?php echo $usr['nama_lengkap'] ?></h4>
                                                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                    <div class="user-image">
                                                                        <div class="fileupload-new thumbnail"><img src="<?php echo base_url() ?><?php echo isset($usr['path_image']) ? $usr['path_image'] : 'assets/images/avatar-1-xl.jpg' ?>" width="160" alt="">
                                                                        </div>
                                                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                                        <div class="user-image-buttons">
                                                                            <span class="btn btn-teal btn-file btn-sm"><span class="fileupload-new"><i class="fa fa-pencil"></i></span><span class="fileupload-exists"><i class="fa fa-pencil"></i></span>
                                                                                <input type="file">
                                                                            </span>
                                                                            <a href="#" class="btn fileupload-exists btn-bricky btn-sm" data-dismiss="fileupload">
                                                                                <i class="fa fa-times"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            </div>


                                                        </div>
                                                    </div>
                                                    <div class="col-sm-7 col-md-8">

                                                        <table class="table table-condensed table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th colspan="3">Account Information</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Username</td>
                                                                    <td>
                                                                        <a href="#">
                                                                            <?php echo $usr['username'] ?>
                                                                        </a></td>
                                                                    <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Role:</td>
                                                                    <td>
                                                                        <a href="#">
                                                                            <?php echo $usr['role']; ?>
                                                                        </a></td>
                                                                    <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Email:</td>
                                                                    <td>
                                                                        <a href="#">
                                                                            <?php echo $usr['email']; ?>
                                                                        </a></td>
                                                                    <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Kode Unit Kerja:</td>
                                                                    <td><?php echo $usr['kode_unit_kerja'] ?></td>
                                                                    <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>NIP:</td>
                                                                    <td><?php echo $usr['nik'] ?></td>
                                                                    <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Jenis Kelamin:</td>
                                                                    <td><?php echo $usr['jenis_kelamin'] ?></td>
                                                                    <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <h3>About Me</h3>
                                                        <p>
                                                            <?php echo isset($usr['about']) ? $usr['about'] : 'About you'; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="panel_edit_account" class="tab-pane">
                                                <form action="<?php echo base_url() ?>user/profile/editProfile" method="post" enctype="multipart/form-data">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h3>Account Info</h3>
                                                            <hr>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    Nama Lengkap
                                                                </label>
                                                                <input type="text" placeholder="Nama Lengkap" class="form-control" id="firstname" name="nama" value="<?php echo isset($usr['nama_lengkap']) ? $usr['nama_lengkap'] : '' ?>" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    Password
                                                                </label>
                                                                <input type="password" placeholder="Password" class="form-control" name="password" id="password" value="" >
                                                                <br/>
                                                                <div class="alert alert-warning">

                                                                    <i class="fa fa-exclamation-triangle"></i>
                                                                    <strong>Jika Password Tidak Diganti, Kosongkan saja.</strong>
                                                                   
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    Email Address
                                                                </label>
                                                                <input type="email" placeholder="mail@example.com" class="form-control" id="email" name="email" value="<?php echo isset($usr['email']) ? $usr['email'] : '' ?>" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    NIP
                                                                </label>
                                                                <input type="text" placeholder="NIP" class="form-control" id="phone" name="nik" value="<?php echo isset($usr['nik']) ? $usr['nik'] : '' ?>">
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    Kode Unit Kerja
                                                                </label>
                                                                <select id="form-field-select-1" class="form-control" name="kodeunit">
                                                                    <option>--Pilih Kode Unit Kerja</option>
                                                                    <?php
                                                                     foreach ($kdunit as $rw) {
                                                                         $selected = '';
                                                                         if ($rw['kode_unit_kerja'] == $usr['kode_unit_kerja']) {
                                                                             $selected = 'selected';
                                                                         }
                                                                         ?>
                                                                         <option value="<?php echo $rw['kode_unit_kerja'] ?>" <?php echo $selected; ?>><?php echo $rw['kode_unit_kerja'] ?> :: <?php echo $rw['nama_unit_kerja'] ?></option>
                                                                     <?php } ?>
                                                                </select>
                                                            </div>

                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    Jenis Kelamin
                                                                </label>
                                                                <div>

                                                                    <select id="form-field-select-1" class="form-control" name="jk">
                                                                        <option>--Pilih Jenis Kelamin--</option>
                                                                        <?php
                                                                         foreach ($jk as $val) {
                                                                             $selected = '';
                                                                             if ($val == $usr['jenis_kelamin']) {
                                                                                 $selected = 'selected';
                                                                             }
                                                                             ?>
                                                                             <option value="<?php echo $val; ?>" <?php echo $selected; ?>><?php echo $val ?></option>
                                                                         <?php } ?>
                                                                    </select>

                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>
                                                                    Image Upload
                                                                </label>
                                                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                    <div class="fileupload-new thumbnail" style="width: 150px; height: 150px;"><img src="<?php echo base_url() ?><?php echo isset($usr['path_image']) ? $usr['path_image'] : 'assets/images/avatar-1-xl.jpg' ?>" alt="">
                                                                    </div>
                                                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 150px; max-height: 150px; line-height: 20px;"></div>
                                                                    <div class="user-edit-image-buttons">
                                                                        <span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                                            <input type="file" name="thumbnail"><br>
                                                                        </span>
                                                                        <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                                                            <i class="fa fa-times"></i> Remove
                                                                        </a>
                                                                    </div><br>
                                                                    <strong><small>Maximum Size 150 x 150</small></strong>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    About Me
                                                                </label>
                                                                <div>
                                                                    <label class="radio-inline">
                                                                        <textarea name="about" id="form-field-22" style="width:470px;height:150px;" class="form-control" placeholder="Default Text"><?php echo isset($usr['about']) ? $usr['about'] : '' ?></textarea>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div>
                                                                Required Fields
                                                                <hr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <p>
                                                                By clicking UPDATE, you are agreeing to the Policy and Terms &amp; Conditions.
                                                            </p>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <button class="btn btn-teal btn-block" type="submit">
                                                                Update <i class="fa fa-arrow-circle-right"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- end: PAGE HEADER -->
                    <!-- start: PAGE CONTENT -->



                    <!-- end: PAGE CONTENT-->
                </div>
            </div>
            <!-- end: PAGE -->
        </div>
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <?php echo Modules::run('templates/' . TEMPLATE . '/footer'); ?>
        <!-- end: FOOTER -->

        <!-- start: MAIN JAVASCRIPTS -->
        <?php echo Modules::run('templates/' . TEMPLATE . '/js'); ?>
        <!-- end: MAIN JAVASCRIPTS -->
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/select2/select2.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
        <script src="<?php echo base_url() ?>assets/js/table-data.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/bootstrap-paginator/src/bootstrap-paginator.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/jquery.pulsate/jquery.pulsate.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/gritter/js/jquery.gritter.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/ui-elements.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
        <script src="<?php echo base_url() ?>assets/js/ui-modals.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/autosize/jquery.autosize.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/select2/select2.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/jquery-maskmoney/jquery.maskMoney.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/bootstrap-colorpicker/js/commits.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/summernote/build/summernote.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/ckeditor/ckeditor.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
        <script src="<?php echo base_url() ?>assets/js/form-elements.js"></script>
        <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script>
              jQuery(document).ready(function() {
                  Main.init();

                  UIElements.init();
                  FormElements.init();
              });

              function formUser() {
                  GB_show("User Management", '<?php echo base_url() ?>management_user/user/userManagement', 900, 1080);
                  $('html, body').animate({scrollTop: 0}, 'slow');
              }
              function formRole() {
                  GB_show("Role Management", '<?php echo base_url() ?>management_user/user/role', 500, 900);
                  $('html, body').animate({scrollTop: 0}, 'slow');
              }
        </script>

    </body>
    <!-- end: BODY -->

    <!-- Mirrored from www.cliptheme.com/clip-one/ by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 16 Nov 2013 08:37:43 GMT -->
</html>