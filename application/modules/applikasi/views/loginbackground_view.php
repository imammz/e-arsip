<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
	<!--<![endif]-->
	<!-- start: HEAD -->

	<?php echo Modules::run('templates/' . TEMPLATE . '/meta_css'); ?>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: HEADER -->
		<?php echo Modules::run('templates/' . TEMPLATE . '/header'); ?>
		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugins/select2/select2.css" />
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/DataTables/media/css/DT_bootstrap.css" />
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/gritter/css/jquery.gritter.css">
		<link href="<?php echo base_url() ?>assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
		<link href="<?php echo base_url() ?>assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/select2/select2.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/datepicker/css/datepicker.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/jQuery-Tags-Input/jquery.tagsinput.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/summernote/build/summernote.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/ckeditor/contents.css">
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
		<!-- end: HEADER -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>
		<link type="text/css" href="<?php echo base_url() ?>assets/library/gb/greybox.css" rel="stylesheet" />
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
		<!-- end: HEADER -->
		<!-- start: MAIN CONTAINER -->
		<div class="main-container" style="margin-top:150px;">
			<?php echo Modules::run('templates/' . TEMPLATE . '/menu'); ?>
			<!-- start: PAGE -->
			<div class="main-content">

				<div class="container">
					<!-- start: PAGE HEADER -->
					<div class="row">
						<div class="col-sm-12">
							<!-- start: PAGE TITLE & BREADCRUMB -->
							<ol class="breadcrumb">
								<li>
									<i class="clip-stack"></i>
									<a href="#"> <?php echo $menu ?> </a>
								</li>
							</ol>

							<div class="page-header">
								<h1><?php echo $menu ?></h1>
							</div>
							<!-- end: PAGE TITLE & BREADCRUMB -->

							<div class="row">
								<div class="col-sm-12">
							<!-- Content Here -->
								<div class="form-group">
											<div class="col-sm-12" align="center">
												<label>
													Upload Background Image
												</label>
												<form role="form" id="form-entry" class="form-horizontal" method="POST" action="<?php echo base_url() ?>applikasi/Loginbackground/upload_process" enctype="multipart/form-data">
												<div class="fileupload fileupload-new" data-provides="fileupload" align="center">
													<div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="<?php echo base_url()?>assets/images/<?php echo isset($rt['background'])?$rt['background']:'aaa.png'?>" alt=""/>
													</div>
													<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
													<div>
														<span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-picture-o"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
															<input type="file" name="thumbnail">
														</span>
														<a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
															<i class="fa fa-times"></i> Remove
														</a>
														<button type="submit" class="btn btn-blue"><i class="clip-checkmark"></i> Proses</button>
														
													</div>
													<br/>
													<div class="alert alert-info">
<i class="fa fa-info-circle"></i>
Ukuran Resolusi Yang Direkomendasikan Adalah :
<strong> 1366x768 </strong> - <strong> 1360x768 </strong> - <strong> 1280x768 </strong>
</div>
												</div>
												</form>
												<!--<div class="alert alert-warning">
													<span class="label label-warning">NOTE!</span>
													<span> Image preview only works in IE10+, FF3.6+, Chrome6.0+ and Opera11.1+. In older browsers and Safari, the filename is shown instead. </span>
												</div>-->
											</div>
										</div>
							
							
								</div>
							</div>

						</div>
					</div>
					<!-- end: PAGE HEADER -->
					<!-- start: PAGE CONTENT -->

					<!-- end: PAGE CONTENT-->
				</div>
			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->
		<!-- start: FOOTER -->
		<?php echo Modules::run('templates/' . TEMPLATE . '/footer'); ?>
		<!-- end: FOOTER -->

		<!-- start: MAIN JAVASCRIPTS -->
		<?php echo Modules::run('templates/' . TEMPLATE . '/js'); ?>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/select2/select2.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
		<script src="<?php echo base_url() ?>assets/js/table-data.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-paginator/src/bootstrap-paginator.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery.pulsate/jquery.pulsate.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/gritter/js/jquery.gritter.min.js"></script>
		<script src="<?php echo base_url() ?>assets/js/ui-elements.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
		<script src="<?php echo base_url() ?>assets/js/ui-modals.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/autosize/jquery.autosize.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/select2/select2.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery-maskmoney/jquery.maskMoney.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-colorpicker/js/commits.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/summernote/build/summernote.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/ckeditor/ckeditor.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
		<script src="<?php echo base_url() ?>assets/js/form-elements.js"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script>
			jQuery(document).ready(function() {
				Main.init();

				UIElements.init();
				FormElements.init();
			});

		</script>

	</body>
	<!-- end: BODY -->

	<!-- Mirrored from www.cliptheme.com/clip-one/ by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 16 Nov 2013 08:37:43 GMT -->
</html>