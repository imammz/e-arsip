<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Instansi extends MY_Controller {
     
	  function __construct() {
			parent::__construct();
			$this->_checkLogin();
			$this->load->library('grocery_CRUD');
		}
    
    
	public function index()
	{
            $data = array();
            $data['class'] = 'isntansi';
            $data['function'] = 'index'; 				
			$data['menu'] = 'Instansi';
			$data['rt'] = $this->db->query("select * from m_instansi where id_instansi='1'")->row_array();	           
            	           
            $this->load->view('instansi_view',$data);
	}
	
	public function update()
	{
	//print_r($_POST);die;
		$data = array();
		$data['nama_instansi'] = $this->input->post('nama', true);
		$data['prefix_instansi'] = $this->input->post('prefix', true);
		$data['alamat_instansi'] = $this->input->post('alamat', true);
		$data['telp_instansi'] = $this->input->post('telp', true);
		$data['kodepos_instansi'] = $this->input->post('kodepos', true);
		$data['email_instansi'] = $this->input->post('email', true);
		
		$this->db->where('id_instansi',1);
		$this->db->update('m_instansi',$data);
		
		$data['class'] = 'instansi';
        $data['function'] = 'index'; 				
		$data['menu'] = 'Instansi';
		$data['form'] = 'instansi';
		$data['rnt'] = $data['nama_instansi'];
		
		//redirect('applikasi/runningtext');
		
		$this->load->view('confirm_sukses',$data);
	}
	
	
	
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */