<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logs extends MY_Controller {
     
	  function __construct() {
			parent::__construct();
			$this->_checkLogin();
		}
    
    
	public function index()
	{
            $data = array();
            $data['class'] = 'logs';
            $data['function'] = 'index'; 				
				$data['menu'] = 'Logs History';
				$data['logs'] = $this->db->query("select * from m_logs order by id_logs DESC")->result_array();	           
            	           
            $this->load->view('logs_view',$data);
	}
	
		
	
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */