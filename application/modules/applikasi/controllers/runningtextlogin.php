<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Runningtextlogin extends MY_Controller {
     
	  function __construct() {
			parent::__construct();
			$this->_checkLogin();
			$this->load->library('grocery_CRUD');
		}
    
    
	public function index()
	{
            $data = array();
            $data['class'] = 'runningtextlogin';
            $data['function'] = 'index'; 				
			$data['menu'] = 'Running Text Login';
			$data['rt'] = $this->db->query("select * from m_applikasi where id_applikasi='1'")->row_array();
			
            $this->load->view('runningtextlogin_view',$data);
	}
	
	public function update()
	{
		$data = array();
		$data['running_text_login'] = $this->input->post('runningtextlogin', true);
		$this->db->where('id_applikasi',1);
		$this->db->update('m_applikasi',$data);
		
		$data['class'] = 'runningtextlogin';
        $data['function'] = 'index'; 				
		$data['menu'] = 'Running Text Login';
		$data['form'] = 'runningtextlogin';
		$data['rnt'] = $data['running_text_login'];
		
		
		$this->load->view('confirm_sukses',$data);
	}
	
	
	
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */