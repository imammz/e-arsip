<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Help extends MY_Controller {
     
	  function __construct() {
			parent::__construct();
			$this->_checkLogin();
			$this->load->library('grocery_CRUD');
		}
    
    
	public function index()
	{
            $data = array();
            $data['class'] = 'help';
            $data['function'] = 'index'; 				
			$data['menu'] = 'Help';
			$data['rt'] = $this->db->query("select * from m_applikasi where id_applikasi='1'")->row_array();	           
            $this->load->view('help_view',$data);
	}
	
	public function update()
	{
	//print_r($_POST);die;
		$data = array();
		$data['help'] = $this->input->post('help', true);
		
		$this->db->where('id_applikasi',1);
		$this->db->update('m_applikasi',$data);
		
		$data['class'] = 'help';
        $data['function'] = 'index'; 				
		$data['menu'] = 'Help';
		$data['form'] = 'help';
		$data['rnt'] = substr($data['help'],0,100);
		
		//redirect('applikasi/runningtext');
		
		$this->load->view('confirm_sukses',$data);
	}
	
	
	
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */