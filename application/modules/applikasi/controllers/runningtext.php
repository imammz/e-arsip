<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Runningtext extends MY_Controller {
     
	  function __construct() {
			parent::__construct();
			$this->_checkLogin();
			$this->load->library('grocery_CRUD');
		}
    
    
	public function index()
	{
            $data = array();
            $data['class'] = 'runningtext';
            $data['function'] = 'index'; 				
			$data['menu'] = 'Running Text';
			$data['rt'] = $this->db->query("select * from m_applikasi where id_applikasi='1'")->row_array();
				           
            $this->load->view('runningtext_view',$data);
	}
	
	public function update()
	{
	//print_r($_POST);die;
		$data = array();
		$data['running_text'] = $this->input->post('runningtext', true);
		
		$this->db->where('id_applikasi',1);
		$this->db->update('m_applikasi',$data);
		
		$data['class'] = 'runningtext';
        $data['function'] = 'index'; 				
		$data['menu'] = 'Running Text';
		$data['form'] = 'runningtext';
		$data['rnt'] = $data['running_text'];
		
		//redirect('applikasi/runningtext');
		
		$this->load->view('confirm_sukses',$data);
	}
	
	
	
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */