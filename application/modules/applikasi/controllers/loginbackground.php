<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Loginbackground extends MY_Controller {
     
	  function __construct() {
			parent::__construct();
			$this->_checkLogin();
			$this->load->library('grocery_CRUD');
		}
    
    
	public function index()
	{
            $data = array();
            $data['class'] = 'loginbackground';
            $data['function'] = 'index'; 				
			$data['menu'] = 'Login Background';
			$data['rt'] = $this->db->query("select * from m_applikasi where id_applikasi='1'")->row_array();
				           
            $this->load->view('loginbackground_view',$data);
	}
	
	public function upload_process() {

		$this->load->model('file_model');
		
		$data = array();
		
		//$config['upload_path'] = './'.uploads;
		$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp|tif|doc|pdf|xls|ppt|docx|xlsx|pptx|zip|rar';
        $config['encrypt_name'] = TRUE;
        $config['remove_spaces'] = TRUE;
		
		$this->load->library('upload', $config);
        
        $upload_path = 'assets/images'; 
        
        $error = 0;
        $no = 0;
        
		$tmp_name = $_FILES['thumbnail']['tmp_name'];
		$name = $_FILES['thumbnail']['name'];
		
		$tipe_file   = $_FILES['thumbnail']['type'];
        
        if( $this->file_model->get_file_type($tipe_file)=='photo' OR
            $this->file_model->get_file_type($tipe_file)=='pdf' OR
            $this->file_model->get_file_type($tipe_file)=='doc' OR 
            $this->file_model->get_file_type($tipe_file)=='video' 
          ){
            
            $rand = rand();
            
            $file_upload = $upload_path.'/'."{$_FILES['thumbnail']['name']}";   ///penamaan file yang diupload         
            //$file_location = "upload/$album_id-".$rand."-{$_FILES['thumbnail']['name'][$key]}";       ///lokasi file yang akan diupload     
             move_uploaded_file($tmp_name, $file_upload );
                                      
             $data['background'] = $name;
             
        }
        
        else {
            $error++;
        }  
		
		$this->db->where('id_applikasi', 1);
		$this->db->update('m_applikasi', $data);
		
		$data['class'] = 'loginbackground';
        $data['function'] = 'index'; 				
		$data['menu'] = 'Login Background';
		$data['form'] = 'loginbackground';
		$data['rnt'] = $name;
		
		//redirect('applikasi/runningtext');
		
		$this->load->view('confirm_sukses',$data);
		
		}
	
	
	
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */