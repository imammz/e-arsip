<!DOCTYPE html>
<!-- Template Name: Clip-One - Responsive Admin Template build with Twitter Bootstrap 3 Version: 1.0 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
	<!--<![endif]-->
	<!-- start: HEAD -->
	
<!-- Mirrored from www.cliptheme.com/clip-one/login_example2.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 16 Nov 2013 08:40:08 GMT -->
<head>
		<title><?php echo WEB_TITLE ?></title>
		<!-- start: META -->
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- end: META -->
		<!-- start: MAIN CSS -->
		<link href="<?php echo base_url() ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/fonts/style.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main-responsive.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/iCheck/skins/all.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/theme_light.css" id="skin_color">
                <link rel="shortcut icon" href="<?php echo base_url() ?>assets/favicon.ico" />
		<!--[if IE 7]>
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
		<![endif]-->
		<!-- end: MAIN CSS -->
		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<?php
	$background = Master_model::_getBackground();
	 ?>
	<body class="login" style="background-image:url('assets/images/<?php echo $background ?>'); background-size: 110%;">
		<div class="top_menu">
            	<div class="top-running-text">
            		<?php
						$running_text_login = Master_model::_getRunningTextLogin();
	 ?>
                	<marquee>
                	<span style="font-size:14px; color:#FFF; float:right; font-weight:bold;"><?php echo $running_text_login ?></span>
                    </marquee>
                </div>
                
                <li style="margin-top:15px;">
                    <a href="" style="color:white; text-decoration:none; margin-left:10px;">
                        <i class="clip-calendar"></i>
                        &nbsp;<?php echo DATE('D d-M-Y'); ?>
                    </a>
                <li>
            </div>
		<div class="main-login col-sm-4 col-sm-offset-4">			                  
                    <div class="logo" style="color:black; text-shadow:3px 0px 3px #FFFFFF;">
                        <img src="<?php echo base_url() ?>public/images/logo.png" width="180"/>
                        <br/>E-ARS<i class="clip-clip"></i>P <br/><font size="4" color="#fff"> E-ARSIP </font> 
			</div>
 			<!-- start: LOGIN BOX -->
			<div class="box-login">
				<h3>Silahkan Masukan Username & Password Anda</h3>
				<p>
					Please enter your name and password to log in.
				</p>
				<form class="form-login" action="<?php echo base_url() ?>login/process" method="POST">
					<div class="errorHandler alert alert-danger no-display">
						<i class="fa fa-remove-sign"></i> Tidak Bisa Logi, Akun Tidak Ditemukan
					</div>
					<fieldset>
						<div class="form-group">
							<span class="input-icon">
								<input type="text" class="form-control" name="username" placeholder="Username">
								<i class="fa fa-user"></i> </span>
						</div>
						
						<div class="form-actions">
							
							<button type="submit" class="btn btn-bricky pull-right">
								Login <i class="fa fa-arrow-circle-right"></i>
							</button>
						</div>
						
					</fieldset>
				</form>
			</div>
			<!-- end: LOGIN BOX -->
			<!-- start: FORGOT BOX -->
			<div class="box-forgot">
				<h3>Lupa Password?</h3>
				
				<form class="form-forgot">
					
					<fieldset>
						<div class="form-group">
							<span class="input-icon">
								<h4>Harap Hubungi Administrator Pusat Kearsipan</h4>
								</span>
						</div>
						<div class="form-actions">
							<button class="btn btn-light-grey go-back pull-right">
								<i class="fa fa-circle-arrow-left"></i> Back
							</button>

						</div>
					</fieldset>
				</form>
			</div>
			<!-- end: FORGOT BOX -->
			<!-- start: REGISTER BOX -->
			
			<!-- end: REGISTER BOX -->
			<!-- start: COPYRIGHT -->
			<div class="copyright">
				<?php echo COPYRIGHT ?>
			</div>
			<!-- end: COPYRIGHT -->
		</div>
		<!-- start: MAIN JAVASCRIPTS -->
		<!--[if lt IE 9]>
		<script src="<?php echo base_url() ?>assets/plugins/respond.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/excanvas.min.js"></script>
		<![endif]-->
		<script src="<?php echo base_url() ?>assets/library/ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/blockUI/jquery.blockUI.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/iCheck/jquery.icheck.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/less/less-1.5.0.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
		<script src="<?php echo base_url() ?>assets/js/main.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script src="<?php echo base_url() ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
		<script src="<?php echo base_url() ?>assets/js/login.js"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script>
			jQuery(document).ready(function() {
				Main.init();
				Login.init();
			});
		</script>
	</body>
	<!-- end: BODY -->

<!-- Mirrored from www.cliptheme.com/clip-one/login_example2.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 16 Nov 2013 08:40:08 GMT -->
</html>