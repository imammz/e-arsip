<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Login extends MY_Controller {

	function __construct() {
		parent::__construct();
	}

	function index() {

		if($this->session->userdata('login') == TRUE) {
			redirect('main');
		}
		else {

			$this->load->view('login_view');
		}
	}

	public function process() {

		$this->load->model("login_model");

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		if(!empty($username) || !empty($password)) {

			if($this->login_model->check_user($username,MD5($password))) {

				$query = $this->db->query("SELECT m_user.username, m_user.nama_lengkap, m_user.nik, m_user.jenis_kelamin,  m_user.email, m_user.kode_unit_kerja, m_user.path_image, unit_kerja.nama_unit_kerja,  m_role.id_role as id_role, m_role.role FROM (m_user left join m_role on(m_user.id_role = m_role.id_role )) left join unit_kerja ON(m_user.kode_unit_kerja = unit_kerja.kode_unit_kerja) WHERE username = '$username' AND stat_active = 'Y' LIMIT 0, 1");

				$rs = $query->row_array();

				$data = array();
				$rs['login'] = TRUE;
				$data = $rs;

				$this->session->set_userdata($data);
				redirect('home');
			}
			else {
				redirect('login');
			}
		}
		else {
			redirect('login');
		}

	}

	public function lockscreen() {
		$this->load->view('lockscreen_view');
	}

	private function _encode_password($string) {
		$string .= $this->config->item('encryption_key');
		// Return the SHA-1 encryption
		return sha1($string);
	}
	
	private function no_encode_password($string) {
	
		return $string;
	}

	function logout() {
		$this->session->sess_destroy();
		redirect("login");
	}

	function r3g1st3r($username,$password,$id_role,$email,$unitkerja) {

		$data = array();
		$data['username'] = $username;
		$data['password'] = $this->_encode_password($password);
		$data['id_role'] = $id_role;
		$data['email'] = $email;
		$data['kode_unit_kerja'] = $unitkerja;
		$data['insert_at'] = date("Y-m-d g:i:s a");

		if($this->db->insert('m_user',$data)) {
			echo 'SUCCESS ENTRY NEW USER';
		}

	}

}


/* End of file login.php */
/* Location: ./application/controllers/login.php */
