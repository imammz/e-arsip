<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Otoritas extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->_checkLogin();
		$this->load->model('master/master_model');
		$this->load->model('peminjaman_model');
	}

	public function index() {
		
		$this->output->enable_profiler(FALSE);				
		$data = array();
		$data['class'] = 'pelayanan';
		$data['function'] = 'otoritas';		
		$data['peminjaman'] = $this->peminjaman_model->_loadPeminjaman();	
		
		$this->load->view('otoritas_view',$data);
	}
	
	public function detil($id_arsip) {

			$data = array();
			$data['id_arsip'] = $id_arsip;
			$data['peminjaman'] = $this->peminjaman_model->_getPeminjaman($id_arsip);			
			//$this->_outputjson($data['peminjaman']);
			$this->load->view('otoritas_detil_view',$data);
		}
	
	public function accept($id_peminjaman) {
			
		$this->peminjaman_model->processAccept($id_peminjaman);
		$this->successAccept();		
		
	}
	
	public function reject($id_peminjaman) {
			
		$this->peminjaman_model->processReject($id_peminjaman);
		$this->successReject();
	}
	
	private function successAccept() {
			echo '<script>
                alert("Pengajuan Peminjaman Telah Disetujui");
                window.location.replace("' . base_url() . 'pelayanan/otoritas");
                  </script>';
		}

	private function successReject() {
			echo '<script>
                alert("Pengajuan Peminjaman Telah Digagalkan");
                window.location.replace("' . base_url() . 'pelayanan/otoritas");
                  </script>';
		}

}


/* End of file login.php */
/* Location: ./application/controllers/login.php */
