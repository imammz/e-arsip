<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengembalian extends MY_Controller {

		function __construct() {
			parent::__construct();
			$this->_checkLogin();
			$this->load->model('peminjaman_model');
		}
		
		
	public function index()
	{
            $data = array();
            $data['class'] = 'pelayanan';
            $data['function'] = 'pengembalian';
				$data['peminjaman'] = $this->peminjaman_model->_loadPengembalian();
            
		$this->load->view('pengembalian_view',$data);                
	}
	
	public function proses($id_peminjaman) {
			
		$this->peminjaman_model->processPengembalian($id_peminjaman);
		$this->successPengembalian();		
		
	}
	
	private function successPengembalian() {
			echo '<script>
                alert("Pengajuan Peminjaman Telah Disetujui");
                window.location.replace("' . base_url() . 'pelayanan/pengembalian");
                  </script>';
		}
		
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */