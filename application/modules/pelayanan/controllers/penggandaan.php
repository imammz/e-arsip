<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Penggandaan extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->_checkLogin();
		$this->load->model('master/master_model');
		$this->load->model('peminjaman_model');
	}

	public function index() {
		$data = array();
		$data['class'] = 'pelayanan';
		$data['function'] = 'penggandaan';

		$data['klasifikasi'] = $this->master_model->_loadKodeKlasifikasi();
		$data['unit_kerja'] = $this->master_model->_loadUnitKerja();
		$data['tingkat_perkembangan'] = $this->master_model->_loadTingkatPerkembangan();
		$data['kondisi_fisik'] = $this->master_model->_loadKondisiFisik();
		$data['media_simpan'] = $this->master_model->_loadMediaSimpan();
		$data['depo'] = $this->master_model->_loadLokasiDepo();
		$data['ruang'] = $this->master_model->_loadLokasiRuang();
		$data['boks'] = $this->master_model->_loadLokasiBoks();
		
		$data['peminjaman'] = $this->peminjaman_model->_loadAllPenggandaan();

		$this->load->view('penggandaan_view',$data);
	}

	public function result() {
		$this->output->enable_profiler(FALSE);
		$data = array();

		$data['kode_klasifikasi'] = $this->input->post('kode_klasifikasi',true);
		$data['kode_jra'] = $this->input->post('kode_jra',true);
		$data['kode_unit_kerja'] = $this->input->post('kode_unit_kerja',true);
		$data['uraian'] = $this->input->post('uraian',true);
		$data['kurun_waktu_awal'] = $this->input->post('kurun_waktu_awal',true);
		$data['kurun_waktu_akhir'] = $this->input->post('kurun_waktu_akhir',true);
		$data['nomor_depo'] = $this->input->post('nomor_depo',true);
		$data['nomor_ruang'] = $this->input->post('nomor_ruang',true);
		$data['nomor_lemari'] = $this->input->post('nomor_lemari',true);
		$data['nomor_boks'] = $this->input->post('nomor_boks',true);
		$data['nomor_folder'] = $this->input->post('nomor_folder',true);

		//$this->db->where('status_pindah','Y');
		//$this->db->where('status_pinjam','N');
		$this->db->where('status_musnah','N');
		$this->db->where('status_serah','N');

		if(!empty($data['kode_klasifikasi'])) {
			$this->db->where('kode_klasifikasi',$data['kode_klasifikasi']);
		}
		if(!empty($data['kode_jra'])) {
			$this->db->where('kode_jra',$data['kode_jra']);
		}
		if(!empty($data['kode_unit_kerja'])) {
			$this->db->where('arsip.kode_unit_kerja',$data['kode_unit_kerja']);
		}
		if(!empty($data['kurun_waktu_awal'])) {
			$this->db->where('kurun_waktu_awal >=',$data['kurun_waktu_awal']);
		}
		if(!empty($data['kurun_waktu_akhir'])) {
			$this->db->where('kurun_waktu_awal <=',$data['kurun_waktu_akhir']);
		}
		if(!empty($data['nomor_depo'])) {
			$this->db->where('nomor_depo',$data['nomor_depo']);
		}
		if(!empty($data['nomor_ruang'])) {
			$this->db->where('nomor_ruang',$data['nomor_ruang']);
		}
		if(!empty($data['nomor_lemari'])) {
			$this->db->where('nomor_lemari',$data['nomor_lemari']);
		}
		if(!empty($data['nomor_boks'])) {
			$this->db->where('nomor_boks',$data['nomor_boks']);
		}
		if(!empty($data['nomor_folder'])) {
			$this->db->like('nomor_folder',$data['nomor_folder']);
		}
		if(!empty($data['uraian'])) {
			$this->db->like('uraian',$data['uraian']);
		}
		$this->db->order_by('id_arsip','ASC');
		$this->db->select('nama_unit_kerja as unit_kerja, id_arsip, nomor_definitif, uraian, kode_klasifikasi');
		$this->db->join('unit_kerja','unit_kerja.kode_unit_kerja = arsip.kode_unit_kerja');
		$result = $this->db->get('arsip')->result_array();
		// print_r($result);
		$data = array();
		$data['class'] = 'pelayanan';
		$data['function'] = 'peminjaman';
	   $data['unit_kerja'] = $this->master_model->_loadUnitKerja(); 
		$data['result'] = $result;
		$data['count'] = count($result);
		$this->load->view('penggandaan_result_view',$data);
	}

	public function process() {
			
		$this->output->enable_profiler(FALSE);
      
		$data = array();
		$data['nama_peminjam'] = $this->input->post('nama_peminjam',TRUE);
		$data['kode_unit_kerja'] = $this->input->post('kode_unit_kerja',TRUE);
		$data['nik'] = $this->input->post('nik',TRUE);
		$data['hp'] = $this->input->post('hp',TRUE);
		$data['email'] = $this->input->post('email',TRUE);
		$data['tanggal_peminjaman'] = $this->input->post('tanggal_peminjaman',TRUE);
		$data['tanggal_pengembalian'] = $this->input->post('tanggal_pengembalian',TRUE);
		$data['jenis_peminjaman'] = $this->input->post('jenis_peminjaman',TRUE);
		$data['insert_by'] = $this->session->userdata('username');
		
		$id_arsip_pinjam = $this->input->post('peminjaman',TRUE);
		
		$peminjaman = $this->peminjaman_model->_processPeminjaman($data,$id_arsip_pinjam);
		
		$this->successProcess();	
                
                $pesan = "Terima Kasih Untuk Pengajuan Peminjaman Arsip Anda, Tunggu Proses Otorisasi Selanjutnya. -DKPP-";  $pesan = str_replace(' ', '%20', $pesan);            
                $url = "http://162.211.84.203/sms/smsmasking.php?username=imammz&key=967d4d8c4f9667f72ea7241fe0f92c58&number={$data['hp']}&message={$pesan}";
$fields = array(
	'test' => 'raja-sms'
);

//url-ify the data for the POST
foreach($fields as $key=>$value) {
    $fields_string .= $key.'='.$value.'&';
}
rtrim($fields_string, '&');

//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch,CURLOPT_POST, count($fields));
curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

//execute post
$result = curl_exec($ch);

//close connection
curl_close($ch);
		
	}
	
	private function successProcess() {
			echo '<script>
                alert("Proses Pengajuan Penggandaan Berhasil, Harap Tunggu Otorisasi!");
                window.location.replace("' . base_url() . 'pelayanan/penggandaan");
               </script>';
		}
	
	
	

	public function pdfFormPeminjaman($id_peminjaman = '') {
		
		$peminjaman = $this->peminjaman_model->_getPeminjamanById($id_peminjaman);
		$kode_peminjaman = $peminjaman['kode_peminjaman'];
		$id_arsip_pinjam = $this->peminjaman_model->_getIdArsipById($id_peminjaman);
				
		$this->load->library('TCPDF');

		// create new PDF document
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION,PDF_UNIT,PDF_PAGE_FORMAT,true,'UTF-8',false);
		// set document information
		$pdf->SetCreator(WEB_TITLE);
		$pdf->SetAuthor($this->session->userdata('username'));
		$pdf->SetTitle("Form Peminjaman - ".$kode_peminjaman);
		$pdf->SetSubject($kode_peminjaman.' - '.date("d-M-Y"));
		$pdf->code = $kode_peminjaman;
	
		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN,'',PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA,'',PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(5,5,5);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE,PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// ---------------------------------------------------------

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);

		// Set font
		// dejavusans is a UTF-8 Unicode font, if you only need to
		// print standard ASCII chars, you can use core fonts like
		// helvetica or times to reduce file size.
		$pdf->SetFont('times','',10,'',true);

		// Add a page
		// This method has several options, check the source code documentation for more information.
		$pdf->AddPage("L");
		// Set some content to print

		$nama_peminjam = $peminjaman['nama_peminjam'];
		$tanggal_peminjaman = $peminjaman['tanggal_peminjaman'];
		$tanggal_pengembalian = $peminjaman['tanggal_pengembalian'];		
		$petugas = $this->session->userdata('nama_lengkap');
		$petugas_satker = $this->session->userdata('nama_unit_kerja');
		
	$html = <<<EOD
<div align="center" width="100%"><h3>Form Bukti Peminjaman Arsip</h3></div><br/>
<table width="100%">
<tr><td width="130">Nama </td><td>: <b>$nama_peminjam</b></td></tr>
<tr><td width="130">Tanggal Peminjaman </td><td>: <b>$tanggal_peminjaman</b></td></tr>
<tr><td width="130">Tanggal Pengembalian </td><td>: <b>$tanggal_pengembalian</b></td></tr>    
</table>
    <hr><hr><br><br><br><br><br><br>
<h3>Daftar Arsip Yang Dipinjam</h3>    
<br>
 <table width="100%"><tr bgColor="#aaaaff" align="center"><th width="30">#</th><th width="95"><b>Nomor Definitif</b></th><th width="300"><b>Uraian</b></th><th width="95"><b>Kode Klasifikasi</b></th><th width="160"><b>Unit Kerja</b></th><th width="140"><b>Tingkat Perkembangan</b></th><th width="180"><b>Lokasi Simpan</b><br/>Depo|Ruang|Lemari|Boks|Folder</th></tr>    
EOD;


$no =1;
 foreach($id_arsip_pinjam as $value) {     
 
 $arsip = $this->peminjaman_model->_getArsipById($value);
          
$html.= <<<EOD
 <tr><td>$no</td><td>{$arsip['nomor_definitif']}</td><td>{$arsip['uraian']}</td><td>{$arsip['kode_klasifikasi']}</td><td>{$arsip['kode_unit_kerja']} - {$arsip['unit_kerja']}</td><td>{$arsip['tingkat_perkembangan']}</td><td>{$arsip['lokasi_simpan']}</td></tr>
EOD;
$no++;
      }
$html .= <<<EOD
      </table>
          <br><br><br><br><br><br>
<table width="100%"><tr>
<td align="center">Peminjam <br><br><br><br> <b><u>$nama_peminjam</u></b></td><td align="center">Petugas<br><br><br><br> <b><u>$petugas</u></b><br><b>$petugas_satker</br></td>

</tr></table>
EOD;

		// Print text using writeHTMLCell()
		$pdf->writeHTMLCell($w = 0,$h = 0,$x = '',$y = '',$html,$border = 0,$ln = 1,$fill = 0,$reseth = true,$align = '',$autopadding = true);
		//$pdf->writeHTML($html, true, 0, true, true);

		// ---------------------------------------------------------

		// Close and output PDF document
		// This method has several options, check the source code documentation for more information.
		$kode_peminjaman = str_replace('.', '', $kode_peminjaman);
		$pdf->Output("Form Peminjaman_".$kode_peminjaman,'I');

		//============================================================+
		// END OF FILE
		//============================================================+

	}

}


class MYPDF extends TCPDF {

        var $id;
        var $indeks;
        var $style = array('border'=>0,'vpadding'=>0,'hpadding'=>0,'fgcolor'=> array(0,0,0),'bgcolor'=>false,//array(255,255,255)
		'module_width'=>1,// width of a single module in points
		'module_height'=>1 // height of a single module in points
		);
        var $code;
		  
	//Page header
	public function Header() {
        //set Image Background (watermark)
        $img_file = K_PATH_IMAGES.'logo.png';
        $this->Image($img_file, 85, 30, 108, 160, '', '', 'center', true, 300, '', false, false, 0);
    }
	// Page footer
	public function Footer() {
            
            $html = "<div align='right' width='100%'><sub>".WEB_TITLE." - ".date("Y")."</sub></div>";
            $this->writeHTML($html, true, 0, true, true);
            
            $code = $this->code;
				$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);
				$this->write2DBarcode($code, 'QRCODE,H', 270, 182, 22, 22, $style, 'N');
				$this->Text(255, 202, $code);		
	}
}


/* End of file login.php */
/* Location: ./application/controllers/login.php */
