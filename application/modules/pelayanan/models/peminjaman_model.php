<?php
class Peminjaman_model extends CI_Model {
	/**
	 * Constructor
	 */
	function __construct() {
		parent::__construct();
	}

	public function _processPeminjaman($data = array(),$id_arsip_peminjaman = array()) {
		$data['kode_peminjaman'] = $this->_getKodePeminjaman($data['kode_unit_kerja'],$data['tanggal_peminjaman'],$data['tanggal_pengembalian']);
		$data['tanggal_peminjaman'] = Tanggal::sqlDate($data['tanggal_peminjaman']);
		$data['tanggal_pengembalian'] = Tanggal::sqlDate($data['tanggal_pengembalian']);
		$this->db->insert('peminjaman',$data);

		$new_id_peminjaman = $this->_getMaxIdPeminjaman();
		foreach($id_arsip_peminjaman as $value) {
			$arsip = array();
			$arsip['id_arsip'] = $value;
			$arsip['id_peminjaman'] = $new_id_peminjaman;
			$this->db->insert('peminjaman_detail',$arsip);
		}

		$result = array();
		$result['id_peminjaman'] = $new_id_peminjaman;
		$result['kode_peminjaman'] = $data['kode_peminjaman'];

		return $result;

	}

	public function _getKodePeminjaman($kode_unit_kerja,$tanggal_peminjaman,$tanggal_pengembalian) {
		$ai = $this->_getMaxIdPeminjaman();
		$next_id_peminjaman = $ai + 1;
		$tanggal_peminjaman = str_replace('-','',$tanggal_peminjaman);
		$tanggal_pengembalian = str_replace('-','',$tanggal_pengembalian);
		$kode = $kode_unit_kerja . '.' . $tanggal_peminjaman . '.' . $tanggal_pengembalian . '.' . $next_id_peminjaman;
		return $kode;
	}

	private function _getMaxIdPeminjaman() {
		$this->db->select('MAX(id_peminjaman) as maks');
		$result = $this->db->get('peminjaman')->row_array();
		if($result == 0)
			$result = 1;
		return $result['maks'];
	}

	public function _getPeminjamanById($id_peminjaman) {
		$this->db->where('id_peminjaman',$id_peminjaman);
		$result = $this->db->get('peminjaman')->row_array();
		return $result;
	}

	public function _getIdArsipById($id_peminjaman) {
		$this->db->select('id_arsip');
		$this->db->where('id_peminjaman',$id_peminjaman);
		$result = $this->db->get('peminjaman_detail')->result_array();
		$id_arsip = array();
		foreach($result as $value) {
			$id_arsip[] = $value['id_arsip'];
		}

		return $id_arsip;
	}

	public function _getArsipById($id_arsip) {
		$this->db->select("nama_unit_kerja as unit_kerja, arsip.kode_unit_kerja, id_arsip, nomor_definitif, uraian, kode_klasifikasi, tingkat_perkembangan, nomor_depo, nomor_ruang, nomor_lemari, nomor_boks, nomor_folder, CONCAT(`nomor_depo`,'|',`nomor_ruang`,'|',`nomor_lemari`,'|',`nomor_boks`,'|',`nomor_folder`) as lokasi_simpan",FALSE);
		$this->db->where("id_arsip",$id_arsip);
		$this->db->join('unit_kerja','unit_kerja.kode_unit_kerja = arsip.kode_unit_kerja');
		$result = $this->db->get('arsip')->row_array();
		return $result;
	}

	public function _loadPeminjaman() {
		$this->db->select("id_peminjaman, kode_peminjaman, jenis_peminjaman, nama_peminjam, peminjaman.kode_unit_kerja, nama_unit_kerja as unit_kerja,  nik, hp, email, tanggal_peminjaman, tanggal_pengembalian");
		$this->db->join('unit_kerja','unit_kerja.kode_unit_kerja = peminjaman.kode_unit_kerja');
		$this->db->where('status_otoritas','N');
		$this->db->where('status_pengembalian','N');
		if($this->session->userdata('id_role')!=1) {
				$this->db->where('peminjaman.kode_unit_kerja',$this->session->userdata('kode_unit_kerja'));
			}
		$peminjaman = $this->db->get('peminjaman')->result_array();

		$result = array();
		foreach($peminjaman as $value) {
			$id_arsip_pinjam = array();
			$id_arsip_pinjam = $this->_getIdArsipById($value['id_peminjaman']);

			$arsip = array();
			foreach($id_arsip_pinjam as $val_id_arsip) {
				$arsip[] = $this->_getArsipById($val_id_arsip);
			}

			$value['arsip'] = $arsip;
			$result[] = $value;
		}

		return $result;
	}
	
	public function _loadPengembalian() {
		$this->db->select("id_peminjaman, kode_peminjaman, nama_peminjam, peminjaman.kode_unit_kerja, nama_unit_kerja as unit_kerja,  nik, hp, email, tanggal_peminjaman, tanggal_pengembalian");
		$this->db->join('unit_kerja','unit_kerja.kode_unit_kerja = peminjaman.kode_unit_kerja');
		$this->db->where('status_otoritas','Y');
		$this->db->where('status_pengembalian','N');
		if($this->session->userdata('id_role')!=1) {
				$this->db->where('peminjaman.kode_unit_kerja',$this->session->userdata('kode_unit_kerja'));
			}
		$peminjaman = $this->db->get('peminjaman')->result_array();

		$result = array();
		foreach($peminjaman as $value) {
			$id_arsip_pinjam = array();
			$id_arsip_pinjam = $this->_getIdArsipById($value['id_peminjaman']);

			$arsip = array();
			foreach($id_arsip_pinjam as $val_id_arsip) {
				$arsip[] = $this->_getArsipById($val_id_arsip);
			}

			$value['arsip'] = $arsip;
			$result[] = $value;
		}

		return $result;
	}
	
	public function _loadAllPeminjaman() {
		$this->db->select("status_otoritas, id_peminjaman, kode_peminjaman, nama_peminjam, jenis_peminjaman, peminjaman.kode_unit_kerja, nama_unit_kerja as unit_kerja,  nik, hp, email, tanggal_peminjaman, tanggal_pengembalian");
		$this->db->where('status_pengembalian','N');
		$this->db->where('jenis_peminjaman','peminjaman');
		$this->db->join('unit_kerja','unit_kerja.kode_unit_kerja = peminjaman.kode_unit_kerja');
		if($this->session->userdata('id_role')!=1) {
				$this->db->where('peminjaman.kode_unit_kerja',$this->session->userdata('kode_unit_kerja'));
			}
		$peminjaman = $this->db->get('peminjaman')->result_array();

		$result = array();
		foreach($peminjaman as $value) {
			$id_arsip_pinjam = array();
			$id_arsip_pinjam = $this->_getIdArsipById($value['id_peminjaman']);

			$arsip = array();
			foreach($id_arsip_pinjam as $val_id_arsip) {
				$arsip[] = $this->_getArsipById($val_id_arsip);
			}

			$value['arsip'] = $arsip;
			$result[] = $value;
		}

		return $result;
	}
        
        public function _loadAllPenggandaan() {
		$this->db->select("status_otoritas, id_peminjaman, kode_peminjaman, nama_peminjam, jenis_peminjaman, peminjaman.kode_unit_kerja, nama_unit_kerja as unit_kerja,  nik, hp, email, tanggal_peminjaman, tanggal_pengembalian");
		$this->db->where('status_pengembalian','N');
		$this->db->where('jenis_peminjaman','penggandaan');
		$this->db->join('unit_kerja','unit_kerja.kode_unit_kerja = peminjaman.kode_unit_kerja');
		if($this->session->userdata('id_role')!=1) {
				$this->db->where('peminjaman.kode_unit_kerja',$this->session->userdata('kode_unit_kerja'));
			}
		$peminjaman = $this->db->get('peminjaman')->result_array();

		$result = array();
		foreach($peminjaman as $value) {
			$id_arsip_pinjam = array();
			$id_arsip_pinjam = $this->_getIdArsipById($value['id_peminjaman']);

			$arsip = array();
			foreach($id_arsip_pinjam as $val_id_arsip) {
				$arsip[] = $this->_getArsipById($val_id_arsip);
			}

			$value['arsip'] = $arsip;
			$result[] = $value;
		}

		return $result;
	}
	
	public function _loadAllPelayanan() {
		$this->db->select("status_otoritas, status_pengembalian, id_peminjaman, kode_peminjaman, nama_peminjam, peminjaman.kode_unit_kerja, nama_unit_kerja as unit_kerja,  nik, hp, email, tanggal_peminjaman, tanggal_pengembalian");
		$this->db->join('unit_kerja','unit_kerja.kode_unit_kerja = peminjaman.kode_unit_kerja');
		if($this->session->userdata('id_role')!=1) {
				$this->db->where('peminjaman.kode_unit_kerja',$this->session->userdata('kode_unit_kerja'));
			}
		$peminjaman = $this->db->get('peminjaman')->result_array();

		$result = array();
		foreach($peminjaman as $value) {
			$id_arsip_pinjam = array();
			$id_arsip_pinjam = $this->_getIdArsipById($value['id_peminjaman']);

			$arsip = array();
			foreach($id_arsip_pinjam as $val_id_arsip) {
				$arsip[] = $this->_getArsipById($val_id_arsip);
			}

			$value['arsip'] = $arsip;
			$result[] = $value;
		}

		return $result;
	}
	
	public function _getPeminjaman($id_peminjaman) {
		$this->db->select("id_peminjaman, kode_peminjaman, nama_peminjam, jenis_peminjaman, peminjaman.kode_unit_kerja, nama_unit_kerja as unit_kerja,  nik, hp, email, tanggal_peminjaman, tanggal_pengembalian");
		$this->db->join('unit_kerja','unit_kerja.kode_unit_kerja = peminjaman.kode_unit_kerja');
		//$this->db->where('status_otoritas','N');
		//$this->db->where('status_pengembalian','N');
		$this->db->where('id_peminjaman',$id_peminjaman);
		if($this->session->userdata('id_role')!=1) {
				$this->db->where('peminjaman.kode_unit_kerja',$this->session->userdata('kode_unit_kerja'));
			}
		$peminjaman = $this->db->get('peminjaman')->row_array();

		$result = array();
			$id_arsip_pinjam = array();
			$id_arsip_pinjam = $this->_getIdArsipById($peminjaman['id_peminjaman']);

			$arsip = array();
			foreach($id_arsip_pinjam as $val_id_arsip) {
				$arsip[] = $this->_getArsipById($val_id_arsip);
			}

			$peminjaman['arsip'] = $arsip;
			$result = $peminjaman;
	
		return $result;
	}

	public function processAccept($id_peminjaman) {

		$id_arsip_peminjaman = $this->_getIdArsipById($id_peminjaman);

		$data = array();
		$data['status_otoritas'] = 'Y';
		$this->db->update('peminjaman',$data,'id_peminjaman = ' . $id_peminjaman);		

		foreach($id_arsip_peminjaman as $value) {
			$data_arsip = array();
			$data_arsip['status_pinjam'] = 'Y';
			$this->db->update('arsip',$data_arsip,'id_arsip = ' . $value);
			Master_model::logs('PEMINJAMAN',$value);
		}
	}

  public function processReject($id_peminjaman) {
  	
  	 $this->db->delete('peminjaman','id_peminjaman = ' . $id_peminjaman);
  	 $this->db->delete('peminjaman_detail','id_peminjaman = ' . $id_peminjaman);
	 
  }
  
  public function processPengembalian($id_peminjaman) {
  		
  	$id_arsip_peminjaman = $this->_getIdArsipById($id_peminjaman);

		$data = array();
		$data['status_pengembalian'] = 'Y';
		$this->db->update('peminjaman',$data,'id_peminjaman = ' . $id_peminjaman);		

		foreach($id_arsip_peminjaman as $value) {
			$data_arsip = array();
			$data_arsip['status_pinjam'] = 'N';
			$this->db->update('arsip',$data_arsip,'id_arsip = ' . $value);
			Master_model::logs('PENGEMBALIAN',$value);
		}
  }


}
