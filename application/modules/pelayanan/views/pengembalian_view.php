<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- start: HEAD -->

    <?php echo Modules::run('templates/'.TEMPLATE.'/meta_css'); ?>
    <!-- end: HEAD -->
    <!-- start: BODY -->
    <body>
        <!-- start: HEADER -->
        <?php echo Modules::run('templates/'.TEMPLATE.'/header'); ?>
        	<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugins/select2/select2.css" />
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/DataTables/media/css/DT_bootstrap.css" />
                <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/gritter/css/jquery.gritter.css">
                <link href="<?php echo base_url() ?>assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
		<link href="<?php echo base_url() ?>assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
            <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/select2/select2.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/datepicker/css/datepicker.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/jQuery-Tags-Input/jquery.tagsinput.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/summernote/build/summernote.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/ckeditor/contents.css">
		
		<script type="text/javascript" src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>
      <link type="text/css" href="<?php echo base_url() ?>assets/library/gb/greybox.css" rel="stylesheet" />		
                <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
        <!-- end: HEADER -->
        <!-- start: MAIN CONTAINER -->
        <div class="main-container" style="margin-top:150px;">
            <?php echo Modules::run('templates/'.TEMPLATE.'/menu'); ?>
            <!-- start: PAGE -->
            <div class="main-content">

                <div class="container">
                    <!-- start: PAGE HEADER -->
                    <div class="row">
                        <div class="col-sm-12">							
                            <!-- start: PAGE TITLE & BREADCRUMB -->
                            <ol class="breadcrumb">
                                <li>
                                    <i class="clip-stack"></i>
                                    <a href="#">
                                        Pelayanan
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Pengembalian
                                    </a>
                                </li>
                                
                            </ol>

                            <div class="page-header">
                                <h1>Pengembalian</h1>
                                <p>Fungsi modul untuk mencatat pengembalian fisik arsip/dokumen inaktif di Pusat Arsip yang dilakukan oleh Unit Pengolah. &nbsp; <a href="#modalMore" data-toggle="modal" class="btn btn-default btn-xs"> <i class="clip-expand"></i> </a> </p>
                            </div>
                            <!-- end: PAGE TITLE & BREADCRUMB -->
                            <p align="right">
                                <!--<a href="#modalImportExcel" data-toggle="modal" class="btn btn-success"><i class="clip-file-excel "></i> Import From Excel </a> &nbsp;-->
                                <!--<a href="#modalAddItem" data-toggle="modal" class="btn btn-primary"><i class="fa fa-plus"></i> Pengembalian </a>-->
                            </p>
                            
                             <div class="row">
                                                    <div class="col-sm-12">
                                                            
											<div id="demo"> </div>	
																							
									</div>
                                                        <!-- end fade modal detail uraian -->
        
		
		
        
                                                        
                                                        
                                                        <div id="modalMore" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					&times;
				</button>
				<h4 class="modal-title">Modul Penerimaan</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
                                            <p>
                                                    Petunjuk Pemakaian : <br/>
    Proses penerimaan fisik arsip/dokumen : isi atau pilih field pada tabel, untuk field yang berwarna atau mandatory berarti harus diisi kemudian klik tombol Search untuk menampilkan data arsip/dokumen yang telah disetujui oleh Unit Kerja untuk disimpan di Pusat Arsip, isi kode lokasi simpan fisik arsip/dokumen pada kolom tabel daftar arsip/dokumen pindah, klik tombol 'Pemindahan' untuk menyimpan data.
                                            </p>
                                            <p>
    Keterangan Field : <br/>
    Unit Pengolah : Unit kerja yang memindahkan arsip/dokumen ke pusat arsip <br/>
    Tahun Pindah : Tahun dipindahkan ke pusat arsip <br/>
    Lokasi Simpan : Kode default lokasi simpan fisik arsip/dokumen
                                            </p> <p>
    Contoh Pengisian : <br/>
    Unit Pengolah : Pilih <br/>
    Tahun Pindah : Pilih <br/>
    Lokasi Simpan : D1.20 [Ruang.Rak] / 000 [No. Boks]

                                            </p>
					</div>
					
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-light-grey">
					Close
				</button>
				<button type="button" class="btn btn-blue">
					Process Import
				</button>
			</div>
		</div>
                                                        
                                                        
                                                    </div>
                                                   
                                                 </div>

                        </div>
                    </div>
                    <!-- end: PAGE HEADER -->
                    <!-- start: PAGE CONTENT -->



                    <!-- end: PAGE CONTENT-->
                </div>
            </div>
            <!-- end: PAGE -->
        </div>
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <?php echo Modules::run('templates/'.TEMPLATE.'/footer'); ?>
        <!-- end: FOOTER -->

        <!-- start: MAIN JAVASCRIPTS -->
        <?php echo Modules::run('templates/'.TEMPLATE.'/js'); ?>
        <!-- end: MAIN JAVASCRIPTS -->
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/select2/select2.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
		<script src="<?php echo base_url() ?>assets/js/table-data.js"></script>
                <script src="<?php echo base_url() ?>assets/plugins/bootstrap-paginator/src/bootstrap-paginator.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery.pulsate/jquery.pulsate.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/gritter/js/jquery.gritter.min.js"></script>
		<script src="<?php echo base_url() ?>assets/js/ui-elements.js"></script>
                <script src="<?php echo base_url() ?>assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
		<script src="<?php echo base_url() ?>assets/js/ui-modals.js"></script>
                <script src="<?php echo base_url() ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/autosize/jquery.autosize.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/select2/select2.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery-maskmoney/jquery.maskMoney.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-colorpicker/js/commits.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/summernote/build/summernote.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/ckeditor/ckeditor.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
		<script src="<?php echo base_url() ?>assets/js/form-elements.js"></script>
        <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script>
                                    jQuery(document).ready(function() {
                                        Main.init();
                                        TableData1.init();
                                        UIElements.init();
                                        FormElements.init();
                                        
                                        $('#demo').html( '<table border="0" class="table table-striped table-bordered table-hover table-full-width" id="example"></table>' );
    $('#example').dataTable( {
        "aaData": [
            /* Reduced data set */
            <?php 
            $no = 1;
            foreach($peminjaman as $row) { ?>
            [ 
                "<?php echo $no ?>", 
                "<?php echo $row['kode_peminjaman'] ?>", 
                "<?php echo $row['nama_peminjam'] ?>", 
                "<?php echo $row['kode_unit_kerja'].' | '.$row['unit_kerja'] ?>", 
                "<a href='#' role='button' onclick='formDetil(<?php echo $row['id_peminjaman'] ?>);' class='btn btn-default' > Detail </a>",
                '<a onclick="return confirm(\'Anda yakin akan Memproses Pengembalian?\')" class="btn btn-green" href="<?php echo base_url() ?>pelayanan/pengembalian/proses/<?php echo $row['id_peminjaman'] ?>"><i class="glyphicon glyphicon-ok-sign"></i></a>'
                ],
            <?php $no++; } ?>        
        ],
       
        "oLanguage": {
                        "sLengthMenu": "Show _MENU_ Rows",
                        "sSearch": "",
                        "oPaginate": {
                            "sPrevious": "",
                            "sNext": ""
                        }
                    },
                  
                    "aoColumns": [
                        {"sTitle": "No", "sClass": "center"},
                        {"sTitle": "Kode Peminjaman"},
                        {"sTitle": "Nama Peminjam"},
                        {"sTitle": "Unit Kerja", "sClass": "center"},
                        {"sTitle": "Detail Arsip", "sClass": "center"},
                        {"sTitle": "Proses Pengembalian", "sClass": "center"}
                    ],
                    // set the initial value
                    "iDisplayLength": 10,
    } );  
                                        
                                    });
        </script>
        <script>
			function formDetil(id) {
            GB_show("Detil Data Arsip", '<?php echo base_url(); ?>pelayanan/otoritas/detil/'+id, 900, 1070);
           $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
		</script>
  
    </body>
    <!-- end: BODY -->

    <!-- Mirrored from www.cliptheme.com/clip-one/ by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 16 Nov 2013 08:37:43 GMT -->
</html>