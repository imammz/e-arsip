<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- start: HEAD -->

    <?php echo Modules::run('templates/' . TEMPLATE . '/meta_css'); ?>
    <!-- end: HEAD -->
    <!-- start: BODY -->
    <body>
        <!-- start: HEADER -->
        <?php echo Modules::run('templates/' . TEMPLATE . '/header'); ?>
        	<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugins/select2/select2.css" />
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/DataTables/media/css/DT_bootstrap.css" />
                <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/gritter/css/jquery.gritter.css">
                <link href="<?php echo base_url() ?>assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
		<link href="<?php echo base_url() ?>assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
            <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/select2/select2.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/datepicker/css/datepicker.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/jQuery-Tags-Input/jquery.tagsinput.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/summernote/build/summernote.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/ckeditor/contents.css">
		
		<script type="text/javascript" src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>
      <link type="text/css" href="<?php echo base_url() ?>assets/library/gb/greybox.css" rel="stylesheet" />
                <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
        <!-- end: HEADER -->
        <!-- start: MAIN CONTAINER -->
        <div class="main-container" style="margin-top:150px;">
            <?php echo Modules::run('templates/' . TEMPLATE . '/menu'); ?>
            <!-- start: PAGE -->
            <div class="main-content">

                <div class="container">
                    <!-- start: PAGE HEADER -->
                    <div class="row">
                        <div class="col-sm-12">							
                            <!-- start: PAGE TITLE & BREADCRUMB -->
                            <ol class="breadcrumb">
                                <li>
                                    <i class="clip-stack"></i>
                                    <a href="#">
                                        Pelayan
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Penggandaan
                                    </a>
                                </li>

                                
                            </ol>

                            <div class="page-header">
                                <h1>Penggandaan</h1>
                                <p>Fungsi modul untuk mencatat Penggandaan Softcopy arsip/dokumen inaktif di Pusat Arsip yang dilakukan oleh Unit Pengolah. &nbsp; <a href="#modalMore" data-toggle="modal" class="btn btn-default btn-xs"> <i class="clip-expand"></i> </a> </p>
                            </div>
                            <!-- end: PAGE TITLE & BREADCRUMB -->
                            <p align="left">
								
                                 <form role="form" id="form-entry" class="form-horizontal" method="POST" action="<?php echo base_url() ?>pelayanan/peminjaman/result" enctype="multipart/form-data">
                                               
                            <table align="center" width="80%" >
									
									<tr width="100%">
										<td width="30%" valign="top">Kode Klasifikasi</td>
										<td width="5%" valign="top">:</td>
										<td colspan="2">
											 <select id="kode_klasifikasi" name="kode_klasifikasi" class="form-control search-select">
                                                                             <option value="" selected>-- Semua Kode Klasifikasi --</option>
                                                                            <?php foreach ($klasifikasi as $row) { ?>

                                                                                <option value="<?php echo $row['kode_klasifikasi'] ?>"><?php echo $row['kode_klasifikasi'] ?> - <?php echo $row['nama_klasifikasi'] ?></option>

                                                                            <?php } ?>

                                                                        </select>
										</td>
									</tr>
									
									<tr width="100%">
										<td width="30%" valign="top">Unit Pengolah</td>
										<td width="5%" valign="top">:</td>
										<td colspan="2">
											<select class="form-control search-select" id="kode_unit_kerja" name="kode_unit_kerja">
                                                                            <option value="" selected>--Semua Unit Pengelola--</option>
                                                                            <?php foreach ($unit_kerja as $row) { ?>

                                                                                <option value="<?php echo $row['kode_unit_kerja'] ?>"><?php echo $row['kode_unit_kerja'] ?> - <?php echo $row['nama_unit_kerja'] ?></option>

                                                                            <?php } ?>
                                                                        </select>
										</td>
										
									</tr>
									<tr width="100%">
										<td width="30%" valign="top">Uraian</td>
										<td width="5%" valign="top">:</td>
										<td colspan="2">
											       <textarea class="autosize form-control" id="uraian" name="uraian" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 69px;"></textarea>
                                                                 
										</td>
									</tr>
									<tr width="100%">
										<td width="30%" valign="top">Kurun Waktu</td>
										<td width="5%" valign="top">:</td>
										<td width="30%"> Awal :
											       <input type="text" placeholder="Kurun Waktu Awal Arsip" id="kurun_waktu_awal" name="kurun_waktu_awal" value="" class="form-control" number="true" requered="true">
                                                                                              
										</td>
										<td width="30%"> Akhir :
											       <input type="text" placeholder="Kurun Waktu Akhir Arsip" id="kurun_waktu_akhir" name="kurun_waktu_akhir" value="" class="form-control" number="true" requered="true">
                                                                 
										</td>
									</tr>
									
                                                                        <tr width="100%">
                                                                            <td width="30%" valign="top">Lokasi Simpan</td>
										<td width="5%" valign="top">:</td>
										
                                                                            <td>
                                                                                   <div class="row">
                                            <div class="col-md-12">
                                                                                <p>
								<div class="form-group">
                                                                    <label class="control-label" for="nomor_depo">Nomor Depo</label>
                                                                    <div class="">
                                                                    <select id="nomor_depo" name="nomor_depo" class="form-control" onchange="changeDepo()">  <!--  for search select -->
                                                                            <option value="" selected>-- Semua Lokasi Depo --</option>
                                                                            <?php foreach ($depo as $row) { ?>

                                                                                <option value="<?php echo $row['nomor_depo'] ?>"> <?php echo $row['nomor_depo'] ?> - <?php echo $row['nama_depo'] ?> </option>

                                                                            <?php } ?>
                                                                        </select>
                                                                         </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label" for="nomor_ruang">Nomor Ruang</label>
                                                                    <div class="">
                                                                    <select id="nomor_ruang" name="nomor_ruang" class="form-control" onchange="changeRuang()">  <!--  for search select -->
                                                                            <option value="" selected>-- Semua Lokasi Ruang --</option>                                                                           
                                                                     </select>
                                                                        </div>
                                                                </div>	
                                                                <div class="form-group">
                                                                    <label class="control-label" for="nomor_lemari">Nomor Lemari</label>
                                                                    <div class="">
                                                                    <select id="nomor_lemari" name="nomor_lemari" class="form-control" onchange="changeLemari()">  <!--  for search select -->
                                                                            <option value="" selected>-- Semua Lokasi Lemari --</option>                                                                           
                                                                     </select>
                                                                      </div>
                                                                </div>	
                                                                <div class="form-group">
                                                                    <label class="control-label" for="nomor_boks">Nomor Boks</label>
                                                                    <div class="">
                                                                    <select id="nomor_boks" name="nomor_boks" class="form-control">  <!--  for search select -->
                                                                            <option value="" selected>-- Semua Lokasi Boks --</option>                                                                           
                                                                     </select>                                                                         
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label" for="nomor_folder">Nomor Folder  </label>
                                                                    <div>
                                                                        <input type="text" placeholder="Nomor Folder" id="nomor_folder" name="nomor_folder" class="form-control">
                                                                 </div>
                                                                </div>                                                
                                                                                                                
                                                                                                                
                                                                                                                </p>
                                            </div> </div>
                                                                            </td>
                                                                        </tr>
									
								
									
									<tr width="100%">
										<td width="30%" valign="top" colspan="2"></td>
										<td style="text-align: right">
											<input type="submit" class="btn btn-blue" value="SEARCH"/>
										</td>
									</tr>
									
								</table>
                                 </form>
                             </p>
                             <br/>
                             <h2>Daftar Permintaan Penggandaan Arsip</h2>
                             <p>
                             	<div id="demo"> </div>	
                             </p>
				
                        </div>
                    </div>
                    <!-- end: PAGE HEADER -->
                    <!-- start: PAGE CONTENT -->



                    <!-- end: PAGE CONTENT-->
                </div>
            </div>
            <!-- end: PAGE -->
        </div>
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <?php echo Modules::run('templates/' . TEMPLATE . '/footer'); ?>
        <!-- end: FOOTER -->

        <!-- start: MAIN JAVASCRIPTS -->
        <?php echo Modules::run('templates/' . TEMPLATE . '/js'); ?>
        <!-- end: MAIN JAVASCRIPTS -->
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/select2/select2.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
		<script src="<?php echo base_url() ?>assets/js/table-data.js"></script>
                <script src="<?php echo base_url() ?>assets/plugins/bootstrap-paginator/src/bootstrap-paginator.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery.pulsate/jquery.pulsate.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/gritter/js/jquery.gritter.min.js"></script>
		<script src="<?php echo base_url() ?>assets/js/ui-elements.js"></script>
                <script src="<?php echo base_url() ?>assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
		<script src="<?php echo base_url() ?>assets/js/ui-modals.js"></script>
                <script src="<?php echo base_url() ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/autosize/jquery.autosize.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/select2/select2.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery-maskmoney/jquery.maskMoney.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-colorpicker/js/commits.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/summernote/build/summernote.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/ckeditor/ckeditor.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
		<script src="<?php echo base_url() ?>assets/js/form-elements.js"></script>
		
		<script type="text/javascript" src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>
      <link type="text/css" href="<?php echo base_url() ?>assets/library/gb/greybox.css" rel="stylesheet" />
        <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script>
                                    			jQuery(document).ready(function() {
                                        Main.init();
                                        TableData1.init();
                                        UIElements.init();
                                        FormElements.init();
                                        
    $('#demo').html( '<table border="0" class="table table-striped table-bordered table-hover table-full-width" id="example"></table>' );
    $('#example').dataTable( {
        "aaData": [
            /* Reduced data set */
            <?php 
            $no = 1;
            foreach($peminjaman as $row) { 
            	$status = '<span class="label label-danger"> Menunggu Persetujuan</span>';
					if($row['status_otoritas']=='Y') $status = '<span class="label label-success">Disetujui</span>';
            	
					$cetak = '-';
					if($row['status_otoritas']=='Y') $cetak = '<a class="btn btn-bricky" href="#" onclick="formCetak('.$row['id_peminjaman'].');"><i class="clip-file-pdf"></i>Cetak</a>';
            	
            	?>
            [ 
                "<?php echo $no ?>", 
                "<?php echo $row['kode_peminjaman'] ?>", 
                "<?php echo $row['nama_peminjam'] ?>", 
                "<?php echo $row['kode_unit_kerja'].' | '.$row['unit_kerja'] ?>", 
                "<?php echo $row['jenis_peminjaman'] ?>", 
                "<a href='#' role='button' onclick='formDetil(<?php echo $row['id_peminjaman'] ?>);' class='btn btn-default' > Detail </a>",
                '<?php echo $status; ?>',
                '<?php echo $cetak; ?>'
                ],
            <?php $no++; } ?>        
        ],
       
        "oLanguage": {
                        "sLengthMenu": "Show _MENU_ Rows",
                        "sSearch": "",
                        "oPaginate": {
                            "sPrevious": "",
                            "sNext": ""
                        }
                    },
                  
                    "aoColumns": [
                        {"sTitle": "No", "sClass": "center"},
                        {"sTitle": "Kode Peminjaman"},
                        {"sTitle": "Nama Peminjam"},
                        {"sTitle": "Unit Kerja", "sClass": "center"},
                        {"sTitle": "Jenis Peminjaman", "sClass": "center"},
                        {"sTitle": "Detail Arsip", "sClass": "center"},
                        {"sTitle": "Status", "sClass": "center"},
                        {"sTitle": "Bukti Peminjaman", "sClass": "center"}
                    ],
                    // set the initial value
                    "iDisplayLength": 10,
    } );   
                                        
                                    });
                                    
                                    
                                     function changeDepo() {
        
        var get_nomor_depo = $('#nomor_depo').val();
        
        var url = '<?php echo base_url() ?>pemindahan/entry/comboRuangByDepo/'+get_nomor_depo;
        $.post(url,{
            nomor_depo: get_nomor_depo },
            function(data, status) { 
                 var data_ruang = '<option value="" selected>-- Semua Lokasi Ruang --</option>';  
                 if(data.result) { 
                     
                    $.each(data.ruang, function () {
                            data_ruang += '<option value="'+this.nomor_ruang+'"> '+this.nomor_ruang+' </option>';
                    });                           
        }   
        $('#nomor_ruang').html(data_ruang);
        changeRuang();
        changeLemari()
        
    },"json"
            );
    }
    
    function changeRuang() {
        
        var get_nomor_depo = $('#nomor_depo').val();
        var get_nomor_ruang = $('#nomor_ruang').val();        
       
        
        var url = '<?php echo base_url() ?>pemindahan/entry/comboLemariByRuang/'+get_nomor_depo+'/'+get_nomor_ruang;
        $.post(url,{
            nomor_depo: get_nomor_depo, 
            nomor_ruang: get_nomor_ruang 
        
    },
            function(data, status) { 
                
         var data_lemari = '<option value="" selected>-- Semua Lokasi Lemari --</option>';
               if(data.result) { 
                     
                                        
                  
                    $.each(data.lemari, function () {
                            data_lemari += '<option value="'+this.nomor_lemari+'"> '+this.nomor_lemari+' </option>';
                    });
                  
                     
                     
        } 
        $('#nomor_lemari').html(data_lemari);
        changeLemari();
    },"json"
            );
    }
    
    function changeLemari() {
        
        var get_nomor_depo = $('#nomor_depo').val();
        var get_nomor_ruang = $('#nomor_ruang').val();        
        
        var get_nomor_lemari = $('#nomor_lemari').val();
        
        var url = '<?php echo base_url() ?>pemindahan/entry/comboBoksByLemari/'+get_nomor_depo+'/'+get_nomor_ruang+'/'+get_nomor_lemari;
        $.post(url,{
            nomor_depo: get_nomor_depo, 
            nomor_ruang: get_nomor_ruang, 
            nomor_lemari: get_nomor_lemari 
        
    },
            function(data, status) { 
                 
        var data_boks = '<option value="" selected>-- Semua Lokasi Boks --</option>'; 
                       if(data.result) { 
                           
                    $.each(data.boks, function () {
                            data_boks += '<option value="'+this.nomor_boks+'"> '+this.nomor_boks+' </option>';
                    });
        }
         $('#nomor_boks').html(data_boks);
    },"json"
            );
    }
        </script>
        
        <script>
			function formDetil(id) {
            GB_show("Detil Data Arsip", '<?php echo base_url(); ?>pelayanan/otoritas/detil/'+id, 900, 1070);
           $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
        
        function formCetak(id) {
            GB_show("Detil Data Arsip", '<?php echo base_url(); ?>pelayanan/peminjaman/pdfFormPeminjaman/'+id, 900, 1070);
           $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
		</script>
  
    </body>
    <!-- end: BODY -->

    <!-- Mirrored from www.cliptheme.com/clip-one/ by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 16 Nov 2013 08:37:43 GMT -->
</html>