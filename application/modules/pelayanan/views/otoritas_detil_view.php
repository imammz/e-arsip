<?php echo Modules::run('templates/' . TEMPLATE . '/meta_css'); ?>
<!-- end: HEAD -->
<!-- start: BODY -->
<body onload="changeDepoStart()">
    <!-- start: HEADER -->

    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugins/select2/select2.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/DataTables/media/css/DT_bootstrap.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/gritter/css/jquery.gritter.css">
    <link href="<?php echo base_url() ?>assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/select2/select2.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/datepicker/css/datepicker.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/jQuery-Tags-Input/jquery.tagsinput.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/summernote/build/summernote.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/ckeditor/contents.css">

    <script type="text/javascript" src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>
    <link type="text/css" href="<?php echo base_url() ?>assets/library/gb/greybox.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/plugins/colorbox/example2/colorbox.css">
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: HEADER -->
    <!-- start: MAIN CONTAINER -->
    
        <div style="background-color: #ffffff">

                    <div class="tabbable">
                        <ul id="myTab4" class="nav nav-tabs tab-padding tab-space-3 tab-blue">
                                            <li class="active">
                                                <a href="#tab1" data-toggle="tab">
                                                    Arsip Yang Dipinjam
                                                </a>
                                            </li>
                        </ul>
                        <div class="tab-content">
                                            <div class="tab-pane in active" id="tab1">
                                        <p align="left">
                             <form method="post" class="form-horizontal">
                             	 <div class="form-group">
											<label class="col-sm-4 control-label" for="nama_peminjam">
												Nama Peminjam
											</label>
											<div class="col-sm-4">
												<strong>
												<?php echo $peminjaman['nama_peminjam'] ?>
												</strong>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label" for="kode_unit_kerja">
												Unit Kerja
											</label>
											<div class="col-sm-4">
												<strong>
										<?php echo $peminjaman['kode_unit_kerja'].' - '.$peminjaman['unit_kerja'] ?>
											</strong>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label" for="nik">
												NIP Peminjam
											</label>
											<div class="col-sm-2">
												<strong>
												<?php echo $peminjaman['nik'] ?>
												</strong>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label" for="hp">
												Nomor HP Peminjam
											</label>
											<div class="col-sm-4">
												<strong>
											<?php echo $peminjaman['nik'] ?>
											</strong>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label" for="email">
												Email Peminjam
											</label>
											<div class="col-sm-4">
												<strong>
											<?php echo $peminjaman['hp'] ?>
											</strong>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label" for="tanggal_peminjaman">
												Tanggal Peminjaman
											</label>
											<div class="col-sm-2">
												<strong>
											<?php echo Tanggal::formatDate($peminjaman['tanggal_peminjaman']) ?>
											</strong>
											</div>
											<label class="col-sm-2 control-label" for="tanggal_pengembalian">
												Tanggal Pengembalian
											</label>
											<div class="col-sm-2">
												<strong>
											<?php echo Tanggal::formatDate($peminjaman['tanggal_pengembalian']) ?>
											</strong>
											</div>
										</div>
										<div class="page-header">
                                <h3>Arsip-arsip yang dipinjam</h3>
                            </div>
                           <table class="table table-striped table-bordered table-hover table-full-width">
										<thead>
											<tr>
												<th>No</th>
												<th class="hidden-xs">Nomor Definitif</th>
												<th>Kode Klasifikasi</th>
												<th class="hidden-xs"> Unit Kerja</th>
												<th>Uraian</th>
												<th>Detail</th>
												<th> - - </th>
											</tr>
										</thead>
										<tbody>
									
									<?php 
									$no = 1;
									foreach($peminjaman['arsip'] as $row) { ?>		
											<tr>
												<td><?php echo $no ?></td>
												<td class="hidden-xs"><?php echo $row['nomor_definitif'] ?></td>
												<td><?php echo $row['kode_klasifikasi'] ?></td>
												<td class="hidden-xs"><?php echo $row['unit_kerja'] ?></td>												
												<td class="hidden-xs"><?php echo $row['uraian'] ?></td>
                                                                                                <td><a href='#' role='button' onclick='formDetilArsip(<?php echo $row['id_arsip']?>);' class='btn btn-default' > Detail </a></td>
                                                                                                <td class="hidden-xs"><?php if($peminjaman['jenis_peminjaman']=='penggandaan') { ?>   
                                                                                                    <a href="<?php echo base_url() ?>pemindahan/entry/hasilscann/<?php echo $row['id_arsip'] ?>" class="btn btn-success" > Download </a>
                                                                                                    <?php } ?> </td>
                                                                                        </tr>
									<?php $no++; } ?>		
                             </tbody>
									</table>
                                 <p class="text-center text-error" style="color:red; font-size: larger;">
                                     Dengan Mendownload File Arsip Tersebut Berarti Anda Setuju Untuk Menjaga Kerahasian Softcopy File Arsip.
                                     Apabila dikemudian hari didapatkan menyebarluaskan tanpa seizin Pihak Unit Pusat Kearsipan DKPP, Anda setuju untuk dikenakan sangsi sesuai dengan  Undang-undang yang berlaku.
                                 </p>
                           </form>		
                            
                             </p>                                                                              
                                            </div>
                            
                            
                            
                                    </div>
                                   

                



        </div>
        <!-- end: PAGE -->
   
   

    <!-- start: MAIN JAVASCRIPTS -->
    <?php echo Modules::run('templates/' . TEMPLATE . '/js'); ?>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
      <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/select2/select2.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
		<script src="<?php echo base_url() ?>assets/js/table-data.js"></script>
                <script src="<?php echo base_url() ?>assets/plugins/bootstrap-paginator/src/bootstrap-paginator.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery.pulsate/jquery.pulsate.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/gritter/js/jquery.gritter.min.js"></script>
		<script src="<?php echo base_url() ?>assets/js/ui-elements.js"></script>
                <script src="<?php echo base_url() ?>assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
		<script src="<?php echo base_url() ?>assets/js/ui-modals.js"></script>
                <script src="<?php echo base_url() ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/autosize/jquery.autosize.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/select2/select2.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery-maskmoney/jquery.maskMoney.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-colorpicker/js/commits.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/summernote/build/summernote.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/ckeditor/ckeditor.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
		<script src="<?php echo base_url() ?>assets/js/form-elements.js"></script>
        <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script>
                                    jQuery(document).ready(function() {
                                        Main.init();
                                        UIElements.init();
                                        FormElements.init();
                                        });
         
     function formDetilArsip(id) {
            GB_show("Detil Data Arsip", '<?php echo base_url(); ?>pemindahan/penerimaan/detil/'+id, 600, 800);
           $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
        
        </script>
    
</body>
<!-- end: BODY -->

<!-- Mirrored from www.cliptheme.com/clip-one/ by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 16 Nov 2013 08:37:43 GMT -->
</html>