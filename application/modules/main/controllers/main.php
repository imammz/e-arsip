<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Main extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->_checkLogin();
	}

	public function index() {

		$data = array();
		$data['class'] = 'main';
		$data['function'] = 'index';
		$data['url'] = base_url() . 'home/index';

		$this->load->view('templates/standard/content_view',$data);
	}

}


/* End of file login.php */
/* Location: ./application/controllers/login.php */
