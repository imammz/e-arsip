
                <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugins/select2/select2.css" />
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/DataTables/media/css/DT_bootstrap.css" />
                <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/gritter/css/jquery.gritter.css">
                <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
            
            <!-- start: PAGE -->
            <div class="main-content">

                
                <div class="container">
                    <!-- start: PAGE HEADER -->
                    <div class="row">
                        <div class="col-sm-12">							
                            <!-- start: PAGE TITLE & BREADCRUMB -->
                            <ol class="breadcrumb">
                                <li>
                                    <i class="clip-home-3 active"></i>
                                    <a href="#">
                                        Home
                                    </a>
                                </li>

                                <li class="search-box">
                                    <form class="sidebar-search">
                                        <div class="form-group">
                                            <input type="text" placeholder="Start Searching...">
                                            <button class="submit">
                                                <i class="clip-search-3"></i>
                                            </button>
                                        </div>
                                    </form>
                                </li>
                            </ol>

                            <div class="page-header">
                                <h1>Home</h1>
                                <marquee scrollamount="3"scrolldelay="10" onmouseover='this.stop()' onmouseout='this.start()'> Selamat Datang Di SISKA ( SISTEM INFORMASI KEARSIPAN ) - Dewan Kehormatan Penyelenggara Pemilu (DKPP) </marquee>
                            </div>
                            <!-- end: PAGE TITLE & BREADCRUMB -->

                            <div class="row">                                
                                <div class="col-sm-12">
                                    <div class="tabbable">
                                        <ul id="myTab4" class="nav nav-tabs tab-padding tab-space-3 tab-blue">
                                            <li class="active">
                                                <a href="#tab_dashboard" data-toggle="tab">
                                                    Dashboard
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab_jumlah_arsip" data-toggle="tab">
                                                    Statistik Jumlah Arsip Simpan & Usul Musnah
                                                </a>
                                            </li>                                         
                                            <li>
                                                <a href="#tab_perminjaman_arsip" data-toggle="tab">
                                                    Statistik Peminjaman Arsip
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab_arsip_unit_pengelola" data-toggle="tab">
                                                    Statistik Arsip Per-unit Pengelola
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab_arsip_lokasi_simpan" data-toggle="tab">
                                                    Statistik Arsip Per-lokasi Simpan
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane in active" id="tab_dashboard">
                                                <!--- content in tab 1 -->
                                                 <div class="row">
                                                    <div class="col-sm-6">	
                                                        <h3>Otorisasi Pinjam</h3>
                                                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
										<thead>
											<tr>
												<th class="center">No</th>
												<th class="hidden-xs">Tgl Pinjam</th>
												<th class="hidden-xs">Tgl Kembali</th>
												<th> Peminjaman </th>
												<th> Unit Pengelola </th>
												<th class="center"> Uraian </th>												
											</tr>
										</thead>
										<tbody>
											<tr >
												<td class="center" style="alignment-adjust: after-edge"> 1 </td>
												<td class="hidden-xs" valign="top">	29-06-2012 </td>
												<td class="hidden-xs" valign="top">	05-07-2012</td>
												<td valign="top">Edi</td>
												<td valign="top"> Bagian Keuangan]</td>
												<td>
                                                                                                    <a href="#myModal1" role="button" class="btn btn-default" data-toggle="modal">
										Detail
									</a>
                                                                                                </td>
											</tr>
                                                                                </tbody>    
                                                            </table>
                                                        <!-- start fade modal detail uraian -->
                                                       <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														&times;
													</button>
													<h4 class="modal-title">Uraian</h4>
												</div>
												<div class="modal-body">
													<p>
													KEputusan Direksi PT. Jasa Marga ( Persero ) Cabang Surabaya- Gempol No: 040/KPTS/CE/2001 Tangal 18 September 2001 Tentang Pembentukan Panitia Penghapusan dan Pelelangan Barang/Peralatan dilingkungan PT. Jasa Marga ( Persero ) Cabang Surabaya-Gempol Tahun Buku 2001
                                                                                                        </p><p> <strong> Berkas dipinjam : </strong> Jasa Marga KEputusan Direksi PT. Jasa Marga ( Persero ) Cabang Surabaya- Gempol No: 040/KPTS/CE/2001 Tangal 18 September 2001 T
													</p>
                                                                                                        <div class="right" align="right"> <button class="btn btn-success" type="button"> Accept </button> &nbsp; <button class="btn btn-danger" type="button"> Reject </button>  </div>
												</div>
												<div class="modal-footer">
													<button class="btn btn-default" data-dismiss="modal">
														Tutup
													</button>
												</div>
											</div>
										</div>
									</div>
                                                        <!-- end fade modal detail uraian -->
                                                    </div>
                                                     <div class="col-sm-6">	
                                                             <h3>Jatuh Tempo Pinjam</h3>
                                                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
										<thead>
											<tr>
												<th class="center">No</th>
												<th class="hidden-xs">Tgl Pinjam</th>
												<th class="hidden-xs">Tgl Kembali</th>
												<th> Peminjaman </th>
												<th> Unit Pengelola </th>
												<th class="center"> Uraian </th>												
											</tr>
										</thead>
										<tbody>
											<tr >
												<td class="center" style="alignment-adjust: after-edge"> 1 </td>
												<td class="hidden-xs" valign="top">	29-06-2012 </td>
												<td class="hidden-xs" valign="top">	05-07-2012</td>
												<td valign="top">Imam</td>
												<td valign="top"> Biro Perencanaan</td>
												<td>
                                                                                                    <a href="#myModal2" role="button" class="btn btn-default" data-toggle="modal">
										Detail
									</a>
                                                                                                </td>
											</tr>
                                                                                </tbody>    
                                                            </table>
                                                        <!-- start fade modal detail uraian -->
                                                       <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														&times;
													</button>
													<h4 class="modal-title">Uraian</h4>
												</div>
												<div class="modal-body">
													<p>
													KEputusan Direksi PT. Jasa Marga ( Persero ) Cabang Surabaya- Gempol No: 040/KPTS/CE/2001 Tangal 18 September 2001 Tentang Pembentukan Panitia Penghapusan dan Pelelangan Barang/Peralatan dilingkungan PT. Jasa Marga ( Persero ) Cabang Surabaya-Gempol Tahun Buku 2001
                                                                                                        </p><p> <strong> Berkas dipinjam : </strong> Jasa Marga KEputusan Direksi PT. Jasa Marga ( Persero ) Cabang Surabaya- Gempol No: 040/KPTS/CE/2001 Tangal 18 September 2001 T
													</p>
                                                                                                        </div>
												<div class="modal-footer">
													<button class="btn btn-default" data-dismiss="modal">
														Tutup
													</button>
												</div>
											</div>
										</div>
									</div>
                                                        <!-- end fade modal detail uraian -->
                                                    </div>
                                                 </div>
                                                
                                                
                                                <div class="row">
                                                    <div class="col-sm-6">	
                                                        <h3>Usul Pindah</h3>
                                                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
										<thead>
											<tr>
												<th class="center">No</th>
												<th class="hidden-xs">Tgl Pinjam</th>
												<th class="hidden-xs">Tgl Kembali</th>
												<th> Peminjaman </th>
												<th> Unit Pengelola </th>
												<th class="center"> Uraian </th>												
											</tr>
										</thead>
										<tbody>
											<tr >
												<td class="center" style="alignment-adjust: after-edge"> 1 </td>
												<td class="hidden-xs" valign="top">	29-06-2012 </td>
												<td class="hidden-xs" valign="top">	05-07-2012</td>
												<td valign="top">Edi</td>
												<td valign="top"> Bagian Keuangan]</td>
												<td>
                                                                                                    <a href="#myModal3" role="button" class="btn btn-default" data-toggle="modal">
										Detail
									</a>
                                                                                                </td>
											</tr>
                                                                                </tbody>    
                                                            </table>
                                                        <!-- start fade modal detail uraian -->
                                                       <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														&times;
													</button>
													<h4 class="modal-title">Uraian</h4>
												</div>
												<div class="modal-body">
													<p>
													KEputusan Direksi PT. Jasa Marga ( Persero ) Cabang Surabaya- Gempol No: 040/KPTS/CE/2001 Tangal 18 September 2001 Tentang Pembentukan Panitia Penghapusan dan Pelelangan Barang/Peralatan dilingkungan PT. Jasa Marga ( Persero ) Cabang Surabaya-Gempol Tahun Buku 2001
                                                                                                        </p><p> <strong> Berkas dipinjam : </strong> Jasa Marga KEputusan Direksi PT. Jasa Marga ( Persero ) Cabang Surabaya- Gempol No: 040/KPTS/CE/2001 Tangal 18 September 2001 T
													</p>
                                                                                                        <div class="right" align="right"> <button class="btn btn-success" type="button"> Accept </button> &nbsp; <button class="btn btn-danger" type="button"> Reject </button>  </div>
												</div>
												<div class="modal-footer">
													<button class="btn btn-default" data-dismiss="modal">
														Tutup
													</button>
												</div>
											</div>
										</div>
									</div>
                                                        <!-- end fade modal detail uraian -->
                                                    </div>
                                                     <div class="col-sm-6">	
                                                             <h3>Usul Musnah</h3>
                                                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_4">
										<thead>
											<tr>
												<th class="center">No</th>
												<th class="hidden-xs">Tgl Pinjam</th>
												<th class="hidden-xs">Tgl Kembali</th>
												<th> Peminjaman </th>
												<th> Unit Pengelola </th>
												<th class="center"> Uraian </th>												
											</tr>
										</thead>
										<tbody>
											<tr >
												<td class="center" style="alignment-adjust: after-edge"> 1 </td>
												<td class="hidden-xs" valign="top">	29-06-2012 </td>
												<td class="hidden-xs" valign="top">	05-07-2012</td>
												<td valign="top">Imam</td>
												<td valign="top"> Biro Perencanaan</td>
												<td>
                                                                                                    <a href="#myModal4" role="button" class="btn btn-default" data-toggle="modal">
										Detail
									</a>
                                                                                                </td>
											</tr>
                                                                                </tbody>    
                                                            </table>
                                                        <!-- start fade modal detail uraian -->
                                                       <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														&times;
													</button>
													<h4 class="modal-title">Uraian</h4>
												</div>
												<div class="modal-body">
													<p>
													KEputusan Direksi PT. Jasa Marga ( Persero ) Cabang Surabaya- Gempol No: 040/KPTS/CE/2001 Tangal 18 September 2001 Tentang Pembentukan Panitia Penghapusan dan Pelelangan Barang/Peralatan dilingkungan PT. Jasa Marga ( Persero ) Cabang Surabaya-Gempol Tahun Buku 2001
                                                                                                        </p><p> <strong> Berkas dipinjam : </strong> Jasa Marga KEputusan Direksi PT. Jasa Marga ( Persero ) Cabang Surabaya- Gempol No: 040/KPTS/CE/2001 Tangal 18 September 2001 T
													</p>
                                                                                                        </div>
												<div class="modal-footer">
													<button class="btn btn-default" data-dismiss="modal">
														Tutup
													</button>
												</div>
											</div>
										</div>
									</div>
                                                        <!-- end fade modal detail uraian -->
                                                    </div>
                                                 </div>
                                                
                                            </div>
                                            <div class="tab-pane" id="tab_jumlah_arsip">
                                                <!--- content in tab 2 -->
                                                
                                                <div class="row">
						<div class="col-md-12">
							<!-- start: INTERACTIVITY PANEL -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-external-link-square"></i>
									Statistik Jumlah Arsip Simpan dan Usul Musnah 									<div class="panel-tools">										
										<a class="btn btn-xs btn-link panel-refresh" href="#">
											<i class="fa fa-refresh"></i>
										</a>
										<a class="btn btn-xs btn-link panel-expand" href="#">
											<i class="fa fa-resize-full"></i>
										</a>
										
									</div>
								</div>
								<div class="panel-body">
									<div id="chart1" style="width: 75%;margin: 0 0"></div>
								</div>
							</div>
							<!-- end: INTERACTIVITY PANEL -->
						</div>
					</div>
                                                
                                            </div>
                                            
                                            <div class="tab-pane" id="tab_perminjaman_arsip">
                                                <!--- content in tab 2 -->
                                                
                                                <div class="row">
						<div class="col-md-12">
							<!-- start: INTERACTIVITY PANEL -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-external-link-square"></i>
									Statistik Peminjaman Arsip 									<div class="panel-tools">										
										<a class="btn btn-xs btn-link panel-refresh" href="#">
											<i class="fa fa-refresh"></i>
										</a>
										<a class="btn btn-xs btn-link panel-expand" href="#">
											<i class="fa fa-resize-full"></i>
										</a>
										
									</div>
								</div>
								<div class="panel-body">
									<div id="chart2" style="width: 75%;margin: 0 0"></div>
								</div>
							</div>
							<!-- end: INTERACTIVITY PANEL -->
						</div>
					</div>
                                                
                                            </div>
                                            
                                            <div class="tab-pane" id="tab_arsip_unit_pengelola">
                                                <!--- content in tab 2 -->
                                                
                                                <div class="row">
						<div class="col-md-12">
							<!-- start: INTERACTIVITY PANEL -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-external-link-square"></i>
									Statistik Arsip Per-unit Pengelola 									<div class="panel-tools">										
										<a class="btn btn-xs btn-link panel-refresh" href="#">
											<i class="fa fa-refresh"></i>
										</a>
										<a class="btn btn-xs btn-link panel-expand" href="#">
											<i class="fa fa-resize-full"></i>
										</a>
										
									</div>
								</div>
								<div class="panel-body">
									<div id="chart1" style="width: 75%;margin: 0 0"></div>
								</div>
							</div>
							<!-- end: INTERACTIVITY PANEL -->
						</div>
					</div>
                                                
                                            </div>
                                            
                                            <div class="tab-pane" id="tab_arsip_lokasi_simpan">
                                                <!--- content in tab 2 -->
                                                
                                                <div class="row">
						<div class="col-md-12">
							<!-- start: INTERACTIVITY PANEL -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-external-link-square"></i>
									Statistik Arsip Per-lokasi Simpan									<div class="panel-tools">										
										<a class="btn btn-xs btn-link panel-refresh" href="#">
											<i class="fa fa-refresh"></i>
										</a>
										<a class="btn btn-xs btn-link panel-expand" href="#">
											<i class="fa fa-resize-full"></i>
										</a>
										
									</div>
								</div>
								<div class="panel-body">
									<div id="chart1" style="width: 75%;margin: 0 0"></div>
								</div>
							</div>
							<!-- end: INTERACTIVITY PANEL -->
						</div>
					</div>
                                                
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- end: PAGE HEADER -->
                    <!-- start: PAGE CONTENT -->



                    <!-- end: PAGE CONTENT-->
                </div>
            </div>
            
            <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/select2/select2.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
		<script src="<?php echo base_url() ?>assets/js/table-data.js"></script>
                <script src="<?php echo base_url() ?>assets/plugins/bootstrap-paginator/src/bootstrap-paginator.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery.pulsate/jquery.pulsate.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/gritter/js/jquery.gritter.min.js"></script>
		<script src="<?php echo base_url() ?>assets/js/ui-elements.js"></script>
                <script src="<?php echo base_url() ?>assets/library/chart/js/highcharts.js"></script>
                <script src="<?php echo base_url() ?>assets/library/chart/js/modules/exporting.js"></script>
        <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script>
                                    jQuery(document).ready(function() {
                                        Main.init();
                                        TableData1.init();
                                        TableData2.init();
                                        TableData3.init();
                                        TableData4.init();
                                        UIElements.init();
                                    });
        </script>
        <script type="text/javascript">
jQuery(document).ready(function () {
        $('#chart1').highcharts({
            title: {
                text: 'Statistik Jumlah Arsip',
                x: -20 //center
            },
            subtitle: {
                text: 'DKPP (Dewan Kehormatan Penyelenggara Pemilu)',
                x: -20
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                title: {
                    text: 'Jumlah Arsip'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ' Arsip'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Jumlah Arsip Simpan',
                data: [27, 36, 19, 14, 18, 21, 25, 26, 23, 18, 13, 12]
            }, {
                name: 'Jumlah Arsip Usul Musnah',
                data: [2, 8, 7, 11, 7, 2, 4, 14, 10, 11, 8, 2]
            }]
        });
    });
    
    
   
		</script>
                
                 <script type="text/javascript">
jQuery(document).ready(function () {
    
        var colors = Highcharts.getOptions().colors,
            categories = [
                'Aku Memilih Setia - Fatin Shidqia Lubis','Serba Salah - Raisa','Payphone (Cover) - Jayesslee','Bye bye - JKT48'            ],
            name = 'Most Populer Song',
            data = [                
                { y: 4, color: colors[1] },{ y: 3, color: colors[2] },{ y: 2, color: colors[3] },{ y: 2, color: colors[4] }                   ];
    
        function setChart(name, categories, data, color) {
			chart.xAxis[0].setCategories(categories, false);
			chart.series[0].remove(false);
			chart.addSeries({
				name: name,
				data: data,
				color: color || 'white'
			}, false);
			chart.redraw();
        }
    
        var chart = jQuery('#chart2').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Most Populer Song'
            },
            subtitle: {
                text: 'Click the columns to view who user have been play.'
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: 'Total Play'
                }
            },
            plotOptions: {
                column: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                var drilldown = this.drilldown;
                                if (drilldown) { // drill down
                                    setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                                } else { // restore
                                    setChart(name, categories, data);
                                }
                            }
                        }
                    },
                    dataLabels: {
                        enabled: true,
                        color: colors[0],
                        style: {
                            fontWeight: 'bold'
                        },
                        formatter: function() {
                            return this.y +' times';
                        }
                    }
                }
            },
            tooltip: {
                formatter: function() {
                    var point = this.point,
                        s = this.x +':<b>'+ this.y +'times played</b><br/>';
                    
                    return s;
                }
            },
            series: [{
                name: name,
                data: data,
                color: 'white'
            }],
            exporting: {
                enabled: true
            }
        })
        .highcharts(); // return chart
    });
    

		</script>

 <script type="text/javascript">
jQuery(document).ready(function () {
    
        var colors = Highcharts.getOptions().colors,
            categories = [
                'Januari','Februari','Maret','April', 'Mei', 'Juni','Juli','Agustus','September','Oktober','November','Desember'],
            name = 'Bulan',
            data = [                
                { y: 15, color: colors[1] },
                { y: 34, color: colors[2] },
                { y: 14, color: colors[3] },
                { y: 4, color: colors[4] },
                { y: 18, color: colors[5] },
                { y: 9, color: colors[6] },
                { y: 12, color: colors[7] },
                { y: 10, color: colors[8] },
                { y: 8, color: colors[9] },
                { y: 21, color: colors[1] },
                { y: 20, color: colors[2] },
                { y: 4, color: colors[3] }       ];
    
        function setChart(name, categories, data, color) {
			chart.xAxis[0].setCategories(categories, false);
			chart.series[0].remove(false);
			chart.addSeries({
				name: name,
				data: data,
				color: color || 'white'
			}, false);
			chart.redraw();
        }
    
        var chart = jQuery('#chart2').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Statistik Peminjaman Arsip 2013'
            },
            subtitle: {
                text: 'Klik Untuk Melihat Detil Arsip.'
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: 'Total'
                }
            },
            plotOptions: {
                column: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                var drilldown = this.drilldown;
                                if (drilldown) { // drill down
                                    setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                                } else { // restore
                                    setChart(name, categories, data);
                                }
                            }
                        }
                    },
                    dataLabels: {
                        enabled: true,
                        color: colors[0],
                        style: {
                            fontWeight: 'bold'
                        },
                        formatter: function() {
                            return this.y +' arsip';
                        }
                    }
                }
            },
            tooltip: {
                formatter: function() {
                    var point = this.point,
                        s = this.x +':<b>'+ this.y +' star point</b><br/>';
                    
                    return s;
                }
            },
            series: [{
                name: name,
                data: data,
                color: 'white'
            }],
            exporting: {
                enabled: true
            }
        })
        .highcharts(); // return chart
    });
    

		</script>   
        
  <script type="text/javascript">
jQuery(document).ready(function () {
    
        var colors = Highcharts.getOptions().colors,
            categories = [
                'Ruang 01','Ruang 02','Ruang 03','Ruang 04', 'Ruang 05', 'Ruang 06','Ruang 07'],
            name = 'Kode Ruang',
            data = [                
                { y: 315, color: colors[1],drilldown: {
                        name: 'Ruang 01',
                        categories: ['Lemari 01', 'Lemari 02', 'Lemari 03', 'Lemari 04', 'Lemari 05',, 'Lemari 06'],
                        data: [80, 60, 40, 70,90,40],
                        color: colors[1]
                    } },
                { y: 334, color: colors[2] },
                { y: 314, color: colors[3] },
                { y: 304, color: colors[4] },
                { y: 318, color: colors[5] },
                { y: 409, color: colors[6] },
                { y: 312, color: colors[7] },
                { y: 10, color: colors[8] },
                { y: 408, color: colors[9] },
                { y: 321, color: colors[1] },
                { y: 320, color: colors[2] },
                { y: 424, color: colors[3] }       ];
    
        function setChart(name, categories, data, color) {
			chart.xAxis[0].setCategories(categories, false);
			chart.series[0].remove(false);
			chart.addSeries({
				name: name,
				data: data,
				color: color || 'white'
			}, false);
			chart.redraw();
        }
    
        var chart = jQuery('#chart2').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Statistik Arsip Perlokasi Simpan Tahun 2013'
            },
            subtitle: {
                text: 'Klik Untuk Melihat Detil Per-Lemari Arsip.'
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: 'Total'
                }
            },
            plotOptions: {
                column: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                var drilldown = this.drilldown;
                                if (drilldown) { // drill down
                                    setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                                } else { // restore
                                    setChart(name, categories, data);
                                }
                            }
                        }
                    },
                    dataLabels: {
                        enabled: true,
                        color: colors[0],
                        style: {
                            fontWeight: 'bold'
                        },
                        formatter: function() {
                            return this.y +' arsip';
                        }
                    }
                }
            },
            tooltip: {
                formatter: function() {
                    var point = this.point,
                        s = this.x +':<b>'+ this.y +' star point</b><br/>';
                    
                    return s;
                }
            },
            series: [{
                name: name,
                data: data,
                color: 'white'
            }],
            exporting: {
                enabled: true
            }
        })
        .highcharts(); // return chart
    });
    

		</script>      
            
            <!-- end: PAGE -->
     