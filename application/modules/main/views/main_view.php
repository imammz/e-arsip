<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- start: HEAD -->
	
    <?php $this->load->view('meta_view'); ?>    
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/ladda-bootstrap/dist/ladda-themeless.min.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-social-buttons/social-buttons-3.css">
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->   
    <!-- end: HEAD -->
    <!-- start: BODY -->
    <body>
        <!-- start: HEADER -->
        <?php $this->load->view('header_view'); ?>
        	<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugins/select2/select2.css" />
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/DataTables/media/css/DT_bootstrap.css" />
                <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/gritter/css/jquery.gritter.css">
                <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
        <!-- end: HEADER -->
        <!-- start: MAIN CONTAINER -->
        <div class="main-container" style="margin-top:150px;">
            <?php $this->load->view('menu_view'); ?>
            <!-- start: PAGE -->
            <div class="main-content">

                <div class="container">                	
                    <!-- start: PAGE HEADER -->
                    <div class="row">
                        <div class="col-sm-12">							
                            <!-- start: PAGE TITLE & BREADCRUMB -->
                            <ol class="breadcrumb">
                                <li>
                                    <i class="clip-home-3 active"></i>
                                    <a href="#">
                                        Home
                                    </a>
                                </li>
                            </ol>

                            <div class="page-header">
                                <h1>Home</h1>
                            </div>
                            <!-- end: PAGE TITLE & BREADCRUMB -->

                            <div class="row">                                
                                <div class="col-sm-12">
                                    <div class="tabbable">
                                        <ul id="myTab4" class="nav nav-tabs tab-padding tab-space-3 tab-blue">
                                            <li class="active">
                                                <a href="#panel_tab3_example1" data-toggle="tab">
                                                    Dashboard
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#panel_tab3_example2" data-toggle="tab">
                                                    Statistic
                                                </a>
                                            </li>                                         
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane in active" id="panel_tab3_example1">
                                                <!--- content in tab 1 -->
                                                 <div class="row">
                                                    <div class="col-sm-6">	
                                                        <h3>Otorisasi Pinjam</h3>
                                                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
										<thead>
											<tr>
												<th class="center">No</th>
												<th class="hidden-xs">Tgl Pinjam</th>
												<th class="hidden-xs">Tgl Kembali</th>
												<th> Peminjaman </th>
												<th> Unit Pengelola </th>
												<th class="center"> Uraian </th>												
											</tr>
										</thead>
										<tbody>
											<tr >
												<td class="center" style="alignment-adjust: after-edge"> 1 </td>
												<td class="hidden-xs" valign="top">	29-06-2012 </td>
												<td class="hidden-xs" valign="top">	05-07-2012</td>
												<td valign="top">Edi</td>
												<td valign="top"> Bagian Keuangan]</td>
												<td>
                                                                                                    <a href="#myModal1" role="button" class="btn btn-default" data-toggle="modal">
										Detail
									</a>
                                                                                                </td>
											</tr>
                                                                                </tbody>    
                                                            </table>
                                                        <!-- start fade modal detail uraian -->
                                                       <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														&times;
													</button>
													<h4 class="modal-title">Uraian</h4>
												</div>
												<div class="modal-body">
													<p>
													KEputusan Direksi PT. Jasa Marga ( Persero ) Cabang Surabaya- Gempol No: 040/KPTS/CE/2001 Tangal 18 September 2001 Tentang Pembentukan Panitia Penghapusan dan Pelelangan Barang/Peralatan dilingkungan PT. Jasa Marga ( Persero ) Cabang Surabaya-Gempol Tahun Buku 2001
                                                                                                        </p><p> <strong> Berkas dipinjam : </strong> Jasa Marga KEputusan Direksi PT. Jasa Marga ( Persero ) Cabang Surabaya- Gempol No: 040/KPTS/CE/2001 Tangal 18 September 2001 T
													</p>
                                                                                                        <div class="right" align="right"> <button class="btn btn-success" type="button"> Accept </button> &nbsp; <button class="btn btn-danger" type="button"> Reject </button>  </div>
												</div>
												<div class="modal-footer">
													<button class="btn btn-default" data-dismiss="modal">
														Tutup
													</button>
												</div>
											</div>
										</div>
									</div>
                                                        <!-- end fade modal detail uraian -->
                                                    </div>
                                                     <div class="col-sm-6">	
                                                             <h3>Jatuh Tempo Pinjam</h3>
                                                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
										<thead>
											<tr>
												<th class="center">No</th>
												<th class="hidden-xs">Tgl Pinjam</th>
												<th class="hidden-xs">Tgl Kembali</th>
												<th> Peminjaman </th>
												<th> Unit Pengelola </th>
												<th class="center"> Uraian </th>												
											</tr>
										</thead>
										<tbody>
											<tr >
												<td class="center" style="alignment-adjust: after-edge"> 1 </td>
												<td class="hidden-xs" valign="top">	29-06-2012 </td>
												<td class="hidden-xs" valign="top">	05-07-2012</td>
												<td valign="top">Imam</td>
												<td valign="top"> Biro Perencanaan</td>
												<td>
												<a href="#myModal2" role="button" class="btn btn-default" data-toggle="modal">
												Detail
												</a>
                                                </td>
											</tr>
										</tbody>    
									</table>
                                    <!-- start fade modal detail uraian -->
                                    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														&times;
													</button>
													<h4 class="modal-title">Uraian</h4>
												</div>
												<div class="modal-body">
													<p>
													KEputusan Direksi PT. Jasa Marga ( Persero ) Cabang Surabaya- Gempol No: 040/KPTS/CE/2001 Tangal 18 September 2001 Tentang Pembentukan Panitia Penghapusan dan Pelelangan Barang/Peralatan dilingkungan PT. Jasa Marga ( Persero ) Cabang Surabaya-Gempol Tahun Buku 2001
                                                                                                        </p><p> <strong> Berkas dipinjam : </strong> Jasa Marga KEputusan Direksi PT. Jasa Marga ( Persero ) Cabang Surabaya- Gempol No: 040/KPTS/CE/2001 Tangal 18 September 2001 T
													</p>
                                                                                                        </div>
												<div class="modal-footer">
													<button class="btn btn-default" data-dismiss="modal">
														Tutup
													</button>
												</div>
											</div>
										</div>
									</div>
                                                        <!-- end fade modal detail uraian -->
                                                    </div>
                                                 </div>
                                                
                                                
                                                <div class="row">
                                                    <div class="col-sm-6">	
                                                        <h3>Usul Pindah</h3>
                                                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
										<thead>
											<tr>
												<th class="center">No</th>
												<th class="hidden-xs">Tgl Pinjam</th>
												<th class="hidden-xs">Tgl Kembali</th>
												<th> Peminjaman </th>
												<th> Unit Pengelola </th>
												<th class="center"> Uraian </th>												
											</tr>
										</thead>
										<tbody>
											<tr >
												<td class="center" style="alignment-adjust: after-edge"> 1 </td>
												<td class="hidden-xs" valign="top">	29-06-2012 </td>
												<td class="hidden-xs" valign="top">	05-07-2012</td>
												<td valign="top">Edi</td>
												<td valign="top"> Bagian Keuangan]</td>
												<td>
                                                                                                    <a href="#myModal3" role="button" class="btn btn-default" data-toggle="modal">
										Detail
									</a>
                                                                                                </td>
											</tr>
                                                                                </tbody>    
                                                            </table>
                                                        <!-- start fade modal detail uraian -->
                                                       <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														&times;
													</button>
													<h4 class="modal-title">Uraian</h4>
												</div>
												<div class="modal-body">
													<p>
													KEputusan Direksi PT. Jasa Marga ( Persero ) Cabang Surabaya- Gempol No: 040/KPTS/CE/2001 Tangal 18 September 2001 Tentang Pembentukan Panitia Penghapusan dan Pelelangan Barang/Peralatan dilingkungan PT. Jasa Marga ( Persero ) Cabang Surabaya-Gempol Tahun Buku 2001
                                                                                                        </p><p> <strong> Berkas dipinjam : </strong> Jasa Marga KEputusan Direksi PT. Jasa Marga ( Persero ) Cabang Surabaya- Gempol No: 040/KPTS/CE/2001 Tangal 18 September 2001 T
													</p>
                                                                                                        <div class="right" align="right"> <button class="btn btn-success" type="button"> Accept </button> &nbsp; <button class="btn btn-danger" type="button"> Reject </button>  </div>
												</div>
												<div class="modal-footer">
													<button class="btn btn-default" data-dismiss="modal">
														Tutup
													</button>
												</div>
											</div>
										</div>
									</div>
                                                        <!-- end fade modal detail uraian -->
                                                    </div>
                                                     <div class="col-sm-6">	
                                                             <h3>Usul Musnah</h3>
                                                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_4">
										<thead>
											<tr>
												<th class="center">No</th>
												<th class="hidden-xs">Tgl Pinjam</th>
												<th class="hidden-xs">Tgl Kembali</th>
												<th> Peminjaman </th>
												<th> Unit Pengelola </th>
												<th class="center"> Uraian </th>												
											</tr>
										</thead>
										<tbody>
											<tr >
												<td class="center" style="alignment-adjust: after-edge"> 1 </td>
												<td class="hidden-xs" valign="top">	29-06-2012 </td>
												<td class="hidden-xs" valign="top">	05-07-2012</td>
												<td valign="top">Imam</td>
												<td valign="top"> Biro Perencanaan</td>
												<td>
                                                                                                    <a href="#myModal4" role="button" class="btn btn-default" data-toggle="modal">
										Detail
									</a>
                                                                                                </td>
											</tr>
                                                                                </tbody>    
                                                            </table>
                                                        <!-- start fade modal detail uraian -->
                                                       <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														&times;
													</button>
													<h4 class="modal-title">Uraian</h4>
												</div>
												<div class="modal-body">
													<p>
													KEputusan Direksi PT. Jasa Marga ( Persero ) Cabang Surabaya- Gempol No: 040/KPTS/CE/2001 Tangal 18 September 2001 Tentang Pembentukan Panitia Penghapusan dan Pelelangan Barang/Peralatan dilingkungan PT. Jasa Marga ( Persero ) Cabang Surabaya-Gempol Tahun Buku 2001
                                                                                                        </p><p> <strong> Berkas dipinjam : </strong> Jasa Marga KEputusan Direksi PT. Jasa Marga ( Persero ) Cabang Surabaya- Gempol No: 040/KPTS/CE/2001 Tangal 18 September 2001 T
													</p>
                                                                                                        </div>
												<div class="modal-footer">
													<button class="btn btn-default" data-dismiss="modal">
														Tutup
													</button>
												</div>
											</div>
										</div>
									</div>
                                                        <!-- end fade modal detail uraian -->
                                                    </div>
                                                 </div>
                                                
                                            </div>
                                            <div class="tab-pane" id="panel_tab3_example2">
                                                <!--- content in tab 2 -->
                                                
                                                <div class="row">
						<div class="col-md-12">
							<!-- start: INTERACTIVITY PANEL -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-external-link-square"></i>
									Statistik Arsip InAktif
									<div class="panel-tools">										
										<a class="btn btn-xs btn-link panel-refresh" href="#">
											<i class="fa fa-refresh"></i>
										</a>
										<a class="btn btn-xs btn-link panel-expand" href="#">
											<i class="fa fa-resize-full"></i>
										</a>
										
									</div>
								</div>
								<div class="panel-body">
									<div id="chart1" style="width: 75%;margin: 0 0"></div>
								</div>
							</div>
							<!-- end: INTERACTIVITY PANEL -->
						</div>
					</div>
                                                
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- end: PAGE HEADER -->
                    <!-- start: PAGE CONTENT -->



                    <!-- end: PAGE CONTENT-->
                </div>
            </div>
            <!--FAQ!-->
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: STYLE SELECTOR BOX -->
                        <div id="style_selector" class="hidden-xs">
                            <div id="style_selector_container" style="display:block">
                                <div class="style-main-title">
                                    FAQ
                                </div>
								
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                                
                            </div>
                            <div class="style-toggle open"></div>
                        </div>          
                        <!-- start: STYLE SELECTOR BOX -->
                    </div>
                </div>   
                <!-- start: PAGE HEADER END-->                 
			</div>
            <!--FAQ END!-->
            
            <!-- end: PAGE -->
        </div>
        <!-- end: MAIN CONTAINER -->
        
        <!-- start: FOOTER -->
        <?php $this->load->view('footer_view'); ?>
        <!-- end: FOOTER -->

        <!-- start: MAIN JAVASCRIPTS -->
        <?php $this->load->view('js_view'); ?>
        <!-- end: MAIN JAVASCRIPTS -->
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/select2/select2.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
		<script src="<?php echo base_url() ?>assets/js/table-data.js"></script>
                <script src="<?php echo base_url() ?>assets/plugins/bootstrap-paginator/src/bootstrap-paginator.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery.pulsate/jquery.pulsate.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/gritter/js/jquery.gritter.min.js"></script>
		<script src="<?php echo base_url() ?>assets/js/ui-elements.js"></script>
                <script src="<?php echo base_url() ?>assets/library/chart/js/highcharts.js"></script>
                <script src="<?php echo base_url() ?>assets/library/chart/js/modules/exporting.js"></script>
        <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script>
                                    jQuery(document).ready(function() {
                                        Main.init();
                                        TableData1.init();
                                        TableData2.init();
                                        TableData3.init();
                                        TableData4.init();
                                        UIElements.init();
                                    });
        </script>
        <script type="text/javascript">
jQuery(document).ready(function () {
        $('#chart1').highcharts({
            title: {
                text: 'Statistik Arsip Inaktif',
                x: -20 //center
            },
            subtitle: {
                text: 'DKPP (Dewan Kehormatan Penyelenggara Pemilu)',
                x: -20
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                title: {
                    text: 'Jumlah Arsip'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ' Arsip'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Entry Arsip Inaktif',
                data: [27, 36, 19, 14, 18, 21, 25, 26, 23, 18, 13, 12]
            }, {
                name: 'Peminjaman Arsip Inaktif',
                data: [2, 8, 7, 11, 7, 2, 4, 14, 10, 11, 8, 2]
            }]
        });
    });
    

		</script>
        
    </body>
    <!-- end: BODY -->
</html>