<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Home extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->_checkLogin();
		$this->load->model("pelayanan/peminjaman_model");
		$this->load->model("penyusutan/penyusutan_model");
		$this->load->model("statistik_model");
	}

	public function index() {
		$data = array();
		$data['class'] = 'home';
		$data['function'] = 'index';
		
		$this->output->enable_profiler(FALSE);
		
		
		$data['otorisasi'] = $this->peminjaman_model->_loadPeminjaman();
		$data['jatuh_tempo'] = $this->peminjaman_model->_loadPengembalian();
		$data['usul_serah'] = $this->penyusutan_model->_loadArsipUsulserah();
		$data['usul_musnah'] = $this->penyusutan_model->_loadArsipUsulMusnah();

		$data['kurun_waktu_awal'] = $this->statistik_model->_loadKurunWaktuAwal();
		$data['unit_pengelola'] = $this->master_model->_loadAllUnitKerja();
		$data['depo'] = $this->master_model->_loadLokasiDepo();
		$data['stat_pinjam'] = $this->statistik_model->_loadStatPinjam();
		
		$this->load->view('home_view',$data);
	}

}


/* End of file login.php */
/* Location: ./application/controllers/login.php */
