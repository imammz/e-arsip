<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- start: HEAD -->
	
    <?php echo Modules::run('templates/'.TEMPLATE.'/meta_css'); ?>
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/ladda-bootstrap/dist/ladda-themeless.min.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-social-buttons/social-buttons-3.css">
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->   
    <!-- end: HEAD -->
    <!-- start: BODY -->
    <body>
        <!-- start: HEADER -->
      <?php echo Modules::run('templates/'.TEMPLATE.'/header'); ?>
        	<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugins/select2/select2.css" />
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/DataTables/media/css/DT_bootstrap.css" />
                <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/gritter/css/jquery.gritter.css">
                
                 <script type="text/javascript" src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>
      <link type="text/css" href="<?php echo base_url() ?>assets/library/gb/greybox.css" rel="stylesheet" />	
                <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
        <!-- end: HEADER -->
        <!-- start: MAIN CONTAINER -->
        <div class="main-container" style="margin-top:150px;">
           <?php echo Modules::run('templates/'.TEMPLATE.'/menu'); ?>
            <!-- start: PAGE -->
            <div class="main-content">

                <div class="container">                	
                    <!-- start: PAGE HEADER -->
                    <div class="row">
                        <div class="col-sm-12">							
                            <!-- start: PAGE TITLE & BREADCRUMB -->
                            <ol class="breadcrumb">
                                <li>
                                    <i class="clip-home-3 active"></i>
                                    <a href="#">
                                        Home
                                    </a>
                                </li>
                            </ol>

                            <div class="page-header">
                                <h1>Home</h1>
                            </div>
                            <!-- end: PAGE TITLE & BREADCRUMB -->

                          <div class="row">                                
                                <div class="col-sm-12">
                                    <div class="tabbable">
                                        <ul id="myTab4" class="nav nav-tabs tab-padding tab-space-3 tab-blue">
                                            <li class="active">
                                                <a href="#tab_dashboard" data-toggle="tab">
                                                    Dashboard
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab_jumlah_arsip" data-toggle="tab">
                                                    Statistik Jumlah Arsip Simpan &  Arsip Musnah
                                                </a>
                                            </li>                                         
                                            <li>
                                                <a href="#tab_perminjaman_arsip" data-toggle="tab">
                                                    Statistik Peminjaman Arsip
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab_arsip_unit_pengelola" data-toggle="tab">
                                                    Statistik Arsip Per-unit Pengelola
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab_arsip_lokasi_simpan" data-toggle="tab">
                                                    Statistik Arsip Per-lokasi Simpan
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane in active" id="tab_dashboard">
                                                <!--- content in tab 1 -->
                                                 <div class="row">
                                                    <div class="col-sm-6">	
                                                        <h3>Otorisasi Pinjam</h3>
                                                            
                                                            <div id="demo_otorisasi"> </div>	
                                                        <!-- start fade modal detail uraian -->
                                                       
                                                        <!-- end fade modal detail uraian -->
                                                    </div>
                                                     <div class="col-sm-6">	
                                                             <h3>Jatuh Tempo Pinjam</h3>
                                                            
                                                        <!-- start fade modal detail uraian -->
                                                       <div id="demo_jatuh_tempo"> </div>	
                                                        <!-- end fade modal detail uraian -->
                                                    </div>
                                                 </div>
                                                
                                                
                                                <div class="row">
                                                    <div class="col-sm-6">	
                                                        <h3>Usul Serah</h3>
                                                            
                                                        <!-- start fade modal detail uraian -->
                                                       <div id="demo_usul_serah"> </div>	
                                                        <!-- end fade modal detail uraian -->
                                                    </div>
                                                     <div class="col-sm-6">	
                                                             <h3>Usul Musnah</h3>
                                                            
                                                        <!-- start fade modal detail uraian -->
                                                       <div id="demo_usul_musnah"> </div>
                                                        <!-- end fade modal detail uraian -->
                                                    </div>
                                                 </div>
                                                
                                            </div>
                                            <div class="tab-pane" id="tab_jumlah_arsip">
                                                <!--- content in tab 2 -->
                                                
                                                <div class="row">
						<div class="col-md-12">
							<!-- start: INTERACTIVITY PANEL -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-external-link-square"></i>
									Statistik Jumlah Arsip Simpan dan Arsip Musnah 									<div class="panel-tools">										
										<a class="btn btn-xs btn-link panel-refresh" href="#">
											<i class="fa fa-refresh"></i>
										</a>
										<a class="btn btn-xs btn-link panel-expand" href="#">
											<i class="fa fa-resize-full"></i>
										</a>
										
									</div>
								</div>
								<div class="panel-body">
									<div id="chart1" style="width: 75%;margin: 0 0"></div>
								</div>
							</div>
							<!-- end: INTERACTIVITY PANEL -->
						</div>
					</div>
                                                
                                            </div>
                                            
                                            <div class="tab-pane" id="tab_perminjaman_arsip">
                                                <!--- content in tab 2 -->
                                                
                                                <div class="row">
						<div class="col-md-12">
							<!-- start: INTERACTIVITY PANEL -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-external-link-square"></i>
									Statistik Peminjaman Arsip 									<div class="panel-tools">										
										<a class="btn btn-xs btn-link panel-refresh" href="#">
											<i class="fa fa-refresh"></i>
										</a>
										<a class="btn btn-xs btn-link panel-expand" href="#">
											<i class="fa fa-resize-full"></i>
										</a>
										
									</div>
								</div>
								<div class="panel-body">
									<div id="chart2" style="width: 75%;margin: 0 0"></div>
								</div>
							</div>
							<!-- end: INTERACTIVITY PANEL -->
						</div>
					</div>
                                                
                                            </div>
                                            
                                            <div class="tab-pane" id="tab_arsip_unit_pengelola">
                                                <!--- content in tab 2 -->
                                                
                                                <div class="row">
						<div class="col-md-12">
							<!-- start: INTERACTIVITY PANEL -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-external-link-square"></i>
									Statistik Arsip Per-unit Pengelola 									<div class="panel-tools">										
										<a class="btn btn-xs btn-link panel-refresh" href="#">
											<i class="fa fa-refresh"></i>
										</a>
										<a class="btn btn-xs btn-link panel-expand" href="#">
											<i class="fa fa-resize-full"></i>
										</a>
										
									</div>
								</div>
								<div class="panel-body">
									<div id="chart3" style="width: 75%;margin: 0 0"></div>
									<br/>
									<div id="chart3-2" style="width: 75%;margin: 0 0"></div>
								</div>
							</div>
							<!-- end: INTERACTIVITY PANEL -->
						</div>
					</div>
                                                
                                            </div>
                                            
                                            <div class="tab-pane" id="tab_arsip_lokasi_simpan">
                                                <!--- content in tab 2 -->
                                                
                                                <div class="row">
						<div class="col-md-12">
							<!-- start: INTERACTIVITY PANEL -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-external-link-square"></i>
									Statistik Arsip Per-lokasi Simpan									<div class="panel-tools">										
										<a class="btn btn-xs btn-link panel-refresh" href="#">
											<i class="fa fa-refresh"></i>
										</a>
										<a class="btn btn-xs btn-link panel-expand" href="#">
											<i class="fa fa-resize-full"></i>
										</a>
										
									</div>
								</div>
								<div class="panel-body">
									<div id="chart4" style="width: 75%;margin: 0 0"></div>
								</div>
							</div>
							<!-- end: INTERACTIVITY PANEL -->
						</div>
					</div>
                                                
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- end: PAGE HEADER -->
                    <!-- start: PAGE CONTENT -->



                    <!-- end: PAGE CONTENT-->
                </div>
            </div>
            <!--FAQ!-->
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: STYLE SELECTOR BOX -->
                        <div id="style_selector" class="hidden-xs">
                            <div id="style_selector_container" style="display:hidden">
                                <div class="style-main-title">
                                    FAQ
                                </div>
								
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                                
                            </div>
                            <div class="style-toggle close"></div>
                        </div>          
                        <!-- start: STYLE SELECTOR BOX -->
                    </div>
                </div>   
                <!-- start: PAGE HEADER END-->                 
			</div>
            <!--FAQ END!-->
            
            <!-- end: PAGE -->
        </div>
        <!-- end: MAIN CONTAINER -->
        
        <!-- start: FOOTER -->
         <?php echo Modules::run('templates/'.TEMPLATE.'/footer'); ?>
        <!-- end: FOOTER -->

        <!-- start: MAIN JAVASCRIPTS -->
        <?php echo Modules::run('templates/'.TEMPLATE.'/js'); ?>
        <!-- end: MAIN JAVASCRIPTS -->
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/select2/select2.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
		<script src="<?php echo base_url() ?>assets/js/table-data.js"></script>
                <script src="<?php echo base_url() ?>assets/plugins/bootstrap-paginator/src/bootstrap-paginator.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery.pulsate/jquery.pulsate.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/gritter/js/jquery.gritter.min.js"></script>
		<script src="<?php echo base_url() ?>assets/js/ui-elements.js"></script>
                <script src="<?php echo base_url() ?>assets/library/chart/js/highcharts.js"></script>
                <script src="<?php echo base_url() ?>assets/library/chart/js/modules/exporting.js"></script>
        <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        
        <script>
			function formDetil(id) {
            GB_show("Detil Data Arsip", '<?php echo base_url(); ?>pelayanan/otoritas/detil/'+id, 900, 1070);
           $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
        
         function formDetilPenerimaan(id) {
            GB_show("Detil Data Arsip", '<?php echo base_url(); ?>pemindahan/penerimaan/detil/'+id, 900, 1070);
           $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
		</script>

        <script>
                                    jQuery(document).ready(function() {
                                          Main.init();
                                        TableData1.init();
                                        TableData2.init();
                                        TableData3.init();
                                        TableData4.init();
                                        UIElements.init();
      
      $('#demo_usul_musnah').html( '<table border="0" class="table table-striped table-bordered table-hover table-full-width" id="example_usul_musnah"></table>' );
    $('#example_usul_musnah').dataTable( {
        "aaData": [
            /* Reduced data set */
            <?php 
            $no = 1;
            foreach($usul_musnah as $row) { ?>
            												[ 
                "<?php echo $no ?>",
                '<label class="checkbox-inline"><input type="checkbox" name="arsip_musnah[]" class="square-red" value="<?php echo $row['id_arsip']?>"></label>',
					 "<?php echo $row['nomor_definitif'] ?>",
					 "<?php echo $row['kode_klasifikasi'] ?>",
					 "<?php echo $row['unit_kerja'] ?>",
					 "<a href='#' role='button' onclick='formDetilPenerimaan(<?php echo $row['id_arsip']?>);' class='btn btn-default' > Detail </a>"],
            <?php $no++;	}  ?>        
        				 ],
       
        "oLanguage": {
                        "sLengthMenu": "Show _MENU_ Rows",
                        "sSearch": "",
                        "oPaginate": {
                            "sPrevious": "",
                            "sNext": ""
                        }
                    },
                  
                    "aoColumns": [
                        {"sTitle": "No", "sClass": "center"},
                        {"sTitle": "Pilih", "sClass": "center"},
                        {"sTitle": "Nomor Definitif"},
                        {"sTitle": "Kode Klasifikasi"},
                        {"sTitle": "Unit Kerja", "sClass": "center"},
                        {"sTitle": "Detail", "sClass": "center"}
                    ],
                    // set the initial value
                    "iDisplayLength": 10,
    } );  
      
      $('#demo_usul_serah').html( '<table border="0" class="table table-striped table-bordered table-hover table-full-width" id="example_usul_serah"></table>' );
    $('#example_usul_serah').dataTable( {
        "aaData": [
            /* Reduced data set */
            <?php 
            $no = 1;
            foreach($usul_serah as $row) { ?>
            												[ 
                "<?php echo $no ?>",
                '<label class="checkbox-inline"><input type="checkbox" name="arsip_serah[]" class="square-red" value="<?php echo $row['id_arsip']?>"></label>',
					 "<?php echo $row['nomor_definitif'] ?>",
					 "<?php echo $row['kode_klasifikasi'] ?>",
					 "<?php echo $row['unit_kerja'] ?>",
					 "<a href='#' role='button' onclick='formDetilPenerimaan(<?php echo $row['id_arsip']?>);' class='btn btn-default' > Detail </a>"],
            <?php $no++;	}  ?>        
        				 ],
       
        "oLanguage": {
                        "sLengthMenu": "Show _MENU_ Rows",
                        "sSearch": "",
                        "oPaginate": {
                            "sPrevious": "",
                            "sNext": ""
                        }
                    },
                  
                    "aoColumns": [
                        {"sTitle": "No", "sClass": "center"},
                        {"sTitle": "Pilih", "sClass": "center"},
                        {"sTitle": "Nomor Definitif"},
                        {"sTitle": "Kode Klasifikasi"},
                        {"sTitle": "Unit Kerja", "sClass": "center"},
                        {"sTitle": "Detail", "sClass": "center"}
                    ],
                    // set the initial value
                    "iDisplayLength": 10,
    } );                                    
                                        
                                        
       $('#demo_jatuh_tempo').html( '<table border="0" class="table table-striped table-bordered table-hover table-full-width" id="example_jatuh_tempo"></table>' );
    $('#example_jatuh_tempo').dataTable( {
        "aaData": [
            /* Reduced data set */
            <?php 
            $no = 1;
            foreach($jatuh_tempo as $row) { ?>
            [ 
                "<?php echo $no ?>", 
                "<?php echo $row['kode_peminjaman'] ?>", 
                "<?php echo $row['nama_peminjam'] ?>", 
                "<?php echo $row['kode_unit_kerja'].' | '.$row['unit_kerja'] ?>", 
                "<a href='#' role='button' onclick='formDetil(<?php echo $row['id_peminjaman'] ?>);' class='btn btn-default' > Detail </a>"
                ],
            <?php $no++; } ?>        
        ],
       
        "oLanguage": {
                        "sLengthMenu": "Show _MENU_ Rows",
                        "sSearch": "",
                        "oPaginate": {
                            "sPrevious": "",
                            "sNext": ""
                        }
                    },
                  
                    "aoColumns": [
                        {"sTitle": "No", "sClass": "center"},
                        {"sTitle": "Kode Peminjaman"},
                        {"sTitle": "Nama Peminjam"},
                        {"sTitle": "Unit Kerja", "sClass": "center"},
                        {"sTitle": "Detail Arsip", "sClass": "center"}
                    ],
                    // set the initial value
                    "iDisplayLength": 10,
    } );                                    
                                        
                                        
    $('#demo_otorisasi').html( '<table border="0" class="table table-striped table-bordered table-hover table-full-width" id="example_otorisasi"></table>' );
    $('#example_otorisasi').dataTable( {
        "aaData": [
            /* Reduced data set */
            <?php 
            $no = 1;
            foreach($otorisasi as $row) { ?>
            [ 
                "<?php echo $no ?>", 
                "<?php echo $row['nama_peminjam'] ?>", 
                "<a href='#' role='button' onclick='formDetil(<?php echo $row['id_peminjaman'] ?>);' class='btn btn-default' > Detail </a>",
                '<a onclick="return confirm(\'Anda yakin akan Menolak Permohonan Peminjaman?\')" class="btn btn-bricky" href="<?php echo base_url() ?>pelayanan/otoritas/reject/<?php echo $row['id_peminjaman'] ?>"> <i class="glyphicon glyphicon-remove-circle"></i></a> <a onclick="return confirm(\'Anda yakin akan Menyetujui Permohonan Peminjaman?\')" class="btn btn-green" href="<?php echo base_url() ?>pelayanan/otoritas/accept/<?php echo $row['id_peminjaman'] ?>"><i class="glyphicon glyphicon-ok-sign"></i></a>'
                ],
            <?php $no++; } ?>        
        ],
       
        "oLanguage": {
                        "sLengthMenu": "Show _MENU_ Rows",
                        "sSearch": "",
                        "oPaginate": {
                            "sPrevious": "",
                            "sNext": ""
                        }
                    },
                  
                    "aoColumns": [
                        {"sTitle": "No", "sClass": "center"},
                        {"sTitle": "Nama Peminjam"},
                        {"sTitle": "Detail Arsip", "sClass": "center"},
                        {"sTitle": "Otoritas", "sClass": "center"}
                    ],
                    // set the initial value
                    "iDisplayLength": 10,
    } );  
                                        
                                    });
        </script>
       <script type="text/javascript">
jQuery(document).ready(function () {
        $('#chart1').highcharts({
            title: {
                text: 'Statistik Jumlah Arsip',
                x: -20 //center
            },
            subtitle: {
                text: 'DKPP (Dewan Kehormatan Penyelenggara Pemilu)',
                x: -20
            },
            xAxis: {
                categories: [ <?php foreach($kurun_waktu_awal as $val) echo $val['kurun_waktu_awal'].', ' ?>  ]
            },
            yAxis: {
                title: {
                    text: 'Jumlah Arsip'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ' Arsip'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Jumlah Arsip Simpan',
                data: [
                <?php 
               foreach($kurun_waktu_awal as $val) {
                echo statistik_model::_countArsipSimpanByKurunWaktuAwal($val['kurun_waktu_awal']).','; } ?>
                ]
            }, {
                name: 'Jumlah Arsip Musnah',
                data: [<?php 
               foreach($kurun_waktu_awal as $val) {
                echo statistik_model::_countArsipMusnahByKurunWaktuAwal($val['kurun_waktu_awal']).','; } ?>]
            }]
        });
    });
    
    
   
		</script>
                
                 

 <script type="text/javascript">
jQuery(document).ready(function () {
    
        var colors = Highcharts.getOptions().colors,
            categories = [
                'Januari','Februari','Maret','April', 'Mei', 'Juni','Juli','Agustus','September','Oktober','November','Desember'],
            name = 'Bulan',
            data = [                
                <?php 
                $no = 1;
                foreach($stat_pinjam as $val) { 
                	
                	?>
                { y: <?php echo $val; ?>, color: colors[<?php echo $no; ?>] },
                <?php $no++; } ?>
                      ];
    
        function setChart(name, categories, data, color) {
			chart.xAxis[0].setCategories(categories, false);
			chart.series[0].remove(false);
			chart.addSeries({
				name: name,
				data: data,
				color: color || 'white'
			}, false);
			chart.redraw();
        }
    
        var chart = jQuery('#chart2').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Statistik Peminjaman Arsip <?php echo date('Y') ?>'
            },
            subtitle: {
                text: 'Klik Untuk Melihat Detil Arsip.'
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: 'Total'
                }
            },
            plotOptions: {
                column: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                var drilldown = this.drilldown;
                                if (drilldown) { // drill down
                                    setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                                } else { // restore
                                    setChart(name, categories, data);
                                }
                            }
                        }
                    },
                    dataLabels: {
                        enabled: true,
                        color: colors[0],
                        style: {
                            fontWeight: 'bold'
                        },
                        formatter: function() {
                            return this.y +' peminjaman';
                        }
                    }
                }
            },
            tooltip: {
                formatter: function() {
                    var point = this.point,
                        s = this.x +':<b>'+ this.y +' peminjaman arsip</b><br/>';
                    
                    return s;
                }
            },
            series: [{
                name: name,
                data: data,
                color: 'white'
            }],
            exporting: {
                enabled: true
            }
        })
        .highcharts(); // return chart
    });
    

		</script>   
        
        </script>
       <script type="text/javascript">
jQuery(document).ready(function () {
        $('#chart3').highcharts({
            title: {
                text: 'Statistik Arsip Per-Unit Pengelola',   
                x: -20 //center
            },
            subtitle: {
                text: 'DKPP (Dewan Kehormatan Penyelenggara Pemilu)',
                x: -20
            },
            xAxis: {
                categories: [ <?php foreach($kurun_waktu_awal as $val) echo $val['kurun_waktu_awal'].', ' ?>  ]
            },
            yAxis: {
                title: {
                    text: 'Jumlah Arsip'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ' Arsip'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [
            <?php foreach($unit_pengelola as $val_unit_pengelola) { ?>
            {
                name: '<?php echo $val_unit_pengelola['nama_unit_kerja']?>',
                data: [
                <?php 
               foreach($kurun_waktu_awal as $val) {
                echo statistik_model::_countArsipByUnit($val['kurun_waktu_awal'],$val_unit_pengelola['kode_unit_kerja']).','; } ?>
                ]
            }, 
            <?php }  ?>
            ]
        });
    });
    
    
   
		</script>
		
		<script type="text/javascript">
$(function () {
    $('#chart3-2').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: 'Jumlah Seluruh Arsip DKPP : <?php 
            $all_arsip = statistik_model::_countArsipAll();
            echo number_format($all_arsip,0,',','.') ?>'
        },
        tooltip: {
    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    color: '#000000',
                    connectorColor: '#000000',
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Persentase Jumlah Arsip ',
            data: [
               <?php foreach($unit_pengelola as $val_unit_pengelola) { ?> 
                ['<?php echo $val_unit_pengelola['nama_unit_kerja']?>',  <?php echo statistik_model::_countArsipByUnitAll($val_unit_pengelola['kode_unit_kerja'])/$all_arsip*100 ?> ],
                <?php } ?>
            ]
        }]
    });
});
    

		</script>
        
        
  <script type="text/javascript">
jQuery(document).ready(function () {
    
        var colors = Highcharts.getOptions().colors,
            categories = [
                <?php foreach($depo as $val_depo) echo '"'.$val_depo['nomor_depo'].' - '.$val_depo['nama_depo'].'", ' ?> ],
            name = 'Depo',
            data = [
             <?php 
             $no = 1;
             foreach($depo as $val_depo) { ?>               
                { y: <?php echo statistik_model::_countArsipByDepo($val_depo['nomor_depo']) ?>, color: colors[1] },
                  <?php $no++; } ?>  
                        
                     ];
    
        function setChart(name, categories, data, color) {
			chart.xAxis[0].setCategories(categories, false);
			chart.series[0].remove(false);
			chart.addSeries({
				name: name,
				data: data,
				color: color || 'white'
			}, false);
			chart.redraw();
        }
    
        var chart = jQuery('#chart4').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Statistik Arsip Perlokasi Simpan '
            },
            subtitle: {
                text: 'Klik Untuk Melihat Detil.'
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: 'Total'
                }
            },
            plotOptions: {
                column: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                var drilldown = this.drilldown;
                                if (drilldown) { // drill down
                                    setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                                } else { // restore
                                    setChart(name, categories, data);
                                }
                            }
                        }
                    },
                    dataLabels: {
                        enabled: true,
                        color: colors[0],
                        style: {
                            fontWeight: 'bold'
                        },
                        formatter: function() {
                            return this.y +' arsip';
                        }
                    }
                }
            },
            tooltip: {
                formatter: function() {
                    var point = this.point,
                        s = this.x +':<b>'+ this.y +' star point</b><br/>';
                    
                    return s;
                }
            },
            series: [{
                name: name,
                data: data,
                color: 'white'
            }],
            exporting: {
                enabled: true
            }
        })
        .highcharts(); // return chart
    });
    

		</script>  
        
    </body>
    <!-- end: BODY -->
</html>