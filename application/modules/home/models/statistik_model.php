<?php

class statistik_model extends  CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
         {
        parent::__construct();        
	}
			
	public function _loadKurunWaktuAwal() {
		
		$this->db->select('kurun_waktu_awal');
		$this->db->where('kurun_waktu_awal >=',1900);
		$this->db->group_by('kurun_waktu_awal');
		$this->db->order_by('kurun_waktu_awal','ASC');
		
		$ress = $this->db->get('arsip')->result_array();
		
		return $ress;
	}	
	
	public function _countArsipSimpanByKurunWaktuAwal($kurun_waktu_awal) {
		$this->db->select('count(id_arsip) as jml');
		$this->db->where('status_musnah','N');			
		$this->db->where('kurun_waktu_awal',$kurun_waktu_awal);			
				
		$ress = $this->db->get('arsip')->row_array();
		
		return $ress['jml'];	
	}
	
	public function _countArsipMusnahByKurunWaktuAwal($kurun_waktu_awal) {
			$this->db->select('count(id_arsip) as jml');
			$this->db->where('status_musnah','Y');			
			$this->db->where('kurun_waktu_awal',$kurun_waktu_awal);			
					
			$ress = $this->db->get('arsip')->row_array();
			
			return $ress['jml'];	
		}
		
	public function _countArsipByUnit($kurun_waktu_awal,$kode_unit_kerja) {
				$this->db->select('count(id_arsip) as jml');
				$this->db->where('kurun_waktu_awal',$kurun_waktu_awal);			
				$this->db->where('kode_unit_kerja',$kode_unit_kerja);			
						
				$ress = $this->db->get('arsip')->row_array();
				
				return $ress['jml'];	
			}
			
public function _countArsipByUnitAll($kode_unit_kerja) {
				$this->db->select('count(id_arsip) as jml');			
				$this->db->where('kode_unit_kerja',$kode_unit_kerja);			
						
				$ress = $this->db->get('arsip')->row_array();
				
				return $ress['jml'];	
			}			

public function _countArsipAll() {
				$this->db->select('count(id_arsip) as jml');			
				$ress = $this->db->get('arsip')->row_array();
				
				return $ress['jml'];	
			}			
			
 public function _countArsipByDepo($nomor_depo) {
				$this->db->select('count(id_arsip) as jml');
				$this->db->where('nomor_depo',$nomor_depo);								
				$ress = $this->db->get('arsip')->row_array();				
				return $ress['jml'];	
			}
	
	public function _loadStatPinjam() {
		
		$count = array();
		
		for($i=1;$i<=12;$i++) {
		$this->db->select('count(id_peminjaman) as jml');
		$this->db->where('YEAR(tanggal_peminjaman)',date('Y'));			
		$this->db->where('MONTH(tanggal_peminjaman)',$i);
		$ress = $this->db->get('peminjaman')->row_array();
		$count[] = $ress['jml'];			
		}
		
		return $count;	
	}		
	
}
// END Login_model Class