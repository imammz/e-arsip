<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends MY_Controller {
     
	  function __construct() {
			parent::__construct();
			$this->_checkLogin();
			$this->load->model('master_model');
			//$this->load->library('grocery_CRUD');
		}
    
    
	public function index()
	{
			$data = array();
			$data['class'] = 'home';
			$data['function'] = 'index';
            $this->load->view('user_view',$data);
	}
	
	
	public function _example_output($output = null)
	{
		$this->load->view('grocery_view',$output);
	}
    
	 
    public function userManagement()
	{
		$crud = new grocery_CRUD();

		$crud->set_table('m_user');
		$crud->set_primary_key('kode_unit_kerja','unit_kerja');
		$crud->set_relation('kode_unit_kerja','unit_kerja','{kode_unit_kerja} - {nama_unit_kerja}');
		$crud->set_relation('id_role','m_role','role');
		$crud->columns('nama_lengkap', 'nik', 'username', 'email');
		$crud->display_as('nama_lengkap', 'Nama Lengkap')
			 ->display_as('nik', 'NIP')
			 ->display_as('email', 'Email')
			 ->display_as('username', 'Username');
		//$crud->field_type('password', 'password');
		$crud->change_field_type('password','password');
		$crud->callback_before_insert(array($this,'encrypt_password_callback'));
		$crud->callback_before_update(array($this,'encrypt_password_callback'));
		$crud->set_field_upload('path_image', 'assets/images');
		
		$crud->unset_fields('insert_at','update_at');
		$crud->set_subject('User');
	
		$output = $crud->render();

		$this->_example_output($output);                     
	}
	
	 
	function encrypt_password_callback($post_array) {
                //$post_array['password'] .= $this->config->item('encryption_key');
		// Return the MD5 encryption
		$post_array['password'] = MD5($post_array['password']);
		
		return $post_array;
	} 
	
    public function role()
	{
		$crud = new grocery_CRUD();

		$crud->set_table('m_role');
		$crud->columns('role');
		$crud->set_relation_n_n('menus', 'm_menu_role', 'm_menu', 'id_role', 'id_menu', 'menu','urut');
		$crud->display_as('role', 'Role');
		$crud->set_subject('Role');

		$output = $crud->render();

		$this->_example_output($output);
	}
	
	public function menu()
	{
		$crud = new grocery_CRUD();

		$crud->set_table('m_menu');
		$crud->set_subject('Menu');

		$output = $crud->render();

		$this->_example_output($output);
	}
   
	
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */