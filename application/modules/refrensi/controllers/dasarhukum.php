<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Dasarhukum extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->_checkLogin();
	}

	public function index() {
		$data = array();
		$data['class'] = 'refrensi';
		$data['function'] = 'dasarhukum';
		
		$this->output->enable_profiler(FALSE);
		
				
		$this->load->view('dasarhukum_view',$data);
	}

}


/* End of file login.php */
/* Location: ./application/controllers/login.php */
