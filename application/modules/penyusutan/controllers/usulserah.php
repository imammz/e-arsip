<?php
	if(!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Usulserah extends MY_Controller {

		function __construct() {
			parent::__construct();
			$this->_checkLogin();
			$this->load->model('master/master_model');
			$this->load->model('penyusutan_model');
		}

		public function index() {
			
			$this->output->enable_profiler(FALSE);	
				
			$data = array();
			$data['class'] = 'penyusutan';
			$data['function'] = 'usulserah';
				
			$data['arsip'] = $this->penyusutan_model->_loadArsipUsulSerah();
			$data['count'] = count($data['arsip']);
			
			$this->load->view('usulserah_view',$data);

		}

		public function process() {

			$this->output->enable_profiler(FALSE);
			
			$arsip_serah = $this->input->post('arsip_serah',TRUE);
					
			if(!empty($arsip_serah)) {
			$this->penyusutan_model->process_serah($arsip_serah);						
			$this->successSerah();
			}
			else {
			$this->errorSerah();	
			}
						
		}

	private function successSerah() {
			echo '<script>
                alert("Proses Penyerahan Arsip Berhasil!");
                window.location.replace("' . base_url() . 'penyusutan/usulserah");
                  </script>';
		}
	
	private function errorSerah() {
			echo '<script>
                alert("Proses Penyerahan Arsip Gagal!  Tidak Ada Arsip Yang Dipilih");
                window.location.replace("' . base_url() . 'penyusutan/usulserah");
                  </script>';
		}	

	}


	/* End of file login.php */
	/* Location: ./application/controllers/login.php */
