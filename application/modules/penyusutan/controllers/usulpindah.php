<?php
	if(!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Usulpindah extends MY_Controller {

		function __construct() {
			parent::__construct();
			$this->_checkLogin();
			$this->load->model('master/master_model');
			$this->load->model('penyusutan_model');
		}

		public function index() {
			$data = array();
			$data['class'] = 'penyusutan';
			$data['function'] = 'usulpindah';
			
			$data['arsip'] = $this->penyusutan_model->_loadArsipUsulPindah();
			$data['count'] = count($data['arsip']);

			$this->load->view('usulpindah_view',$data);
		}
		
		public function process() {

			$this->output->enable_profiler(FALSE);
			
			$arsip_pindah = $this->input->post('arsip_pindah',TRUE);
					
			if(!empty($arsip_pindah)) {
			$this->penyusutan_model->process_pindah($arsip_pindah);						
			$this->successPindah();
			}
			else {
			$this->errorPindah();	
			}
						
		}

	private function successPindah() {
			echo '<script>
                alert("Proses Pemindahan Arsip Berhasil!");
                window.location.replace("' . base_url() . 'penyusutan/usulpindah");
                  </script>';
		}
	
	private function errorPindah() {
			echo '<script>
                alert("Proses Pemindahan Arsip Gagal!  Tidak Ada Arsip Yang Dipilih");
                window.location.replace("' . base_url() . 'penyusutan/usulpindah");
                  </script>';
		}	

	}


	/* End of file login.php */
	/* Location: ./application/controllers/login.php */
