<?php
	if(!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Usulmusnah extends MY_Controller {

		function __construct() {
			parent::__construct();
			$this->_checkLogin();
			$this->load->model('master/master_model');
			$this->load->model('penyusutan_model');
		}

		public function index() {
			
			$this->output->enable_profiler(FALSE);	
				
			$data = array();
			$data['class'] = 'penyusutan';
			$data['function'] = 'usulmusnah';
					
			$data['arsip'] = $this->penyusutan_model->_loadArsipUsulMusnah();
			$data['count'] = count($data['arsip']);
			
			$this->load->view('usulmusnah_view',$data);

		}

		public function process() {

			$this->output->enable_profiler(FALSE);
						
			$arsip_musnah = $this->input->post('arsip_musnah',TRUE);
			if(!empty($arsip_musnah)) {
			$this->penyusutan_model->process_musnah($arsip_musnah);						
			$this->successMusnah();
			}
			else {
			$this->errorMusnah();	
			}
		}

	private function successMusnah() {
			echo '<script>
                alert("Proses Pemusnahan Arsip Berhasil!");
                window.location.replace("' . base_url() . 'penyusutan/usulmusnah");
                  </script>';
		}
	
	private function errorMusnah() {
			echo '<script>
                alert("Proses Pemusnahan Arsip Gagal!  Tidak Ada Arsip Yang Dipilih");
                window.location.replace("' . base_url() . 'penyusutan/usulmusnah");
                  </script>';
		}		

	}


	/* End of file login.php */
	/* Location: ./application/controllers/login.php */
