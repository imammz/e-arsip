<?php
class Penyusutan_model extends CI_Model {
	/**
	 * Constructor
	 */
	function __construct() {
		parent::__construct();
	}

	public function _loadJRAMusnah() {
		$query = $this->db->query("select * from jra where status_akhir = 'Musnah' order by id_jra ASC")->result_array();
		return $query;
	}

	public function _loadIdJRAMusnah() {
		$query = $this->db->query("select kode_jra from jra where status_akhir = 'Musnah' order by id_jra ASC")->result_array();
		$jra = array();
		foreach($query as $key=>$value) {
			$jra[] = $value['kode_jra'];
		}
		return $jra;
	}

	public function _loadKodeJRAMusnah() {
		$query = $this->db->query("select kode_jra from jra where status_akhir = 'Musnah' order by id_jra ASC")->result_array();
		$jra = array();
		foreach($query as $key=>$value) {
			$jra[] = $value['kode_jra'];
		}
		return $jra;
	}

	public function _loadArsipMusnah() {

		$jra = $this->_loadIdJRAMusnah();

		$this->db->where_in('kode_jra',$jra);
		$this->db->where('status_musnah','Y');
		$query = $this->db->get('arsip')->result_array();

		return $query;
	}

	public function _loadArsipUsulMusnah() {

		$tahun = date('Y');
		$jra = Penyusutan_model::_loadIdJRAMusnah();

		$this->db->select('nama_unit_kerja as unit_kerja, jenis_jra as jra, id_arsip, nomor_definitif, uraian, kode_klasifikasi, kurun_waktu_awal, kurun_waktu_akhir');
		$this->db->where('status_musnah','N');
		$this->db->where('kurun_waktu_akhir <=',$tahun);
		if($this->session->userdata('id_role')!=1) {
				$this->db->where('arsip.kode_unit_kerja',$this->session->userdata('kode_unit_kerja'));
			}
		$this->db->where_in('arsip.kode_jra',$jra);
		$this->db->order_by('id_arsip','ASC');
		$this->db->join('unit_kerja','unit_kerja.kode_unit_kerja = arsip.kode_unit_kerja');
		$this->db->join('jra','jra.kode_jra = arsip.kode_jra'); //selanjutnya diganti jadi kode_jra

		$query = $this->db->get('arsip')->result_array();

		return $query;
	}

	public function process_musnah($arsip_musnah) {
		$data = array();
		$data['status_musnah'] = 'Y';
		foreach($arsip_musnah as $value) {
			$this->db->update('arsip',$data,'id_arsip = "' . $value . '"');
			Master_model::logs('MUSNAH',$value);
		}
	}
	
	public function _loadArsipUsulSerah() {

		$tahun = date('Y');
		$jra = Penyusutan_model::_loadIdJRAMusnah();

		$this->db->select('nama_unit_kerja as unit_kerja, jenis_jra as jra, id_arsip, nomor_definitif, uraian, kode_klasifikasi, kurun_waktu_awal, kurun_waktu_akhir');
		$this->db->where('status_serah','N');
		$this->db->where('kurun_waktu_akhir <=',$tahun);
		if($this->session->userdata('id_role')!=1) {
				$this->db->where('arsip.kode_unit_kerja',$this->session->userdata('kode_unit_kerja'));
			}
		$this->db->where_not_in('arsip.kode_jra',$jra);
		$this->db->order_by('id_arsip','ASC');
		$this->db->join('unit_kerja','unit_kerja.kode_unit_kerja = arsip.kode_unit_kerja');
		$this->db->join('jra','jra.kode_jra = arsip.kode_jra'); //selanjutnya diganti jadi kode_jra

		$query = $this->db->get('arsip')->result_array();

		return $query;
	}
	
	public function _loadArsipUsulPindah() {

		$this->db->select('nama_unit_kerja as unit_kerja, jenis_jra as jra, id_arsip, nomor_definitif, uraian, kode_klasifikasi, kurun_waktu_awal, kurun_waktu_akhir');
		$this->db->where('status_pindah','N');
		if($this->session->userdata('id_role')!=1) {
				$this->db->where('arsip.kode_unit_kerja',$this->session->userdata('kode_unit_kerja'));
			}
		$this->db->order_by('id_arsip','ASC');
		$this->db->join('unit_kerja','unit_kerja.kode_unit_kerja = arsip.kode_unit_kerja');
		$this->db->join('jra','jra.kode_jra = arsip.kode_jra'); //selanjutnya diganti jadi kode_jra

		$query = $this->db->get('arsip')->result_array();

		return $query;
	}
	
	public function process_pindah($arsip_pindah) {
		$data = array();
		$data['status_pindah'] = 'Y';
		foreach($arsip_pindah as $value) {
			$this->db->update('arsip',$data,'id_arsip = "' . $value . '"');
			Master_model::logs('PINDAH',$value);
		}
	}
	
	public function process_serah($arsip_serah) {
		$data = array();
		$data['status_serah'] = 'Y';
		foreach($arsip_serah as $value) {
			$this->db->update('arsip',$data,'id_arsip = "' . $value . '"');
			Master_model::logs('SERAH',$value);
		}
	}
	
	

}
