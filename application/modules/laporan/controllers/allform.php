<?php
if(!defined('BASEPATH'))
    exit('No direct script access allowed');

class allform extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->_checkLogin();
        if ($this->session->userdata('login') == FALSE) {
            redirect('login');
        }
        $this->load->model('pemindahan/penerimaan_model');
        $this->load->model('pelayanan/peminjaman_model');

    }

    public function cetakexcell(){
        $data_pelayanan = $this->peminjaman_model->_loadAllPelayanan();
        $result['data_pelayanan'] = $data_pelayanan;
        $result['jumlah_baris'] = count($data_pelayanan);
        $this->load->view('pelayanan_excel',$result);
    }

    public function index()
    {
        $peminjaman = $this->peminjaman_model->_loadAllPelayanan();
        //echo $peminjaman[1]['id_peminjaman'];
        //echo "<pre>";
        //print_r($peminjaman[4]['arsip'][1]);
        //echo "</pre>";
        //foreach ($peminjaman[4]['arsip'] as $item) {
          //  echo $item['uraian'];
        //}
        //exit;
        $jumlah_baris = count($peminjaman);
        $petugas = $this->session->userdata('nama_lengkap');
        $petugas_satker = $this->session->userdata('nama_unit_kerja');

        $this->load->library('TCPDF');

        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(WEB_TITLE);
        $pdf->SetAuthor($this->session->userdata('username'));
        $pdf->SetTitle("Form Peminjaman");
        $pdf->SetSubject("Semua Form Peminjaman");

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(5, 5, 5);
        $pdf->setHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->setFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // ---------------------------------------------------------

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('times', '', 10, '', true);


        // Add a page
        // This method has several options, check the source code documentation for more information.


        //$pdf->code = $kode_peminjaman;

        // Set some content to print

        for ($i = 0; $i < $jumlah_baris; $i++) {
            $pdf->AddPage("L");
            $kode_peminjaman = $peminjaman[$i]['kode_peminjaman'];
            $nama_peminjam = $peminjaman[$i]['nama_peminjam'];
            $tanggal_peminjaman = $peminjaman[$i]['tanggal_peminjaman'];
            $tanggal_pengembalian = $peminjaman[$i]['tanggal_pengembalian'];
            $unit_kerja = $peminjaman[$i]['unit_kerja'];
            //$id_peminjaman = $peminjaman[$i]['id_peminjaman'];
            //$id_arsip_pinjam = $peminjaman[$i]['id_arsip'];
            $pdf->code = $kode_peminjaman;
            $pdf->setPage($i+1, true);

            $html = <<<EOD
<div align="center" width="100%"><h3>Form Bukti Peminjaman Arsip</h3></div><br/>
<table width="100%">
<tr><td width="130">Nama </td><td>: <b>$nama_peminjam</b></td></tr>
<tr><td width="130">Tanggal Peminjaman </td><td>: <b>$tanggal_peminjaman</b></td></tr>
<tr><td width="130">Tanggal Pengembalian </td><td>: <b>$tanggal_pengembalian</b></td></tr>    
</table>
 <hr><hr><br><br><br><br><br><br>
<h3>Daftar Arsip Yang Dipinjam</h3>    
<br>

EOD;

            //$pdf->SetY(0);

            if ($i+1 == $jumlah_baris) {
                $pdf->lastPage();
            }
            $pdf->writeHTMLCell($w = 0,$h = 0,$x = '',$y = '',$html,$border = 0,$ln = 1,$fill = 0,$reseth = true,$align = '',$autopadding = true);

        }
        $pdf->Output("Semua Form Peminjaman");

    }

}

class MYPDF extends TCPDF {

    var $id;
    var $indeks;
    var $style = array('border'=>0,'vpadding'=>0,'hpadding'=>0,'fgcolor'=> array(0,0,0),'bgcolor'=>false,//array(255,255,255)
        'module_width'=>1,// width of a single module in points
        'module_height'=>1 // height of a single module in points
    );
    var $code;

    //Page header
    public function Header() {
        //set Image Background (watermark)
        $img_file = K_PATH_IMAGES.'logo.png';
        $this->Image($img_file, 85, 10, 210, 297, '', '', 'center', false, 300, '', false, false, 0);

        $html = "<div align='right' width='100%'><sub>".COPYRIGHT."</sub></div>";
        $this->writeHTML($html, true, 0, true, true);
    }
    // Page footer
    public function Footer() {

        $html = "<div align='right' width='100%'><sub>".WEB_TITLE." - ".date("Y")."</sub></div>";
        $this->writeHTML($html, true, 0, true, true);

        $code = $this->code;
        $style = array(
            'border' => 0,
            'vpadding' => 'auto',
            'hpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255)
            'module_width' => 1, // width of a single module in points
            'module_height' => 1 // height of a single module in points
        );
        $this->Cell(0, 5, 'Page '.$this->getAliasNumPage().' of '.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->write2DBarcode($code, 'QRCODE,H', 270, 182, 22, 22, $style, 'N');
        $this->Text(255, 202, $code);
    }

}
