<?php
	if(!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Pertelaan extends MY_Controller {

		function __construct() {
			parent::__construct();
			$this->_checkLogin();
		}

		public function index() {
			$data = array();
			$data['class'] = 'laporan';
			$data['function'] = 'pertelaan';

			$data['klasifikasi'] = $this->master_model->_loadKodeKlasifikasi();
			$data['unit_kerja'] = $this->master_model->_loadUnitKerja();
			$data['tingkat_perkembangan'] = $this->master_model->_loadTingkatPerkembangan();
			$data['kondisi_fisik'] = $this->master_model->_loadKondisiFisik();
			$data['media_simpan'] = $this->master_model->_loadMediaSimpan();

			$data['depo'] = $this->master_model->_loadLokasiDepo();
			$data['ruang'] = $this->master_model->_loadLokasiRuang();
			$data['boks'] = $this->master_model->_loadLokasiBoks();

			$status_arsip = array();
			$status_arsip[] = array('status'=>'status_pindah','title'=>'Arsip Simpan');
			$status_arsip[] = array('status'=>'status_pinjam','title'=>'Arsip Dipinjam');
			$status_arsip[] = array('status'=>'status_musnah','title'=>'Arsip Musnah');
			$status_arsip[] = array('status'=>'status_serah','title'=>'Arsip Serah');

			$data['status_arsip'] = $status_arsip;

			$this->load->view('daftar_pertelaan_view',$data);
		}


   public function process() {
        $this->output->enable_profiler(FALSE);
			
			$this->db->delete('laporan_pertelaan','id = 1 ');
			
			$data = array();
			$data['id'] = 1;
			$data['status_arsip'] = $this->input->post('status_arsip',true);
			$data['kode_klasifikasi'] = $this->input->post('kode_klasifikasi',true);
			$data['kode_jra'] = $this->input->post('kode_jra',true);
			$data['kode_unit_kerja'] = $this->input->post('kode_unit_kerja',true);
			$data['uraian'] = $this->input->post('uraian',true);
			$data['kurun_waktu_awal'] = $this->input->post('kurun_waktu_awal',true);
			$data['kurun_waktu_akhir'] = $this->input->post('kurun_waktu_akhir',true);
			$data['nomor_depo'] = $this->input->post('nomor_depo',true);
			$data['nomor_ruang'] = $this->input->post('nomor_ruang',true);
			$data['nomor_lemari'] = $this->input->post('nomor_lemari',true);
			$data['nomor_boks'] = $this->input->post('nomor_boks',true);
			$data['nomor_folder'] = $this->input->post('nomor_folder',true);
			
			$data['kode_laporan'] = $this->_generateKodeLaporan($data);
			
			$result = $this->db->insert('laporan_pertelaan',$data);
			
			if($result)
				$ress['result'] = TRUE;
			else
				$ress['result'] = FALSE;
			
		    echo json_encode($ress);
       
    }

  private function _generateKodeLaporan($data = array()) {
  	    
		 $a = 'X'; $b = 'X'; $c = 'X'; $d = 'X';$e = 'X'; $f = 'X'; $g = 'X'; $h = 'X'; $i = 'X'; $j = 'X';
		 
		   if(!empty($data['status_arsip'])) {
				switch ($data['status_arsip']) {
					case 'status_pindah':
						$a = 1;
						break;
					case 'status_pinjam':
						$a = 2;
						break;
					case 'status_musnah':
						$a = 3;
						break;
					case 'status_serah':
						$a = 4;
						break;					
					default:
						$a = 'X';				
						break;
				}	
			}
			if(!empty($data['kode_klasifikasi'])) {
				$b = $data['kode_klasifikasi'];
			}
			if(!empty($data['kode_unit_kerja'])) {
				$c = $data['kode_unit_kerja'];
			}
			if(!empty($data['kurun_waktu_awal'])) {
				$d = $data['kurun_waktu_awal'];
			}
			if(!empty($data['kurun_waktu_akhir'])) {
				$e = $data['kurun_waktu_akhir'];
			}
			if(!empty($data['nomor_depo'])) {
				$f = $data['nomor_depo'];
			}
			if(!empty($data['nomor_ruang'])) {
				$g = $data['nomor_ruang'];
			}
			if(!empty($data['nomor_lemari'])) {
				$h = $data['nomor_lemari'];
			}
			if(!empty($data['nomor_boks'])) {
				$i = $data['nomor_boks'];
			}
			if(!empty($data['nomor_folder'])) {
				$j = $data['nomor_folder'];
			}
			
			$kode_laporan = $a.'-'.$b.'-'.$c.'-'.$d.'-'.$e.'-'.$f.'-'.$g.'-'.$h.'-'.$i.'-'.$j;
			
			return $kode_laporan;
  }

 public function resultPDF() {
 	     
		  $this->load->model('pemindahan/penerimaan_model');
		        
	     $data = array();
		  $data = $this->db->query("select * from laporan_pertelaan where id = 1")->row_array();
	
			if(!empty($data['status_arsip'])) {
				$this->db->where($data['status_arsip'],'Y');
			}
			if(!empty($data['kode_klasifikasi'])) {
				$this->db->where('kode_klasifikasi',$data['kode_klasifikasi']);
			}
			if(!empty($data['kode_jra'])) {
				$this->db->where('kode_jra',$data['kode_jra']);
			}
			if(!empty($data['kode_unit_kerja'])) {
				$this->db->where('arsip.kode_unit_kerja',$data['kode_unit_kerja']);
			}
			if(!empty($data['kurun_waktu_awal'])) {
				$this->db->where('kurun_waktu_awal >=',$data['kurun_waktu_awal']);
			}
			if(!empty($data['kurun_waktu_akhir'])) {
				$this->db->where('kurun_waktu_akhir <=',$data['kurun_waktu_akhir']);
			}
			if(!empty($data['nomor_depo'])) {
				$this->db->where('nomor_depo',$data['nomor_depo']);
			}
			if(!empty($data['nomor_ruang'])) {
				$this->db->where('nomor_ruang',$data['nomor_ruang']);
			}
			if(!empty($data['nomor_lemari'])) {
				$this->db->where('nomor_lemari',$data['nomor_lemari']);
			}
			if(!empty($data['nomor_boks'])) {
				$this->db->where('nomor_boks',$data['nomor_boks']);
			}
			if(!empty($data['nomor_folder'])) {
				$this->db->like('nomor_folder',$data['nomor_folder']);
			}
			if(!empty($data['uraian'])) {
				$this->db->like('uraian',$data['uraian']);
			}
			$this->db->order_by('id_arsip','ASC');
			$this->db->select('nama_unit_kerja as unit_kerja, id_arsip, nomor_definitif, uraian, kode_klasifikasi, arsip.kode_unit_kerja, arsip.kode_jra, kurun_waktu_awal, kurun_waktu_akhir, tingkat_perkembangan, media_simpan, kondisi_fisik, nomor_depo, nomor_ruang, nomor_lemari, nomor_boks, nomor_folder, jumlah_berkas');
			$this->db->join('unit_kerja','unit_kerja.kode_unit_kerja = arsip.kode_unit_kerja');
			$arsip = $this->db->get('arsip')->result_array();
			
			$this->pdfLaporan($arsip,$data['kode_laporan']);
			
			
 }


public function resultExcel() {
 	     
		  $this->load->model('pemindahan/penerimaan_model');
		        
	     $data = array();
		  $data = $this->db->query("select * from laporan_pertelaan where id = 1")->row_array();
	
			if(!empty($data['status_arsip'])) {
				$this->db->where($data['status_arsip'],'Y');
			}
			if(!empty($data['kode_klasifikasi'])) {
				$this->db->where('kode_klasifikasi',$data['kode_klasifikasi']);
			}
			if(!empty($data['kode_jra'])) {
				$this->db->where('kode_jra',$data['kode_jra']);
			}
			if(!empty($data['kode_unit_kerja'])) {
				$this->db->where('arsip.kode_unit_kerja',$data['kode_unit_kerja']);
			}
			if(!empty($data['kurun_waktu_awal'])) {
				$this->db->where('kurun_waktu_awal >=',$data['kurun_waktu_awal']);
			}
			if(!empty($data['kurun_waktu_akhir'])) {
				$this->db->where('kurun_waktu_akhir <=',$data['kurun_waktu_akhir']);
			}
			if(!empty($data['nomor_depo'])) {
				$this->db->where('nomor_depo',$data['nomor_depo']);
			}
			if(!empty($data['nomor_ruang'])) {
				$this->db->where('nomor_ruang',$data['nomor_ruang']);
			}
			if(!empty($data['nomor_lemari'])) {
				$this->db->where('nomor_lemari',$data['nomor_lemari']);
			}
			if(!empty($data['nomor_boks'])) {
				$this->db->where('nomor_boks',$data['nomor_boks']);
			}
			if(!empty($data['nomor_folder'])) {
				$this->db->like('nomor_folder',$data['nomor_folder']);
			}
			if(!empty($data['uraian'])) {
				$this->db->like('uraian',$data['uraian']);
			}
						
			
			$this->db->order_by('id_arsip','ASC');
			$this->db->select('nama_unit_kerja as unit_kerja, id_arsip, nomor_definitif, uraian, kode_klasifikasi, arsip.kode_unit_kerja, arsip.kode_jra, kurun_waktu_awal, kurun_waktu_akhir, tingkat_perkembangan, media_simpan, kondisi_fisik, nomor_depo, nomor_ruang, nomor_lemari, nomor_boks, nomor_folder, jumlah_berkas');
			$this->db->join('unit_kerja','unit_kerja.kode_unit_kerja = arsip.kode_unit_kerja');
			$result['arsip'] = $this->db->get('arsip')->result_array();
			
			$this->load->view('pertelaan_excel',$result);
 }


	private function pdfLaporan($arsip = array(),$kode_laporan) {
		
		ini_set('memory_limit', '512M');
				
		$this->load->library('TCPDF');

		// create new PDF document
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION,PDF_UNIT,PDF_PAGE_FORMAT,true,'UTF-8',false);
		// set document information
		$pdf->SetCreator(WEB_TITLE);
		$pdf->SetAuthor($this->session->userdata('username'));
		$pdf->SetTitle("Daftar Pertelaan Arsip");
		$pdf->SetSubject($kode_laporan);
		$pdf->code = $kode_laporan;
	
		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN,'',PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA,'',PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(5,5,5);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE,PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// ---------------------------------------------------------

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);

		// Set font
		// dejavusans is a UTF-8 Unicode font, if you only need to
		// print standard ASCII chars, you can use core fonts like
		// helvetica or times to reduce file size.
		$pdf->SetFont('times','',10,'',true);

		// Add a page
		// This method has several options, check the source code documentation for more information.
		$pdf->AddPage("L");
		// Set some content to print

	$html = '
	<style type="text/css">
.rotate-text
 {

/* Safari */
-webkit-transform: rotate(-90deg);

/* Firefox */
-moz-transform: rotate(-90deg);

/* IE */
-ms-transform: rotate(-90deg);

/* Opera */
-o-transform: rotate(-90deg);

/* Internet Explorer */
filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);

}
.border-table{
	border:0.5px solid #000;
		
}
</style>
	
<body>
<table width="100%">
  <tr>
    <td colspan="15"><H3>Daftar Pertelaan Arsip</H3></td>
	<td colspan="2" rowspan="2" style="text-align: center; vertical-align:text-top;"><img src="'.base_url().'public/images/logo.png" width="50"></td>
  </tr>
  <tr>
    <td width="22"></td>
    <td colspan="15"></td>	
  </tr>
  </table>
  <table cellpadding="0" cellspacing="0" border="0.5">
  <tr>
    <td rowspan="2" width="25"   style="text-align: center;" valign="center" class="border-table"><strong>No</strong></td>
    <td  rowspan="2" style="text-align: center; vertical-align:text-top;" class="border-table"><strong>Nomor Definitif</strong></td>
    <td  rowspan="2" style="text-align: center; vertical-align:text-top;" class="border-table"><strong>KODE KLAS</strong></td>
    <td  rowspan="2" style="text-align: center; vertical-align:text-top;" class="border-table"><strong>UNIT KERJA</strong></td>
    <td  rowspan="2" style="text-align: center; vertical-align:text-top;" class="border-table"><strong>KODE JRA</strong></td>
    <td width="150" rowspan="2" style="text-align: center; vertical-align:text-top;" class="border-table"><strong>URAIAN</strong></td>
    <td colspan="2" width="100"  style="text-align: center; vertical-align:text-top;" class="border-table"><strong>KURUN WAKTU</strong></td>
    <td  rowspan="2" width="80" style="text-align: center; vertical-align:text-top;" class="border-table"><strong><sub>TINGKAT PERKEMBANGAN</sub></strong></td>
    <td  rowspan="2" style="text-align: center; vertical-align:text-top;" class="border-table"><strong><sub>MEDIA SIMPAN</sub></strong></td>
    <td  rowspan="2" style="text-align: center; vertical-align:text-top;" class="border-table"><strong><sub>KONDISI FISIK</sub></strong></td>
    <td colspan="5" width="250" style="text-align: center; vertical-align:text-top;" class="border-table"><strong>LOKASI SIMPAN</strong></td>
    <td width="45"  style="text-align: center; vertical-align:text-top;" class="border-table"><strong>KET.</strong></td>
  </tr>
  <tr>
    <td  style="text-align: center; vertical-align:text-center;" class="border-table"><strong><sub>AWAL</sub></strong></td>
    <td  style="text-align: center; vertical-align:text-top;" class="border-table"><strong><sub>AKHIR</sub></strong></td>
    <td width="50"  style="text-align: center; vertical-align:text-top;" class="border-table"><strong><sub>DEPO</sub></strong></td>
    <td width="50"  style="text-align: center; vertical-align:text-top;" class="border-table"><strong><sub>NO. RUANG</sub></strong></td>
    <td width="50"  style="text-align: center; vertical-align:text-top;" class="border-table"><strong><sub>NO. LEMARI</sub></strong></td>
    <td width="50"  style="text-align: center; vertical-align:text-top;" class="border-table"><strong><sub>NO. BOK</sub></strong></td>
    <td width="50"  style="text-align: center; vertical-align:text-top;" class="border-table"><strong><sub>NO. FOLDER</sub></strong></td>
    <td style="text-align: center; vertical-align:text-top;" class="border-table"><strong><sub>JUMLAH BERKAS</sub></strong></td>
  </tr>';

      
 
   $no = 0;
 foreach($arsip as $row) {
     $no++; 
$html .= '<tr>
    <td style="text-align: center;" class="border-table"> '.$no.'</td>
    <td  class="border-table"> '.$row['nomor_definitif'].'</td>
    <td class="border-table"> '.$row['kode_klasifikasi'].'</td>
    <td style="text-align: center;" class="border-table"> '.$row['kode_unit_kerja'].'</td>
    <td style="text-align: center;" class="border-table"> '.$row['kode_jra'].'</td>
    <td class="border-table"> '.$row['uraian'].'</td>
    <td style="text-align: center;" class="border-table"><sub> '.$row['kurun_waktu_awal'].'</sub></td>
    <td style="text-align: center;" class="border-table"><sub> '.$row['kurun_waktu_akhir'].'</sub></td>
    <td style="text-align: center;" class="border-table"><sub> '.$row['tingkat_perkembangan'].'</sub></td>
    <td style="text-align: center;" class="border-table"><sub> '.$row['media_simpan'].'</sub></td>
    <td style="text-align: center;" class="border-table"><sub> '.$row['kondisi_fisik'].'</sub></td>
    <td class="border-table"><sub> '.$row['nomor_depo'].'</sub></td>
    <td class="border-table"><sub> '.$row['nomor_ruang'].'</sub></td>
    <td class="border-table"><sub> '.$row['nomor_lemari'].'</sub></td>
    <td class="border-table"><sub> '.$row['nomor_boks'].'</sub></td>
    <td class="border-table"><sub> '.$row['nomor_folder'].'</sub></td>
    <td style="text-align: right;" class="border-table"><sub> &nbsp; '.$row['jumlah_berkas'].' &nbsp; </sub></td>
  </tr>';
  }

$html .=  
'</table>
</body>';

		// Print text using writeHTMLCell()
		$pdf->writeHTMLCell($w = 0,$h = 0,$x = '',$y = '',$html,$border = 0,$ln = 1,$fill = 0,$reseth = true,$align = '',$autopadding = true);
		//$pdf->writeHTML($html, true, 0, true, true);

		// ---------------------------------------------------------

		// Close and output PDF document
		// This method has several options, check the source code documentation for more information.
	
		$pdf->Output("Daftar Pertelaan Arsip - ".$kode_laporan,'I');

		//============================================================+
		// END OF FILE
		//============================================================+

	}

}


class MYPDF extends TCPDF {

        var $id;
        var $indeks;
        var $style = array('border'=>0,'vpadding'=>0,'hpadding'=>0,'fgcolor'=> array(0,0,0),'bgcolor'=>false,//array(255,255,255)
		'module_width'=>1,// width of a single module in points
		'module_height'=>1 // height of a single module in points
		);
        var $code;
		  
	//Page header
	 public function Header() {
        //set Image Background (watermark)
        $img_file = K_PATH_IMAGES.'logo.png';
        $this->Image($img_file, 85, 30, 108, 160, '', '', 'center', true, 300, '', false, false, 0);
    }
	// Page footer
	public function Footer() {
            
            $html = "<div align='right' width='100%'><sub>".WEB_TITLE." - ".date("Y")."</sub></div>";
            $this->writeHTML($html, true, 0, true, true);
            
            $code = $this->code;
				$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);
				$this->write2DBarcode($code, 'QRCODE,H', 270, 182, 22, 22, $style, 'N');
					
	}

	}
  
      
	/* End of file login.php */
	/* Location: ./application/controllers/login.php */
