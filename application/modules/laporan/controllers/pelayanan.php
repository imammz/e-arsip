<?php
	if(!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Pelayanan extends MY_Controller {

		function __construct() {
			parent::__construct();
			$this->_checkLogin();
			$this->load->model('master/master_model');
		$this->load->model('pelayanan/peminjaman_model');
		}

		public function index() {
			$data = array();
			$data['class'] = 'laporan';
			$data['function'] = 'pelayanan';
			
			$data['peminjaman'] = $this->peminjaman_model->_loadAllPelayanan();
			
			$this->load->view('laporan_pelayanan_view',$data);
		}

	}


	/* End of file login.php */
	/* Location: ./application/controllers/login.php */
