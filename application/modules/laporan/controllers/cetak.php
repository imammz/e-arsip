<?php

	if(!defined('BASEPATH'))
		exit('No direct script access allowed');

	class cetak extends MY_Controller
    {
        function __construct()
        {
            parent::__construct();
            $this->_checkLogin();
            if ($this->session->userdata('login') == FALSE) {
                redirect('login');
            }
            $this->load->model('pemindahan/penerimaan_model');
            $this->load->model('pelayanan/peminjaman_model');

        }

        public function Dpa() {
            $data = array();
            $data['class'] = 'laporan';
            $data['function'] = 'pertelaan';

            $this->load->library('pdf');

            // create new PDF document
            $pdf = new MYPDF(PDF_PAGE_ORIENTATION,PDF_UNIT,PDF_PAGE_FORMAT,true,'UTF-8',false);
            // set document information
            $pdf->SetCreator('PUSKOMPUBLIK');
            $pdf->SetAuthor('Admin');
            $pdf->SetTitle("Print Out");
            $pdf->SetSubject('');

            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO,PDF_HEADER_LOGO_WIDTH,PDF_HEADER_TITLE . ' 006',PDF_HEADER_STRING);

            // set header and footer fonts
            $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN,'',PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA,'',PDF_FONT_SIZE_DATA));

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT,PDF_MARGIN_TOP,PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

            // set auto page breaks
            $pdf->SetAutoPageBreak(TRUE,PDF_MARGIN_BOTTOM);

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            //set some language-dependent strings
            $lang = 'eng';
            $pdf->setLanguageArray($lang);

            // ---------------------------------------------------------
            // set default font subsetting mode
            $pdf->setFontSubsetting(true);

            // Set font
            // dejavusans is a UTF-8 Unicode font, if you only need to
            // print standard ASCII chars, you can use core fonts like
            // helvetica or times to reduce file size.
            $pdf->SetFont('times','',12,'',true);

            // Add a page
            // This method has several options, check the source code documentation for more information.
            $pdf->AddPage("J");
            // Set some content to print

            $html = <<<EOD
   <centre>
<h2> DKPP </h2>
<sup></sup> <br/>

</centre>

EOD;

            // Print text using writeHTMLCell()
            $pdf->writeHTMLCell($w = 0,$h = 0,$x = '',$y = '',$html,$border = 0,$ln = 1,$fill = 0,$reseth = true,$align = '',$autopadding = true);
            //$pdf->writeHTML($html, true, 0, true, true);
            // ---------------------------------------------------------
            // Close and output PDF document
            // This method has several options, check the source code documentation for more information.
            $pdf->Output('' . "." . date("y"),'I');

            //============================================================+
            // END OF FILE
            //============================================================+

        }








    }



	/* End of file login.php */
	/* Location: ./application/controllers/login.php */
