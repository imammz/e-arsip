<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Beritaacara extends MY_Controller {
     
	  function __construct() {
			parent::__construct();
			$this->_checkLogin();
		}
    
    
	public function index()
	{
			$data = array();
			$data['class'] = 'laporan';
			$data['function'] = 'beritaacara';
         $this->load->view('beritaacara_view',$data);
	}
	
	
   
	
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */