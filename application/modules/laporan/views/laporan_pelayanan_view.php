<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- start: HEAD -->

    <?php echo Modules::run('templates/'.TEMPLATE.'/meta_css'); ?>
    <!-- end: HEAD -->
    <!-- start: BODY -->
    <body>
        <!-- start: HEADER -->
        <?php echo Modules::run('templates/'.TEMPLATE.'/header'); ?>
        	<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugins/select2/select2.css" />
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/DataTables/media/css/DT_bootstrap.css" />
                <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/gritter/css/jquery.gritter.css">
                <link href="<?php echo base_url() ?>assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
		<link href="<?php echo base_url() ?>assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
            <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/select2/select2.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/datepicker/css/datepicker.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/jQuery-Tags-Input/jquery.tagsinput.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/summernote/build/summernote.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/ckeditor/contents.css">
		
		<script type="text/javascript" src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>
      <link type="text/css" href="<?php echo base_url() ?>assets/library/gb/greybox.css" rel="stylesheet" />
                <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
        <!-- end: HEADER -->
        <!-- start: MAIN CONTAINER -->
        <div class="main-container" style="margin-top:150px;">
            <?php echo Modules::run('templates/'.TEMPLATE.'/menu'); ?>
            <!-- start: PAGE -->
            <div class="main-content">

                <div class="container">
                    <!-- start: PAGE HEADER -->
                    <div class="row">
                        <div class="col-sm-12">							
                            <!-- start: PAGE TITLE & BREADCRUMB -->
                            <ol class="breadcrumb">
                                <li>
                                    <i class="clip-stack"></i>
                                    <a href="#">
                                        Laporan
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Pelayanan
                                    </a>
                                </li>

                                
                            </ol>

                            <div class="page-header">
                                <h1>Pelayanan</h1>
                                <p>Fungsi modul untuk membuat laporan pelayanan peminjaman fisik arsip/dokumen inaktif di Pusat Arsip. &nbsp; <a href="#modalMore" data-toggle="modal" class="btn btn-default btn-xs"> <i class="clip-expand"></i> </a> </p>
                                <a class="btn btn-bricky" href="#" onclick="formCetakSemua();"><i class="clip-file-pdf"></i>Cetak Semua</a>
                                <a class="btn btn-success" onclick="formCetakSemuaExcell();" data-toggle="modal" href="#">
                                    <i class="clip-file-excel "></i>
                                    Cetak & Print Excel
                                </a>
                            </div>
                      
                             <div class="row">
                                                    <div class="col-sm-12">
                                                            
                                                        <!-- start fade modal detail uraian -->
                                                       
                                                        <!-- end fade modal detail uraian -->
        
		
							 <h3>Daftar Peminjaman</h3>
                             <p>
                             	<div id="demo"> </div>	
                             </p>
		
		
        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                    </div>
                                                   
                                                 </div>

                        </div>
                    </div>
                    <!-- end: PAGE HEADER -->
                    <!-- start: PAGE CONTENT -->



                    <!-- end: PAGE CONTENT-->
                </div>
            </div>
            <!-- end: PAGE -->
        </div>
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <?php echo Modules::run('templates/'.TEMPLATE.'/footer'); ?>
        <!-- end: FOOTER -->

        <!-- start: MAIN JAVASCRIPTS -->
        <?php echo Modules::run('templates/'.TEMPLATE.'/js'); ?>
        <!-- end: MAIN JAVASCRIPTS -->
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/select2/select2.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
		<script src="<?php echo base_url() ?>assets/js/table-data.js"></script>
                <script src="<?php echo base_url() ?>assets/plugins/bootstrap-paginator/src/bootstrap-paginator.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery.pulsate/jquery.pulsate.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/gritter/js/jquery.gritter.min.js"></script>
		<script src="<?php echo base_url() ?>assets/js/ui-elements.js"></script>
                <script src="<?php echo base_url() ?>assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
		<script src="<?php echo base_url() ?>assets/js/ui-modals.js"></script>
                <script src="<?php echo base_url() ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/autosize/jquery.autosize.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/select2/select2.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery-maskmoney/jquery.maskMoney.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-colorpicker/js/commits.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/summernote/build/summernote.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/ckeditor/ckeditor.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
		<script src="<?php echo base_url() ?>assets/js/form-elements.js"></script>
        <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script>
                                    jQuery(document).ready(function() {
                                        Main.init();
                                        TableData1.init();
                                        UIElements.init();
                                        FormElements.init();
                                        
                                        $('#demo').html( '<table border="0" class="table table-striped table-bordered table-hover table-full-width" id="example"></table>' );
    $('#example').dataTable( {
        "aaData": [
            /* Reduced data set */
            <?php 
            $no = 1;
            foreach($peminjaman as $row) { 
            	$status = '<span class="label label-danger"> Menunggu Persetujuan</span>';
					if($row['status_otoritas']=='Y') $status = '<span class="label label-success">Proses Peminjaman</span>';
					if($row['status_pengembalian']=='Y') $status = '<span class="label label-info">Sudah Dikembalikan</span>';
            	
					$cetak = '-';
					if($row['status_otoritas']=='Y') $cetak = '<a class="btn btn-bricky" href="#" onclick="formCetak('.$row['id_peminjaman'].');"><i class="clip-file-pdf"></i>Cetak</a>';
            	
            	?>
            [ 
                "<?php echo $no ?>", 
                "<?php echo $row['kode_peminjaman'] ?>", 
                "<?php echo $row['nama_peminjam'] ?>", 
                "<?php echo $row['kode_unit_kerja'].' | '.$row['unit_kerja'] ?>", 
                "<a href='#' role='button' onclick='formDetil(<?php echo $row['id_peminjaman'] ?>);' class='btn btn-default' > Detail </a>",
                '<?php echo $status; ?>',
                '<?php echo $cetak; ?>'
                ],
            <?php $no++; } ?>        
        ],
       
        "oLanguage": {
                        "sLengthMenu": "Show _MENU_ Rows",
                        "sSearch": "",
                        "oPaginate": {
                            "sPrevious": "",
                            "sNext": ""
                        }
                    },
                  
                    "aoColumns": [
                        {"sTitle": "No", "sClass": "center"},
                        {"sTitle": "Kode Peminjaman"},
                        {"sTitle": "Nama Peminjam"},
                        {"sTitle": "Unit Kerja", "sClass": "center"},
                        {"sTitle": "Detail Arsip", "sClass": "center"},
                        {"sTitle": "Status", "sClass": "center"},
                        {"sTitle": "Bukti Peminjaman", "sClass": "center"}
                    ],
                    // set the initial value
                    "iDisplayLength": 10,
    } );   
                                        
                                    });
        </script>
        <script>
			function formDetil(id) {
            GB_show("Detil Data Arsip", '<?php echo base_url(); ?>pelayanan/otoritas/detil/'+id, 900, 1070);
           $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
        
        function formCetak(id) {
            GB_show("Detil Data Arsip", '<?php echo base_url(); ?>pelayanan/peminjaman/pdfFormPeminjaman/'+id, 900, 1070);
           $('html, body').animate({ scrollTop: 0 }, 'slow');
        }

        function formCetakSemua(){
            GB_show("Detil Data Arsip", '<?php echo base_url();?>laporan/allform/index',900,1070);
        }

        function formCetakSemuaExcell(){
            GB_show("Detil Data Arsip", '<?php echo base_url();?>laporan/allform/cetakexcell')
        }
		</script>
  
    </body>
    <!-- end: BODY -->

    <!-- Mirrored from www.cliptheme.com/clip-one/ by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 16 Nov 2013 08:37:43 GMT -->
</html>