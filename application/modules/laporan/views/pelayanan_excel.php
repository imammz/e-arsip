<?php
//echo $jumlah_baris;
//echo "<pre>";
//print_r($data_pelayanan);
//echo "</pre>";
//exit;
header("Pragma: public");
header('Content-Type: application/vnd.ms-excel');
$filename = 'DPA_'.date('d-M-Y').'.xls';
header('Content-Disposition: attachment; filename='.$filename);

$data = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">
    <head>
    <!--[if gte mso 9]>
    <xml>
        <x:ExcelWorkbook>
            <x:ExcelWorkSheet>
                <x:ExcelWorkSheet>
                    <x:Name>Sheet 1</x:Name>
                    <x:WorksheetOptions>
                        <x:Print>
                            <x:ValidPrinterInfo/>
                        </x:Print>
                    </x:WorksheetOptions>
                </x:ExcelWorksheets>
            </x:ExcelWorksheets>
        </x:ExcelWorkbook>
    </xml>
    <![endif]-->
<style type="text/css">
.rotate-text
 {

/* Safari */
-webkit-transform: rotate(-90deg);

/* Firefox */
-moz-transform: rotate(-90deg);

/* IE */
-ms-transform: rotate(-90deg);

/* Opera */
-o-transform: rotate(-90deg);

/* Internet Explorer */
filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);

}
.border-table{
    border:1px solid #000;
}
</style>
</head>';
$data .='
<body>
    <body style = "=font-size: 9px; font-family: Arial;" >
    <table width = "100%" cellpadding = "0" cellspacing = "0" >
        <tr >
            <td colspan = "15" style = "text-align: center; vertical-align:text-top;" ><h3 > Daftar Pelayanan Arsip </h3 ></td >
            <td colspan = "2" rowspan = "2" style = "text-align: center; vertical-align:text-top;" ><img src"' . base_url() . 'public/images/logo.png" width = "50" ></td >
        </tr >
        <tr >
            <td colspan = "22" ></td >
            <td colspan = "15" ></td >
        </tr >
    </table>';
for ($i = 0; $i < $jumlah_baris; $i++) {
    $data .= '
    <table>
        <tr >
            <td width = "75" > Nama</td >
            <td width = "100" > ' . $data_pelayanan[$i]['nama_peminjam'] . ' </td >
        </tr >
        <tr >
            <td width = "75" > Tgl Peminjnaman </td >
            <td width = "100" > ' . $data_pelayanan[$i]['tanggal_peminjaman'] . ' </td>
        </tr>
        <tr >
            <td width = "75" > Tgl Pengembalian </td >
            <td width = "100" > ' . $data_pelayanan[$i]['tanggal_pengembalian'] . ' </td >
        </tr >
        <tr >
            <td colspan = "22" ></td >
            <td colspan = "15" ></td >
        </tr >
        <table>
            <tr>
                <td rowspan="2" width="40" style="text-align: center; vertical-align:text-top;" class="border-table"><strong>NO</strong></td>
                <td rowspan="2" width="58" style="text-align: center; vertical-align:text-top;" class="border-table"><strong>NOMOR DEFINITIF</strong></td>
                <td rowspan="2" width="180" style="text-align: center; vertical-align:text-top;" class="border-table"><strong>URAIAN</strong></td>
                <td rowspan="2" width="60" style="text-align: center; vertical-align:text-top;" class="border-table"><strong>KODE KLAS</strong></td>
                <td rowspan="2" width="78" style="text-align: center; vertical-align:text-top;" class="border-table"><strong>UNIT KERJA</strong></td>
                <td rowspan="2" width="80" style="text-align: center; vertical-align:text-top;" class="border-table"><strong>TINGKAT PERKEMBANGAN</strong></td>
                <td colspan="5" style="text-align: center; vertical-align:text-top;" class="border-table"><strong>LOKASI SIMPAN</strong></td>
            </tr>
            <tr>
                <td width="42" style="text-align: center; vertical-align:text-top;" class="border-table"><strong><sup>DEPO</sup></strong></td>
                <td width="42" style="text-align: center; vertical-align:text-top;" class="border-table"><strong><sup>NO. RUANG</sup></strong></td>
                <td width="42" style="text-align: center; vertical-align:text-top;" class="border-table"><strong><sup>NO. LEMARI</sup></strong></td>
                <td width="42" style="text-align: center; vertical-align:text-top;" class="border-table"><strong><sup>NO. BOK</sup></strong></td>
                <td width="42" style="text-align: center; vertical-align:text-top;" class="border-table"><strong><sup>NO. FOLDER</sup></strong></td>
            </tr>
        ';
    $no = 0;
    foreach ($data_pelayanan[$i]['arsip'] as $value){
        $no++;
        $data.='
            <tr>
                <td class="border-table">'.$no.'</td>
                <td class="border-table">'.$value['nomor_definitif'].'</td>
                <td class="border-table">'.$value['uraian'].'</td>
                <td class="border-table">'.$value['kode_klasifikasi'].'</td>
                <td class="border-table">'.$value['kode_unit_kerja'].'-'.$value['unit_kerja'].'</td>
                <td class="border-table">'.$value['tingkat_perkembangan'].'</td>
                <td class="border-table">'.$value['nomor_depo'].'</td>
                <td class="border-table">'.$value['nomor_ruang'].'</td>
                <td class="border-table">'.$value['nomor_lemari'].'</td>
                <td class="border-table">'.$value['nomor_boks'].'</td>
                <td class="border-table">'.$value['nomor_folder'].'</td>
            </tr>
         
        ';
    }
    $data .='
   <tr >
            <td colspan = "22" ></td >
            <td colspan = "15" ></td >
</tr >
<tr >
            <td colspan = "22" ></td >
            <td colspan = "15" ></td >
</tr >
</table>
</table>
';
    


}
    $data.='
    </table >
    </body>';

    $data .= '
    </body>
    </html>';
    echo $data;


?>