<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Standard extends CI_Controller {

    public function header() {
        $this->load->view('standard/header_view');
    }    
    public function footer() {
        $this->load->view('standard/footer_view');
    }
    public function menu() {
        $this->load->view('standard/menu_view');
    }
    public function js() {
        $this->load->view('standard/js_view');
    }
    public function css() {
        $this->load->view('standard/css_view');
    }
    public function meta_css() {
        $this->load->view('standard/meta_css_view');
    }
		
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */