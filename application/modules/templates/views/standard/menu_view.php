<div class="navbar-content">
				<!-- start: SIDEBAR -->
				<div class="main-navigation navbar-collapse collapse">
					<!-- start: MAIN MENU TOGGLER BUTTON -->
					<div class="navigation-toggler">
						<i class="clip-chevron-left"></i>
						<i class="clip-chevron-right"></i>
					</div>
					<!-- end: MAIN MENU TOGGLER BUTTON -->
					<!-- start: MAIN NAVIGATION MENU -->
					<?php 
                                        $id_role = $this->session->userdata('id_role');
                                        $menus = Master_model::_loadMenuByIdRole($id_role); 
                                        ?>
                                        <ul class="main-navigation-menu">
                                            <?php foreach($menus as $row_menu) { ?>    
                                                   <li <?php if($class == $row_menu['class_menu']) { echo 'class="active open"'; } else { echo ''; }  ?>>
                                                        <a href="<?php if($row_menu['function_menu']=='#') { echo 'javascript:void(0)';} else { echo base_url().$row_menu['class_menu'].'/'.$row_menu['function_menu']; } ?>"><i class="<?php echo $row_menu['icon'] ?>"></i>
								<span class="title <?php if($row_menu['id_menu']==18) echo 'label label-default' ?>"> <?php echo $row_menu['menu'] ?> </span><i class="icon-arrow"></i>
								<span class="selected"></span>
							</a>
							<ul class="sub-menu">
                                                            <?php $submenus = Master_model::_loadSubmenu($id_role,$row_menu['id_menu']); ?>
                                                            <?php foreach($submenus as $row_submenu) { ?>
                                                            <li <?php if($function == $row_submenu['function_menu']) { echo 'class="active open"'; } else { echo ''; }  ?>>
									<a href="<?php echo base_url().$row_submenu['class_menu'].'/'.$row_submenu['function_menu'] ?>">
										<span class="title"> <?php echo $row_submenu['menu'] ?> </span>
									</a>
							     </li>							
                                                            <?php } ?>	
							</ul>
						</li> 
                                            <?php } ?>
                                            
                                               
						
					</ul>
					<!-- end: MAIN NAVIGATION MENU -->
				</div>
				<!-- end: SIDEBAR -->
			</div>