<div class="navbar navbar-inverse navbar-fixed-top">
            <!-- TOP -->            
            <div class="top_menu">
            	<div class="top-running-text">
            		<?php
						$running_text = Master_model::_getRunningText();
	 ?>
                	<marquee>
                	<span style="font-size:14px; color:#FFF; float:right; font-weight:bold;"><?php echo $running_text ?></span>
                    </marquee>
                </div>
                <ul style="list-style: none;">
                <li class="search-box">
                    	         <form role="form" id="form-entry" class="sidebar-search"  method="POST" action="<?php echo base_url() ?>pemindahan/pencarian/result" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="text" placeholder="Cari Arsip..." name="uraian" id="search-header">
                            <button class="submit">
                                <i class="clip-search-3"></i>
                            </button>
                        </div>
                    </form>
                </li>
                <li style="margin-top:15px;">
                    <a href="" style="color:white; text-decoration:none; margin-left:30px;">
                        <i class="clip-calendar"></i>
                        &nbsp;<?php echo DATE('D d-M-Y'); ?>
                    </a>
                <li>
                </ul>
            </div>
            <!-- TOP END -->            
            <!-- start: TOP NAVIGATION CONTAINER -->            
			<div class="container" style="margin-top:50px;">
				<div class="navbar-header">
					<!-- start: RESPONSIVE MENU TOGGLER -->
					<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
						<span class="clip-list-2"></span>
					</button>
					<!-- end: RESPONSIVE MENU TOGGLER -->
					<!-- start: LOGO -->
					<a class="navbar-brand" href="<?php echo base_url() ?>">
					<img src="<?php echo base_url() ?>public/images/logo.png" width="30"/> &nbsp;	E-ARS<i class="clip-clip"></i>P
					&nbsp; &nbsp; &nbsp;
                                        [ <?php echo $this->session->userdata('role') . '-' . $this->session->userdata('nama_unit_kerja'); ?> ]
                                        </a>
					<!-- end: LOGO -->
				</div>
				<div class="navbar-tools">
					<!-- start: TOP NAVIGATION MENU -->
					<ul class="nav navbar-right">
					<?php $id_role = $this->session->userdata('id_role');
                                        if($id_role==1) { // jika administrator
                                        ?>	
                                            <!-- start: Refrensi DROPDOWN -->
						<li class="dropdown">
							<a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
								<i class="clip-list-5"></i> 
		
							</a>
							<ul class="dropdown-menu notifications">
								<li>
									<span class="dropdown-menu-title"> Referensi</span>
								</li>
								<li>
									<div class="drop-down-wrapper">
										<ul>
											<li>
                                                                                            <a href="<?php echo base_url() ?>master/masterdata">
													<span class="message">  Master Data </span>
												</a>
											</li>
											<li>
                                                                                            <a href="<?php echo base_url() ?>applikasi/runningtext">
													<span class="message">  Running Text </span>
												</a>
											</li>
											<li>
                                                                                            <a href="<?php echo base_url() ?>applikasi/runningtextlogin">
													<span class="message">  Running Text Login </span>
												</a>
											</li>
											<li>
                                                                                            <a href="<?php echo base_url() ?>applikasi/loginbackground">
													<span class="message">  Login Background </span>
												</a>
											</li>
											<li>
                                                                                            <a href="<?php echo base_url() ?>applikasi/help">
													<span class="message">  Help </span>
												</a>
											</li>
											<li>
                                                                                            <a href="<?php echo base_url() ?>applikasi/about">
													<span class="message">  About</span>
												</a>
											</li>
											<li>
                                                                                            <a href="<?php echo base_url() ?>applikasi/instansi">
													<span class="message">  Instansi</span>
												</a>
											</li>
											
										</ul>
									</div>
								</li>
								
							</ul>
						</li>
						<!-- end: Refrensi DROPDOWN -->
                                                
                                           
                                                 <!-- start: Refrensi DROPDOWN -->
						<li class="dropdown">
							<a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
								<i class="clip-cogs"></i> 
		
							</a>
							<ul class="dropdown-menu notifications">
								<li>
									<span class="dropdown-menu-title"> Administrator</span>
								</li>
								<li>
									<div class="drop-down-wrapper">
										<ul>
											<li>
												<a href="<?php echo base_url() ?>management_user/user">
													<span class="message">  User Management </span>
												</a>
											</li>
											<li>
												<a href="<?php echo base_url() ?>applikasi/logs">
													<span class="message">  Logs History </span>
												</a>
											</li>
	
											
										</ul>
									</div>
								</li>
								
							</ul>
						</li>
						<!-- end: Refrensi DROPDOWN -->
                                        <?php } ?>
                                                
						<!-- start: NOTIFICATION DROPDOWN -->
						<li class="dropdown">
							<a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
								<i class="clip-notification-2"></i>
								
							</a>
							<ul class="dropdown-menu notifications">
								<li>
									<span class="dropdown-menu-title"> Notifications</span>
								</li>
								<li>
									<div class="drop-down-wrapper">
										<ul>
											<li>
												<a href="<?php echo base_url() ?>pelayanan/otoritas">
													<span class="label label-warning"><i class="clip-pencil"></i></span>
													<span class="message">  - Arsip Butuh Otorisasi Peminjaman </span>
												</a>
											</li>
											<li>
												<a href="<?php echo base_url() ?>pelayanan/pengembalian">
													<span class="label label-danger"><i class="clip-warning"></i></span>
													<span class="message">  - Arsip Akan Jatuh Tempo Peminjaman </span>
												</a>
											</li>
											<li>
												<a href="<?php echo base_url() ?>penyusutan/usulpindah">
													<span class="label label-warning"><i class="fa fa-exchange"></i></span>
													<span class="message">  <?php echo Master_model::_countUsulPindah() ?> Arsip, Masuk Usul Pindah  </span>
												</a>
											</li>
											<li>
												<a href="<?php echo base_url() ?>penyusutan/usulmusnah">
													<span class="label label-danger"><i class="fa  fa-eraser"></i></span>
													<span class="message">  <?php echo Master_model::_countUsulMusnah() ?> Arsip Masuk Usul Musnah  </span>
												</a>
											</li>
											<li>
												<a href="<?php echo base_url() ?>penyusutan/usulserah">
													<span class="label label-danger"><i class="fa  fa-eraser"></i></span>
													<span class="message">  <?php echo Master_model::_countUsulSerah() ?> Arsip Masuk Usul Serah  </span>
												</a>
											</li>
										</ul>
									</div>
								</li>
								<!--
								<li class="view-all">                           
                           <a href="<?php echo base_url() ?>notification">
										See all notifications <i class="fa fa-arrow-circle-o-right"></i>
									</a>									
								</li>
								-->
							</ul>
						</li>
						<!-- end: NOTIFICATION DROPDOWN -->
						
						<!-- start: USER DROPDOWN -->
						<li class="dropdown current-user">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<?php $path = Master_model::_getPathImage($this->session->userdata('username')); ?>
								<img src="<?php echo isset($path)?base_url().$path:'assets/images/avatar-1-xl.jpg'?>" class="circle-img" alt="" width="70">
								<span class="username"><?php echo $this->session->userdata('nama_lengkap') ?></span>
								<i class="clip-chevron-down"></i>
							</a>
							<ul class="dropdown-menu">
								<li>
                                                                    <a href="<?php echo base_url() ?>user/profile">
										<i class="clip-user-2"></i>
										&nbsp;My Profile
									</a>
								</li>

								<li class="divider"></li>
								
								<li>
									<a href="<?php echo base_url() ?>login/logout">
										<i class="clip-exit"></i>
										&nbsp;Log Out
									</a>
								</li>
							</ul>
						</li>
						<!-- end: USER DROPDOWN -->
					</ul>
					<!-- end: TOP NAVIGATION MENU -->
				</div>
			</div>
			<!-- end: TOP NAVIGATION CONTAINER -->
		</div>