<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- start: HEAD -->

    <?php echo Modules::run('templates/'.TEMPLATE.'/meta_css'); ?>
    <!-- end: HEAD -->
    <!-- start: BODY -->
    <body style="zoom: 80%;" >
        <!-- start: HEADER -->
            <?php echo Modules::run('templates/'.TEMPLATE.'/header'); ?>
        
         <!-- start: MAIN JAVASCRIPTS -->
        <?php echo Modules::run('templates/'.TEMPLATE.'/js'); ?>
         
         <script>
         jQuery(document).ready(function () { 
                    $('#main-container').load('<?php echo $url ?>');
             });
             
             function menuLoad(url) {                
                var loadUrl = '<?php echo base_url()?>'+url;
                         
                  $('#main-container').load(loadUrl);
                  
                  loadReady();
             }
             
             function loadReady() {
                                         Main.init();
                                        TableData1.init();
                                        UIElements.init();
                                        FormElements.init();
                                       // alert('a');
                                    }
             
         </script>
         
        <!-- end: MAIN JAVASCRIPTS -->	
                
                
        <!-- end: HEADER -->
        <!-- start: MAIN CONTAINER -->
        <div class="main-container">
            <?php echo Modules::run('templates/'.TEMPLATE.'/menu'); ?>
            <div id="main-container">
                
            </div>
        </div>
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <?php echo Modules::run('templates/'.TEMPLATE.'/footer'); ?>
        <!-- end: FOOTER -->

       
                 
                
    </body>
    <!-- end: BODY -->

    <!-- Mirrored from www.cliptheme.com/clip-one/ by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 16 Nov 2013 08:37:43 GMT -->
</html>